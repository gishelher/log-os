name := "log-os"

version := "0.0.1"

organization := "pl.edu.logos"

scalaVersion := "2.9.1"

resolvers ++= Seq("snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
  "releases" at "http://scala-tools.org/repo-releases"
)

seq(
  (com.github.siasia.WebPlugin.webSettings
    ++ inConfig(Compile)(
    artifact in packageWar <<= moduleName(n => Artifact("my-war", "war", "war"))
  )): _*)

scalacOptions ++= Seq("-deprecation")

libraryDependencies ++= {
  val liftVersion = "2.4"
  val slf4jVerion = "1.6.4"
  Seq(
    "net.liftweb" %% "lift-mapper" % liftVersion,
    "org.slf4j" % "slf4j-api" % slf4jVerion,
    "org.slf4j" % "slf4j-simple" % slf4jVerion,
    "postgresql" % "postgresql" % "9.0-801.jdbc4",
    "junit" % "junit" % "4.7" % "test",
    "org.mortbay.jetty" % "jetty" % "6.1.25" % "container; test",
    "org.scala-tools.testing" % "specs_2.8.1" % "1.6.6" % "test",
    "com.lowagie" % "itext" % "2.0.8",
    "org.xhtmlrenderer" % "core-renderer" % "R8",
    "org.apache.commons" % "commons-io" % "1.3.2",
    "commons-lang" % "commons-lang" % "2.6",
    "nz.ac.waikato.cms.weka" % "weka-stable" % "3.6.6" ,
    "org.specs2" % "specs2_2.9.1" % "1.8" ,
    "cc.mallet" % "mallet" % "2.0.7"
  )
}
