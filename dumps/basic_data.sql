--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: case_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE case_t (
    id bigint NOT NULL,
    session_c bigint,
    deleted boolean,
    childfield bigint
);


ALTER TABLE public.case_t OWNER TO sieve;

--
-- Name: case_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE case_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.case_t_id_seq OWNER TO sieve;

--
-- Name: case_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE case_t_id_seq OWNED BY case_t.id;


--
-- Name: case_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('case_t_id_seq', 31, true);


--
-- Name: child; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE child (
    name character varying(64),
    id bigint NOT NULL,
    deleted boolean,
    pesel character varying(16),
    borndate date
);


ALTER TABLE public.child OWNER TO sieve;

--
-- Name: child_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE child_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.child_id_seq OWNER TO sieve;

--
-- Name: child_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE child_id_seq OWNED BY child.id;


--
-- Name: child_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('child_id_seq', 3, true);


--
-- Name: diagnosis; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosis (
    id bigint NOT NULL,
    deleted boolean,
    mycase bigint
);


ALTER TABLE public.diagnosis OWNER TO sieve;

--
-- Name: diagnosis_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosis_id_seq OWNER TO sieve;

--
-- Name: diagnosis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosis_id_seq OWNED BY diagnosis.id;


--
-- Name: diagnosis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosis_id_seq', 11, true);


--
-- Name: diagnosisdisorder; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosisdisorder (
    id bigint NOT NULL,
    deleted boolean,
    diagnosis bigint,
    disorder bigint
);


ALTER TABLE public.diagnosisdisorder OWNER TO sieve;

--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosisdisorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosisdisorder_id_seq OWNER TO sieve;

--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosisdisorder_id_seq OWNED BY diagnosisdisorder.id;


--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosisdisorder_id_seq', 19, true);


--
-- Name: diagnosissuggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosissuggestion (
    id bigint NOT NULL,
    suggestion bigint,
    deleted boolean,
    diagnosis bigint
);


ALTER TABLE public.diagnosissuggestion OWNER TO sieve;

--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosissuggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosissuggestion_id_seq OWNER TO sieve;

--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosissuggestion_id_seq OWNED BY diagnosissuggestion.id;


--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosissuggestion_id_seq', 12, true);


--
-- Name: disorder; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE disorder (
    name character varying(128),
    id bigint NOT NULL,
    deleted boolean
);


ALTER TABLE public.disorder OWNER TO sieve;

--
-- Name: disorder_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE disorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disorder_id_seq OWNER TO sieve;

--
-- Name: disorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE disorder_id_seq OWNED BY disorder.id;


--
-- Name: disorder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('disorder_id_seq', 5, true);


--
-- Name: disordersuggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE disordersuggestion (
    id bigint NOT NULL,
    suggestion bigint,
    deleted boolean,
    disorder bigint
);


ALTER TABLE public.disordersuggestion OWNER TO sieve;

--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE disordersuggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disordersuggestion_id_seq OWNER TO sieve;

--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE disordersuggestion_id_seq OWNED BY disordersuggestion.id;


--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('disordersuggestion_id_seq', 9, true);


--
-- Name: probe; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probe (
    result bigint,
    number_c integer,
    id bigint NOT NULL,
    deleted boolean,
    mycase bigint,
    mytype bigint
);


ALTER TABLE public.probe OWNER TO sieve;

--
-- Name: probe_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probe_id_seq OWNER TO sieve;

--
-- Name: probe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probe_id_seq OWNED BY probe.id;


--
-- Name: probe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probe_id_seq', 149, true);


--
-- Name: proberesource; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesource (
    name character varying(128),
    path character varying(128),
    id bigint NOT NULL,
    label character varying(128),
    deleted boolean,
    probetype bigint
);


ALTER TABLE public.proberesource OWNER TO sieve;

--
-- Name: proberesource_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesource_id_seq OWNER TO sieve;

--
-- Name: proberesource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesource_id_seq OWNED BY proberesource.id;


--
-- Name: proberesource_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesource_id_seq', 4, true);


--
-- Name: proberesult; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesult (
    name character varying(124),
    id bigint NOT NULL,
    deleted boolean,
    classname character varying(32),
    positive_c boolean
);


ALTER TABLE public.proberesult OWNER TO sieve;

--
-- Name: proberesult_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesult_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesult_id_seq OWNER TO sieve;

--
-- Name: proberesult_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesult_id_seq OWNED BY proberesult.id;


--
-- Name: proberesult_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesult_id_seq', 10, true);


--
-- Name: proberesultprobetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesultprobetype (
    id bigint NOT NULL,
    deleted boolean,
    probetype bigint,
    proberesult bigint,
    default_c boolean
);


ALTER TABLE public.proberesultprobetype OWNER TO sieve;

--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesultprobetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesultprobetype_id_seq OWNER TO sieve;

--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesultprobetype_id_seq OWNED BY proberesultprobetype.id;


--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesultprobetype_id_seq', 319, true);


--
-- Name: probetreenode; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetreenode (
    id bigint NOT NULL,
    deleted boolean,
    nodeid integer,
    probe bigint,
    childnodeid integer,
    probetype bigint
);


ALTER TABLE public.probetreenode OWNER TO sieve;

--
-- Name: probetreenode_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetreenode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetreenode_id_seq OWNER TO sieve;

--
-- Name: probetreenode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetreenode_id_seq OWNED BY probetreenode.id;


--
-- Name: probetreenode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetreenode_id_seq', 75, true);


--
-- Name: probetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetype (
    name character varying(124),
    id bigint NOT NULL,
    deleted boolean,
    initial_c boolean,
    dependson bigint,
    listnumber integer
);


ALTER TABLE public.probetype OWNER TO sieve;

--
-- Name: probetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetype_id_seq OWNER TO sieve;

--
-- Name: probetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetype_id_seq OWNED BY probetype.id;


--
-- Name: probetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetype_id_seq', 70, true);


--
-- Name: probetypeprobetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetypeprobetype (
    id bigint NOT NULL,
    parentprobe bigint,
    childprobe bigint
);


ALTER TABLE public.probetypeprobetype OWNER TO sieve;

--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetypeprobetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetypeprobetype_id_seq OWNER TO sieve;

--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetypeprobetype_id_seq OWNED BY probetypeprobetype.id;


--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetypeprobetype_id_seq', 61, true);


--
-- Name: probetypetag; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetypetag (
    id bigint NOT NULL,
    tag bigint,
    probetype bigint
);


ALTER TABLE public.probetypetag OWNER TO sieve;

--
-- Name: probetypetag_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetypetag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetypetag_id_seq OWNER TO sieve;

--
-- Name: probetypetag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetypetag_id_seq OWNED BY probetypetag.id;


--
-- Name: probetypetag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetypetag_id_seq', 102, true);


--
-- Name: school; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE school (
    name character varying(128),
    id bigint NOT NULL,
    deleted boolean
);


ALTER TABLE public.school OWNER TO sieve;

--
-- Name: school_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.school_id_seq OWNER TO sieve;

--
-- Name: school_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE school_id_seq OWNED BY school.id;


--
-- Name: school_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('school_id_seq', 2, true);


--
-- Name: session_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE session_t (
    hash character varying(64),
    open_c boolean,
    id bigint NOT NULL,
    owner bigint,
    deleted boolean,
    creationdate date,
    school bigint,
    examinedate date,
    ispubliceditable boolean
);


ALTER TABLE public.session_t OWNER TO sieve;

--
-- Name: session_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE session_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.session_t_id_seq OWNER TO sieve;

--
-- Name: session_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE session_t_id_seq OWNED BY session_t.id;


--
-- Name: session_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('session_t_id_seq', 10, true);


--
-- Name: suggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE suggestion (
    name character varying(128),
    id bigint NOT NULL,
    description character varying(1024),
    deleted boolean
);


ALTER TABLE public.suggestion OWNER TO sieve;

--
-- Name: suggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE suggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.suggestion_id_seq OWNER TO sieve;

--
-- Name: suggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE suggestion_id_seq OWNED BY suggestion.id;


--
-- Name: suggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('suggestion_id_seq', 2, true);


--
-- Name: tag; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE tag (
    name character varying(128),
    id bigint NOT NULL
);


ALTER TABLE public.tag OWNER TO sieve;

--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO sieve;

--
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE tag_id_seq OWNED BY tag.id;


--
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('tag_id_seq', 77, true);


--
-- Name: user_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE user_t (
    locale character varying(16),
    id bigint NOT NULL,
    validated boolean,
    password_pw character varying(48),
    password_slt character varying(20),
    title bigint,
    uniqueid character varying(32),
    email character varying(48),
    timezone character varying(32),
    firstname character varying(32),
    lastname character varying(32),
    superuser boolean
);


ALTER TABLE public.user_t OWNER TO sieve;

--
-- Name: user_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE user_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_t_id_seq OWNER TO sieve;

--
-- Name: user_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE user_t_id_seq OWNED BY user_t.id;


--
-- Name: user_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('user_t_id_seq', 1, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY case_t ALTER COLUMN id SET DEFAULT nextval('case_t_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY child ALTER COLUMN id SET DEFAULT nextval('child_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosis ALTER COLUMN id SET DEFAULT nextval('diagnosis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosisdisorder ALTER COLUMN id SET DEFAULT nextval('diagnosisdisorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosissuggestion ALTER COLUMN id SET DEFAULT nextval('diagnosissuggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY disorder ALTER COLUMN id SET DEFAULT nextval('disorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY disordersuggestion ALTER COLUMN id SET DEFAULT nextval('disordersuggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probe ALTER COLUMN id SET DEFAULT nextval('probe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesource ALTER COLUMN id SET DEFAULT nextval('proberesource_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesult ALTER COLUMN id SET DEFAULT nextval('proberesult_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesultprobetype ALTER COLUMN id SET DEFAULT nextval('proberesultprobetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetreenode ALTER COLUMN id SET DEFAULT nextval('probetreenode_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetype ALTER COLUMN id SET DEFAULT nextval('probetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetypeprobetype ALTER COLUMN id SET DEFAULT nextval('probetypeprobetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetypetag ALTER COLUMN id SET DEFAULT nextval('probetypetag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY school ALTER COLUMN id SET DEFAULT nextval('school_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY session_t ALTER COLUMN id SET DEFAULT nextval('session_t_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY suggestion ALTER COLUMN id SET DEFAULT nextval('suggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY tag ALTER COLUMN id SET DEFAULT nextval('tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY user_t ALTER COLUMN id SET DEFAULT nextval('user_t_id_seq'::regclass);


--
-- Data for Name: case_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY case_t (id, session_c, deleted, childfield) FROM stdin;
\.


--
-- Data for Name: child; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY child (name, id, deleted, pesel, borndate) FROM stdin;
Marta Nowak	1	t	12324	\N
Jola Ula	2	f	123	\N
Rak Martyna	3	f		\N
\.


--
-- Data for Name: diagnosis; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosis (id, deleted, mycase) FROM stdin;
\.


--
-- Data for Name: diagnosisdisorder; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosisdisorder (id, deleted, diagnosis, disorder) FROM stdin;
\.


--
-- Data for Name: diagnosissuggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosissuggestion (id, suggestion, deleted, diagnosis) FROM stdin;
\.


--
-- Data for Name: disorder; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY disorder (name, id, deleted) FROM stdin;
\.


--
-- Data for Name: disordersuggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY disordersuggestion (id, suggestion, deleted, disorder) FROM stdin;
\.


--
-- Data for Name: probe; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probe (result, number_c, id, deleted, mycase, mytype) FROM stdin;
\.


--
-- Data for Name: proberesource; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesource (name, path, id, label, deleted, probetype) FROM stdin;
\.


--
-- Data for Name: proberesult; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesult (name, id, deleted, classname, positive_c) FROM stdin;
w normie	2	f	inNorm	t
poniżej normy	1	f	belowNorm	f
tak	4	f	someColor	f
nie	5	f	someColor	f
wolne	6	f	someColor	f
normalne	7	f	someColor	f
szybkie	8	f	someColor	f
zaliczone	9	f	someColor	f
niezaliczone	10	f	someColor	f
\.


--
-- Data for Name: proberesultprobetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesultprobetype (id, deleted, probetype, proberesult, default_c) FROM stdin;
205	f	1	1	f
206	f	1	2	f
207	f	2	1	f
208	f	2	2	f
209	f	3	1	f
210	f	3	2	f
211	f	5	1	f
212	f	5	2	f
213	f	6	1	f
214	f	6	2	f
215	f	7	1	f
216	f	7	2	f
217	f	8	1	f
218	f	8	2	f
219	f	9	1	f
220	f	9	2	f
221	f	10	1	f
222	f	10	2	f
223	f	11	1	f
224	f	11	2	f
225	f	12	1	f
226	f	12	2	f
227	f	13	4	f
228	f	13	5	f
229	f	14	4	f
230	f	14	5	f
231	f	15	6	f
232	f	15	7	f
233	f	15	8	f
234	f	28	1	f
235	f	28	2	f
236	f	29	1	f
237	f	29	2	f
238	f	27	1	f
239	f	27	2	f
240	f	30	1	f
241	f	30	2	f
242	f	31	1	f
243	f	31	2	f
244	f	32	1	f
245	f	32	2	f
246	f	33	1	f
247	f	33	2	f
248	f	34	1	f
249	f	34	2	f
250	f	35	1	f
251	f	35	2	f
252	f	36	1	f
253	f	36	2	f
254	f	37	1	f
255	f	37	2	f
256	f	38	1	f
257	f	38	2	f
258	f	39	1	f
259	f	39	2	f
260	f	40	1	f
261	f	40	2	f
262	f	41	1	f
263	f	41	2	f
264	f	53	1	f
265	f	53	2	f
266	f	54	1	f
267	f	54	2	f
268	f	42	1	f
269	f	42	2	f
270	f	43	1	f
271	f	43	2	f
272	f	44	1	f
273	f	44	2	f
274	f	45	1	f
275	f	45	2	f
276	f	46	1	f
277	f	46	2	f
278	f	47	1	f
279	f	47	2	f
280	f	48	1	f
281	f	48	2	f
282	f	49	1	f
283	f	49	2	f
284	f	50	1	f
285	f	50	2	f
286	f	51	1	f
287	f	51	2	f
288	f	52	1	f
289	f	52	2	f
290	f	55	9	f
291	f	55	10	f
292	f	56	9	f
293	f	56	10	f
294	f	59	9	f
295	f	59	10	f
296	f	60	9	f
297	f	60	10	f
298	f	61	9	f
299	f	61	10	f
300	f	57	9	f
301	f	57	10	f
302	f	58	9	f
303	f	58	10	f
304	f	62	9	f
305	f	62	10	f
306	f	63	9	f
307	f	63	10	f
308	f	64	9	f
309	f	64	10	f
310	f	65	9	f
311	f	65	10	f
312	f	66	9	f
313	f	66	10	f
314	f	67	9	f
315	f	67	10	f
316	f	68	9	f
317	f	68	10	f
318	f	69	9	f
319	f	69	10	f
\.


--
-- Data for Name: probetreenode; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetreenode (id, deleted, nodeid, probe, childnodeid, probetype) FROM stdin;
\.


--
-- Data for Name: probetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetype (name, id, deleted, initial_c, dependson, listnumber) FROM stdin;
a	28	f	f	27	28
e	29	f	f	27	28
Artykulacja	27	f	t	\N	27
i	30	f	f	27	28
u	31	f	f	27	28
ę	32	f	f	27	28
ą	33	f	f	27	28
y	34	f	f	27	28
p	35	f	f	27	35
b	36	f	f	27	35
w	37	f	f	27	37
wspolruchy	17	f	f	5	5
f	38	f	f	27	37
tiki	18	f	f	5	5
s	39	f	f	27	39
z	40	f	f	27	39
drzenia	19	f	f	5	5
bawienie sie przedmiotami/palacmi	20	f	f	5	5
c	41	f	f	27	39
g	53	f	f	27	51
Motoryka artykulacyjna	54	f	t	\N	54
brak kontaktu wzrokowego	21	f	f	5	5
nosowy glos	22	f	f	6	6
próba jezyka 1	55	f	f	54	55
próba jezyka 2	56	f	f	54	55
próba jezyka 5	59	f	f	54	55
próba jezyka 6	60	f	f	54	55
próba jezyka 7	61	f	f	54	55
próba jezyka 3	57	f	f	54	55
próba jezyka 4	58	f	f	54	55
proba warg 1	62	f	f	54	62
proba warg 2	63	f	f	54	62
ochryply glos	23	f	f	6	6
zbyt wyskoki glos	24	f	f	6	6
proba warg 3	64	f	f	54	62
proba warg 3	65	f	f	54	62
zbyt niski glos	25	f	f	6	6
załamiania głosu	26	f	f	6	6
Słownictwo i formy wypowiedzi	1	f	t	\N	0
Tempo mowy i oddech	2	f	t	\N	0
Zachowania towrzyszace mowieniu	3	f	t	\N	0
Elementy Prozodyczne	5	f	t	\N	0
Fonacja	6	f	t	\N	0
Formy wypoiedzi	7	f	f	1	1
Rozumienie wypowiedzi	8	f	f	1	1
Myslenie przyczynowo skutkowe	9	f	f	1	1
Akcent	10	f	f	2	2
Intonacja	11	f	f	2	2
Rytm	12	f	f	2	2
oddech przyspieszony	13	f	f	3	3
oddech przerywany	14	f	f	3	3
oddech przez usta	15	f	f	3	3
temo mowy	16	f	f	3	3
dz	42	f	f	27	39
sz	43	f	f	27	43
ż	44	f	f	27	43
cz	45	f	f	27	43
dż	46	f	f	27	43
ś	47	f	f	27	47
ź	48	f	f	27	47
ć	49	f	f	27	47
dź	50	f	f	27	47
h	51	f	f	27	51
k	52	f	f	27	51
proba warg 4	66	f	f	54	62
proba warg 5	67	f	f	54	62
proba warg 6	68	f	f	54	62
proba warg 7	69	f	f	54	62
\.


--
-- Data for Name: probetypeprobetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetypeprobetype (id, parentprobe, childprobe) FROM stdin;
1	1	7
2	1	8
3	1	9
4	5	10
5	5	11
6	5	12
7	2	13
8	2	14
9	2	15
10	2	16
11	3	17
12	3	18
13	3	19
14	3	20
15	3	21
16	6	22
17	6	23
18	6	24
19	6	25
20	6	26
21	27	28
22	27	29
23	27	30
24	27	31
25	27	32
26	27	33
27	27	34
28	27	35
29	27	36
30	27	37
31	27	38
32	27	39
33	27	40
34	27	41
35	27	42
36	27	43
37	27	44
38	27	45
39	27	46
40	27	47
41	27	48
42	27	49
43	27	50
44	27	51
45	27	52
46	27	53
47	54	55
48	54	56
49	54	59
50	54	60
51	54	61
52	54	57
53	54	58
54	54	62
55	54	63
56	54	64
57	54	65
58	54	66
59	54	67
60	54	68
61	54	69
\.


--
-- Data for Name: probetypetag; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetypetag (id, tag, probetype) FROM stdin;
1	2	7
2	2	8
3	2	9
4	3	17
5	3	18
6	3	19
7	3	20
8	3	21
9	4	13
10	4	14
11	4	15
12	4	16
13	5	10
14	5	11
15	5	12
16	6	22
17	6	23
18	6	24
19	6	25
20	6	26
21	27	28
22	27	29
23	27	30
24	27	31
25	27	32
26	27	33
27	27	34
28	27	35
29	27	36
30	27	37
31	27	38
32	27	39
33	27	40
34	27	41
35	27	42
36	27	43
37	27	44
38	27	45
39	27	46
40	27	47
41	27	48
42	27	49
43	27	50
44	27	51
45	27	52
46	27	53
47	28	55
48	28	56
49	28	59
50	28	60
51	28	61
52	28	57
53	28	58
54	28	62
55	28	63
56	28	64
57	28	65
58	28	66
59	28	67
60	28	68
61	28	69
62	69	55
63	69	56
64	69	59
65	69	60
66	69	61
67	69	57
68	69	58
69	70	62
70	70	63
71	70	64
72	70	65
73	70	66
74	70	67
75	70	68
76	70	69
77	71	28
78	71	29
79	71	30
80	71	31
81	71	32
82	71	33
83	71	34
84	72	35
85	72	36
86	73	37
87	73	38
88	74	39
89	74	40
90	74	41
91	74	42
92	75	43
93	75	44
94	75	45
95	75	46
96	76	47
97	76	48
98	76	49
99	76	50
100	77	53
101	77	51
102	77	52
\.


--
-- Data for Name: school; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY school (name, id, deleted) FROM stdin;
\.


--
-- Data for Name: session_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY session_t (hash, open_c, id, owner, deleted, creationdate, school, examinedate, ispubliceditable) FROM stdin;
\.


--
-- Data for Name: suggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY suggestion (name, id, description, deleted) FROM stdin;
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY tag (name, id) FROM stdin;
g	1
Słownictwo i formy wypowiedzi	2
Zachowania towrzyszace mowieniu	3
Tempo mowy i oddech	4
Elementy Prozodyczne	5
Fonacja	6
Formy wypoiedzi	7
Rozumienie wypowiedzi	8
Myslenie przyczynowo skutkowe	9
Akcent	10
Intonacja	11
Rytm	12
oddech przyspieszony	13
oddech przerywany	14
oddech przez usta	15
temo mowy	16
wspolruchy	17
tiki	18
drzenia	19
bawienie sie przedmiotami/palacmi	20
brak kontaktu wzrokowego	21
nosowy glos	22
ochryply glos	23
zbyt wyskoki glos	24
zbyt niski glos	25
załamiania głosu	26
Artykulacja	27
Motoryka artykulacyjna	28
a	29
e	30
i	31
u	32
ę	33
ą	34
y	35
p	36
b	37
w	38
f	39
s	40
z	41
c	42
dz	43
sz	44
ż	45
cz	46
dż	47
ś	48
ź	49
ć	50
dź	51
h	52
k	53
próba jezyka 1	54
próba jezyka 2	55
próba jezyka 5	56
próba jezyka 6	57
próba jezyka 7	58
próba jezyka 3	59
próba jezyka 4	60
proba warg 1	61
proba warg 2	62
proba warg 3	63
proba warg 3	64
proba warg 4	65
proba warg 5	66
proba warg 6	67
proba warg 7	68
próba jezyka	69
proba warg	70
samogłoski	71
wargowe	72
wargowo zębowe	73
przednio jezykowo zebowe	74
przednio jezykowo dziąsłowe	75
średnio językowe	76
tylno językowe	77
\.


--
-- Data for Name: user_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY user_t (locale, id, validated, password_pw, password_slt, title, uniqueid, email, timezone, firstname, lastname, superuser) FROM stdin;
pl_PL	1	f	*	T3AT21ZMQNZARB2Y	1	QSP1VNAESAMQE0PSAQ3FFTAOZB5SIBOH		Europe/Warsaw	Default		f
\.


--
-- Name: case_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY case_t
    ADD CONSTRAINT case_t_pk PRIMARY KEY (id);


--
-- Name: child_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY child
    ADD CONSTRAINT child_pk PRIMARY KEY (id);


--
-- Name: diagnosis_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosis
    ADD CONSTRAINT diagnosis_pk PRIMARY KEY (id);


--
-- Name: diagnosisdisorder_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosisdisorder
    ADD CONSTRAINT diagnosisdisorder_pk PRIMARY KEY (id);


--
-- Name: diagnosissuggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosissuggestion
    ADD CONSTRAINT diagnosissuggestion_pk PRIMARY KEY (id);


--
-- Name: disorder_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY disorder
    ADD CONSTRAINT disorder_pk PRIMARY KEY (id);


--
-- Name: disordersuggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY disordersuggestion
    ADD CONSTRAINT disordersuggestion_pk PRIMARY KEY (id);


--
-- Name: probe_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probe
    ADD CONSTRAINT probe_pk PRIMARY KEY (id);


--
-- Name: proberesource_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesource
    ADD CONSTRAINT proberesource_pk PRIMARY KEY (id);


--
-- Name: proberesult_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesult
    ADD CONSTRAINT proberesult_pk PRIMARY KEY (id);


--
-- Name: proberesultprobetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesultprobetype
    ADD CONSTRAINT proberesultprobetype_pk PRIMARY KEY (id);


--
-- Name: probetreenode_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetreenode
    ADD CONSTRAINT probetreenode_pk PRIMARY KEY (id);


--
-- Name: probetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetype
    ADD CONSTRAINT probetype_pk PRIMARY KEY (id);


--
-- Name: probetypeprobetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetypeprobetype
    ADD CONSTRAINT probetypeprobetype_pk PRIMARY KEY (id);


--
-- Name: probetypetag_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetypetag
    ADD CONSTRAINT probetypetag_pk PRIMARY KEY (id);


--
-- Name: school_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY school
    ADD CONSTRAINT school_pk PRIMARY KEY (id);


--
-- Name: session_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY session_t
    ADD CONSTRAINT session_t_pk PRIMARY KEY (id);


--
-- Name: suggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY suggestion
    ADD CONSTRAINT suggestion_pk PRIMARY KEY (id);


--
-- Name: tag_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pk PRIMARY KEY (id);


--
-- Name: user_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY user_t
    ADD CONSTRAINT user_t_pk PRIMARY KEY (id);


--
-- Name: case_t_childfield; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX case_t_childfield ON case_t USING btree (childfield);


--
-- Name: case_t_session_c; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX case_t_session_c ON case_t USING btree (session_c);


--
-- Name: diagnosis_mycase; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosis_mycase ON diagnosis USING btree (mycase);


--
-- Name: diagnosisdisorder_diagnosis; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosisdisorder_diagnosis ON diagnosisdisorder USING btree (diagnosis);


--
-- Name: diagnosisdisorder_disorder; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosisdisorder_disorder ON diagnosisdisorder USING btree (disorder);


--
-- Name: diagnosissuggestion_diagnosis; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosissuggestion_diagnosis ON diagnosissuggestion USING btree (diagnosis);


--
-- Name: diagnosissuggestion_suggestion; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosissuggestion_suggestion ON diagnosissuggestion USING btree (suggestion);


--
-- Name: disordersuggestion_disorder; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX disordersuggestion_disorder ON disordersuggestion USING btree (disorder);


--
-- Name: disordersuggestion_suggestion; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX disordersuggestion_suggestion ON disordersuggestion USING btree (suggestion);


--
-- Name: probe_mycase; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_mycase ON probe USING btree (mycase);


--
-- Name: probe_mytype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_mytype ON probe USING btree (mytype);


--
-- Name: probe_result; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_result ON probe USING btree (result);


--
-- Name: proberesource_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesource_probetype ON proberesource USING btree (probetype);


--
-- Name: proberesultprobetype_proberesult; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesultprobetype_proberesult ON proberesultprobetype USING btree (proberesult);


--
-- Name: proberesultprobetype_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesultprobetype_probetype ON proberesultprobetype USING btree (probetype);


--
-- Name: probetreenode_probe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetreenode_probe ON probetreenode USING btree (probe);


--
-- Name: probetreenode_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetreenode_probetype ON probetreenode USING btree (probetype);


--
-- Name: probetype_dependson; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetype_dependson ON probetype USING btree (dependson);


--
-- Name: probetypeprobetype_childprobe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypeprobetype_childprobe ON probetypeprobetype USING btree (childprobe);


--
-- Name: probetypeprobetype_parentprobe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypeprobetype_parentprobe ON probetypeprobetype USING btree (parentprobe);


--
-- Name: probetypetag_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypetag_probetype ON probetypetag USING btree (probetype);


--
-- Name: probetypetag_tag; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypetag_tag ON probetypetag USING btree (tag);


--
-- Name: session_t_owner; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX session_t_owner ON session_t USING btree (owner);


--
-- Name: session_t_school; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX session_t_school ON session_t USING btree (school);


--
-- Name: user_t_email; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX user_t_email ON user_t USING btree (email);


--
-- Name: user_t_uniqueid; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX user_t_uniqueid ON user_t USING btree (uniqueid);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

