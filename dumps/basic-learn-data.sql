--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: case_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE case_t (
    id bigint NOT NULL,
    session_c bigint,
    deleted boolean,
    childfield bigint
);


ALTER TABLE public.case_t OWNER TO sieve;

--
-- Name: case_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE case_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.case_t_id_seq OWNER TO sieve;

--
-- Name: case_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE case_t_id_seq OWNED BY case_t.id;


--
-- Name: case_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('case_t_id_seq', 42, true);


--
-- Name: child; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE child (
    name character varying(64),
    id bigint NOT NULL,
    deleted boolean,
    pesel character varying(16),
    borndate date
);


ALTER TABLE public.child OWNER TO sieve;

--
-- Name: child_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE child_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.child_id_seq OWNER TO sieve;

--
-- Name: child_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE child_id_seq OWNED BY child.id;


--
-- Name: child_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('child_id_seq', 3, true);


--
-- Name: diagnosis; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosis (
    id bigint NOT NULL,
    deleted boolean,
    mycase bigint
);


ALTER TABLE public.diagnosis OWNER TO sieve;

--
-- Name: diagnosis_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosis_id_seq OWNER TO sieve;

--
-- Name: diagnosis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosis_id_seq OWNED BY diagnosis.id;


--
-- Name: diagnosis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosis_id_seq', 23, true);


--
-- Name: diagnosisdisorder; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosisdisorder (
    id bigint NOT NULL,
    deleted boolean,
    diagnosis bigint,
    disorder bigint
);


ALTER TABLE public.diagnosisdisorder OWNER TO sieve;

--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosisdisorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosisdisorder_id_seq OWNER TO sieve;

--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosisdisorder_id_seq OWNED BY diagnosisdisorder.id;


--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosisdisorder_id_seq', 33, true);


--
-- Name: diagnosissuggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosissuggestion (
    id bigint NOT NULL,
    suggestion bigint,
    deleted boolean,
    diagnosis bigint
);


ALTER TABLE public.diagnosissuggestion OWNER TO sieve;

--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosissuggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosissuggestion_id_seq OWNER TO sieve;

--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosissuggestion_id_seq OWNED BY diagnosissuggestion.id;


--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosissuggestion_id_seq', 12, true);


--
-- Name: disorder; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE disorder (
    name character varying(128),
    id bigint NOT NULL,
    deleted boolean
);


ALTER TABLE public.disorder OWNER TO sieve;

--
-- Name: disorder_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE disorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disorder_id_seq OWNER TO sieve;

--
-- Name: disorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE disorder_id_seq OWNED BY disorder.id;


--
-- Name: disorder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('disorder_id_seq', 7, true);


--
-- Name: disordersuggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE disordersuggestion (
    id bigint NOT NULL,
    suggestion bigint,
    deleted boolean,
    disorder bigint
);


ALTER TABLE public.disordersuggestion OWNER TO sieve;

--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE disordersuggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disordersuggestion_id_seq OWNER TO sieve;

--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE disordersuggestion_id_seq OWNED BY disordersuggestion.id;


--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('disordersuggestion_id_seq', 9, true);


--
-- Name: probe; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probe (
    result bigint,
    number_c integer,
    id bigint NOT NULL,
    deleted boolean,
    mycase bigint,
    mytype bigint
);


ALTER TABLE public.probe OWNER TO sieve;

--
-- Name: probe_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probe_id_seq OWNER TO sieve;

--
-- Name: probe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probe_id_seq OWNED BY probe.id;


--
-- Name: probe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probe_id_seq', 722, true);


--
-- Name: proberesource; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesource (
    name character varying(128),
    path character varying(128),
    id bigint NOT NULL,
    label character varying(128),
    deleted boolean,
    probetype bigint
);


ALTER TABLE public.proberesource OWNER TO sieve;

--
-- Name: proberesource_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesource_id_seq OWNER TO sieve;

--
-- Name: proberesource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesource_id_seq OWNED BY proberesource.id;


--
-- Name: proberesource_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesource_id_seq', 4, true);


--
-- Name: proberesult; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesult (
    name character varying(124),
    id bigint NOT NULL,
    deleted boolean,
    classname character varying(32),
    positive_c boolean
);


ALTER TABLE public.proberesult OWNER TO sieve;

--
-- Name: proberesult_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesult_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesult_id_seq OWNER TO sieve;

--
-- Name: proberesult_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesult_id_seq OWNED BY proberesult.id;


--
-- Name: proberesult_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesult_id_seq', 10, true);


--
-- Name: proberesultprobetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesultprobetype (
    id bigint NOT NULL,
    deleted boolean,
    probetype bigint,
    proberesult bigint,
    default_c boolean
);


ALTER TABLE public.proberesultprobetype OWNER TO sieve;

--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesultprobetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesultprobetype_id_seq OWNER TO sieve;

--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesultprobetype_id_seq OWNED BY proberesultprobetype.id;


--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesultprobetype_id_seq', 343, true);


--
-- Name: probetreenode; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetreenode (
    id bigint NOT NULL,
    deleted boolean,
    nodeid integer,
    probe bigint,
    childnodeid integer,
    probetype bigint
);


ALTER TABLE public.probetreenode OWNER TO sieve;

--
-- Name: probetreenode_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetreenode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetreenode_id_seq OWNER TO sieve;

--
-- Name: probetreenode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetreenode_id_seq OWNED BY probetreenode.id;


--
-- Name: probetreenode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetreenode_id_seq', 75, true);


--
-- Name: probetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetype (
    name character varying(124),
    id bigint NOT NULL,
    deleted boolean,
    initial_c boolean,
    dependson bigint,
    listnumber integer
);


ALTER TABLE public.probetype OWNER TO sieve;

--
-- Name: probetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetype_id_seq OWNER TO sieve;

--
-- Name: probetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetype_id_seq OWNED BY probetype.id;


--
-- Name: probetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetype_id_seq', 70, true);


--
-- Name: probetypeprobetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetypeprobetype (
    id bigint NOT NULL,
    parentprobe bigint,
    childprobe bigint
);


ALTER TABLE public.probetypeprobetype OWNER TO sieve;

--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetypeprobetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetypeprobetype_id_seq OWNER TO sieve;

--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetypeprobetype_id_seq OWNED BY probetypeprobetype.id;


--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetypeprobetype_id_seq', 61, true);


--
-- Name: probetypetag; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetypetag (
    id bigint NOT NULL,
    tag bigint,
    probetype bigint
);


ALTER TABLE public.probetypetag OWNER TO sieve;

--
-- Name: probetypetag_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetypetag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetypetag_id_seq OWNER TO sieve;

--
-- Name: probetypetag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetypetag_id_seq OWNED BY probetypetag.id;


--
-- Name: probetypetag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetypetag_id_seq', 102, true);


--
-- Name: school; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE school (
    name character varying(128),
    id bigint NOT NULL,
    deleted boolean
);


ALTER TABLE public.school OWNER TO sieve;

--
-- Name: school_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.school_id_seq OWNER TO sieve;

--
-- Name: school_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE school_id_seq OWNED BY school.id;


--
-- Name: school_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('school_id_seq', 2, true);


--
-- Name: session_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE session_t (
    hash character varying(64),
    open_c boolean,
    id bigint NOT NULL,
    owner bigint,
    deleted boolean,
    creationdate date,
    school bigint,
    examinedate date,
    ispubliceditable boolean
);


ALTER TABLE public.session_t OWNER TO sieve;

--
-- Name: session_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE session_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.session_t_id_seq OWNER TO sieve;

--
-- Name: session_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE session_t_id_seq OWNED BY session_t.id;


--
-- Name: session_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('session_t_id_seq', 13, true);


--
-- Name: suggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE suggestion (
    name character varying(128),
    id bigint NOT NULL,
    description character varying(1024),
    deleted boolean
);


ALTER TABLE public.suggestion OWNER TO sieve;

--
-- Name: suggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE suggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.suggestion_id_seq OWNER TO sieve;

--
-- Name: suggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE suggestion_id_seq OWNED BY suggestion.id;


--
-- Name: suggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('suggestion_id_seq', 2, true);


--
-- Name: tag; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE tag (
    name character varying(128),
    id bigint NOT NULL
);


ALTER TABLE public.tag OWNER TO sieve;

--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO sieve;

--
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE tag_id_seq OWNED BY tag.id;


--
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('tag_id_seq', 77, true);


--
-- Name: user_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE user_t (
    locale character varying(16),
    id bigint NOT NULL,
    validated boolean,
    password_pw character varying(48),
    password_slt character varying(20),
    title bigint,
    uniqueid character varying(32),
    email character varying(48),
    timezone character varying(32),
    firstname character varying(32),
    lastname character varying(32),
    superuser boolean
);


ALTER TABLE public.user_t OWNER TO sieve;

--
-- Name: user_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE user_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_t_id_seq OWNER TO sieve;

--
-- Name: user_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE user_t_id_seq OWNED BY user_t.id;


--
-- Name: user_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('user_t_id_seq', 1, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY case_t ALTER COLUMN id SET DEFAULT nextval('case_t_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY child ALTER COLUMN id SET DEFAULT nextval('child_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosis ALTER COLUMN id SET DEFAULT nextval('diagnosis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosisdisorder ALTER COLUMN id SET DEFAULT nextval('diagnosisdisorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosissuggestion ALTER COLUMN id SET DEFAULT nextval('diagnosissuggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY disorder ALTER COLUMN id SET DEFAULT nextval('disorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY disordersuggestion ALTER COLUMN id SET DEFAULT nextval('disordersuggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probe ALTER COLUMN id SET DEFAULT nextval('probe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesource ALTER COLUMN id SET DEFAULT nextval('proberesource_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesult ALTER COLUMN id SET DEFAULT nextval('proberesult_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesultprobetype ALTER COLUMN id SET DEFAULT nextval('proberesultprobetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetreenode ALTER COLUMN id SET DEFAULT nextval('probetreenode_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetype ALTER COLUMN id SET DEFAULT nextval('probetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetypeprobetype ALTER COLUMN id SET DEFAULT nextval('probetypeprobetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetypetag ALTER COLUMN id SET DEFAULT nextval('probetypetag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY school ALTER COLUMN id SET DEFAULT nextval('school_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY session_t ALTER COLUMN id SET DEFAULT nextval('session_t_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY suggestion ALTER COLUMN id SET DEFAULT nextval('suggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY tag ALTER COLUMN id SET DEFAULT nextval('tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY user_t ALTER COLUMN id SET DEFAULT nextval('user_t_id_seq'::regclass);


--
-- Data for Name: case_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY case_t (id, session_c, deleted, childfield) FROM stdin;
33	12	f	\N
34	12	f	\N
35	12	f	\N
36	12	f	\N
37	12	f	\N
38	12	f	\N
39	12	f	\N
40	12	f	\N
41	12	f	\N
42	12	f	\N
\.


--
-- Data for Name: child; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY child (name, id, deleted, pesel, borndate) FROM stdin;
Marta Nowak	1	t	12324	\N
Jola Ula	2	f	123	\N
Rak Martyna	3	f		\N
\.


--
-- Data for Name: diagnosis; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosis (id, deleted, mycase) FROM stdin;
12	f	32
13	f	32
14	f	33
15	f	34
16	f	35
17	f	36
18	f	37
19	f	39
20	f	40
21	f	41
22	f	38
23	f	42
\.


--
-- Data for Name: diagnosisdisorder; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosisdisorder (id, deleted, diagnosis, disorder) FROM stdin;
20	f	14	6
21	f	14	7
22	f	15	6
23	f	15	7
24	f	18	6
25	f	18	7
26	f	19	6
27	f	19	7
28	f	20	6
29	f	20	7
30	f	21	6
31	f	21	7
32	f	23	6
33	f	23	7
\.


--
-- Data for Name: diagnosissuggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosissuggestion (id, suggestion, deleted, diagnosis) FROM stdin;
\.


--
-- Data for Name: disorder; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY disorder (name, id, deleted) FROM stdin;
seplenienie międzyzębowe	6	f
kappacyzm	7	f
\.


--
-- Data for Name: disordersuggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY disordersuggestion (id, suggestion, deleted, disorder) FROM stdin;
\.


--
-- Data for Name: probe; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probe (result, number_c, id, deleted, mycase, mytype) FROM stdin;
1	0	218	f	33	1
1	1	219	f	33	2
1	2	220	f	33	3
2	3	221	f	33	5
1	4	222	f	33	6
1	5	223	f	33	7
2	6	224	f	33	8
2	7	225	f	33	9
2	8	226	f	33	10
2	9	227	f	33	11
1	10	228	f	33	12
5	11	229	f	33	13
4	12	230	f	33	14
5	13	231	f	33	15
6	14	232	f	33	16
4	15	233	f	33	22
5	16	234	f	33	23
5	17	235	f	33	24
5	18	236	f	33	25
5	19	237	f	33	26
1	20	238	f	33	27
2	21	239	f	33	28
1	22	240	f	33	29
2	23	241	f	33	30
2	24	242	f	33	31
1	25	243	f	33	32
2	26	244	f	33	33
2	27	245	f	33	34
2	28	246	f	33	35
2	29	247	f	33	36
2	30	248	f	33	37
2	31	249	f	33	38
1	32	250	f	33	39
1	33	251	f	33	40
1	34	252	f	33	41
1	35	253	f	33	42
1	36	254	f	33	43
1	37	255	f	33	44
1	38	256	f	33	45
1	39	257	f	33	46
2	40	258	f	33	47
2	41	259	f	33	48
2	42	260	f	33	49
2	43	261	f	33	50
2	44	262	f	33	53
2	45	263	f	33	51
2	46	264	f	33	52
1	47	265	f	33	54
10	48	266	f	33	55
10	49	267	f	33	56
10	50	268	f	33	59
10	51	269	f	33	60
10	52	270	f	33	61
10	53	271	f	33	57
10	54	272	f	33	58
9	55	273	f	33	62
9	56	274	f	33	63
9	57	275	f	33	64
9	58	276	f	33	65
9	59	277	f	33	66
9	60	278	f	33	67
9	61	279	f	33	68
9	62	280	f	33	69
1	0	281	f	34	1
1	1	282	f	34	2
1	2	283	f	34	3
1	3	284	f	34	5
1	4	285	f	34	6
1	5	286	f	34	7
2	6	287	f	34	8
2	7	288	f	34	9
2	8	289	f	34	10
2	9	290	f	34	11
2	10	291	f	34	12
5	11	292	f	34	13
5	12	293	f	34	14
5	13	294	f	34	15
7	14	295	f	34	16
5	15	296	f	34	17
5	16	297	f	34	18
5	17	298	f	34	19
5	18	299	f	34	20
4	19	300	f	34	21
4	20	301	f	34	22
5	21	302	f	34	23
5	22	303	f	34	24
5	23	304	f	34	25
5	24	305	f	34	26
1	25	306	f	34	27
2	26	307	f	34	28
2	27	308	f	34	29
2	28	309	f	34	30
2	29	310	f	34	31
2	30	311	f	34	32
2	31	312	f	34	33
2	32	313	f	34	34
2	33	314	f	34	35
2	34	315	f	34	36
2	35	316	f	34	37
2	36	317	f	34	38
1	37	318	f	34	39
1	38	319	f	34	40
1	39	320	f	34	41
1	40	321	f	34	42
1	41	322	f	34	43
1	42	323	f	34	44
1	43	324	f	34	45
1	44	325	f	34	46
2	45	326	f	34	47
2	46	327	f	34	48
1	47	328	f	34	49
2	48	329	f	34	50
2	49	330	f	34	53
2	50	331	f	34	51
1	51	332	f	34	52
1	52	333	f	34	54
10	53	334	f	34	55
10	54	335	f	34	56
10	55	336	f	34	59
10	56	337	f	34	60
10	57	338	f	34	61
9	58	339	f	34	57
10	59	340	f	34	58
9	60	341	f	34	62
9	61	342	f	34	63
9	62	343	f	34	64
10	63	344	f	34	65
9	64	345	f	34	66
9	65	346	f	34	67
9	66	347	f	34	68
9	67	348	f	34	69
2	0	349	f	35	1
2	1	350	f	35	2
1	2	351	f	35	3
2	3	352	f	35	5
2	4	353	f	35	6
5	5	354	f	35	13
5	6	355	f	35	14
5	7	356	f	35	15
8	8	357	f	35	16
2	9	358	f	35	27
2	10	359	f	35	54
2	0	360	f	36	1
2	1	361	f	36	2
2	2	362	f	36	3
2	3	363	f	36	5
1	4	364	f	36	6
5	5	365	f	36	22
4	6	366	f	36	23
5	7	367	f	36	24
5	8	368	f	36	25
5	9	369	f	36	26
1	10	370	f	36	27
2	11	371	f	36	28
2	12	372	f	36	29
2	13	373	f	36	30
2	14	374	f	36	31
2	15	375	f	36	32
2	16	376	f	36	33
2	17	377	f	36	34
2	18	378	f	36	35
1	19	379	f	36	36
2	20	380	f	36	37
2	21	381	f	36	38
2	22	382	f	36	39
1	23	383	f	36	40
2	24	384	f	36	41
1	25	385	f	36	42
2	26	386	f	36	43
2	27	387	f	36	44
2	28	388	f	36	45
2	29	389	f	36	46
2	30	390	f	36	47
2	31	391	f	36	48
2	32	392	f	36	49
2	33	393	f	36	50
2	34	394	f	36	53
2	35	395	f	36	51
2	36	396	f	36	52
2	37	397	f	36	54
1	0	398	f	37	1
2	1	399	f	37	2
1	2	400	f	37	3
1	3	401	f	37	5
1	4	402	f	37	6
1	5	403	f	37	7
2	6	404	f	37	8
2	7	405	f	37	9
5	8	406	f	37	13
5	9	407	f	37	14
5	10	408	f	37	15
7	11	409	f	37	16
5	12	410	f	37	17
5	13	411	f	37	18
5	14	412	f	37	19
5	15	413	f	37	20
4	16	414	f	37	21
4	17	415	f	37	22
5	18	416	f	37	23
5	19	417	f	37	24
5	20	418	f	37	25
5	21	419	f	37	26
1	22	420	f	37	27
2	23	421	f	37	28
2	24	422	f	37	29
2	25	423	f	37	30
2	26	424	f	37	31
2	27	425	f	37	32
2	28	426	f	37	33
2	29	427	f	37	34
2	30	428	f	37	35
2	31	429	f	37	36
1	32	430	f	37	37
1	33	431	f	37	38
1	34	432	f	37	39
1	35	433	f	37	40
1	36	434	f	37	41
1	37	435	f	37	42
1	38	436	f	37	43
1	39	437	f	37	44
1	40	438	f	37	45
1	41	439	f	37	46
2	42	440	f	37	47
2	43	441	f	37	48
2	44	442	f	37	49
2	45	443	f	37	50
1	46	444	f	37	53
1	47	445	f	37	51
1	48	446	f	37	52
1	49	447	f	37	54
10	50	448	f	37	55
10	51	449	f	37	56
10	52	450	f	37	59
10	53	451	f	37	60
10	54	452	f	37	61
10	55	453	f	37	57
10	56	454	f	37	58
9	57	455	f	37	62
9	58	456	f	37	63
9	59	457	f	37	64
9	60	458	f	37	65
9	61	459	f	37	66
9	62	460	f	37	67
9	63	461	f	37	68
9	64	462	f	37	69
1	0	463	f	39	1
1	1	464	f	39	2
1	2	465	f	39	3
2	3	466	f	39	5
1	4	467	f	39	6
1	5	468	f	39	7
2	6	469	f	39	8
2	7	470	f	39	9
2	8	471	f	39	10
2	9	472	f	39	11
1	10	473	f	39	12
5	11	474	f	39	13
5	12	475	f	39	14
5	13	476	f	39	15
8	14	477	f	39	16
4	15	478	f	39	22
5	16	479	f	39	23
5	17	480	f	39	24
5	18	481	f	39	25
4	19	482	f	39	26
1	20	483	f	39	27
2	21	484	f	39	28
2	22	485	f	39	29
2	23	486	f	39	30
2	24	487	f	39	31
1	25	488	f	39	32
2	26	489	f	39	33
2	27	490	f	39	34
2	28	491	f	39	35
2	29	492	f	39	36
1	30	493	f	39	37
1	31	494	f	39	38
1	32	495	f	39	39
1	33	496	f	39	40
1	34	497	f	39	41
1	35	498	f	39	42
2	36	499	f	39	43
2	37	500	f	39	44
2	38	501	f	39	45
2	39	502	f	39	46
2	40	503	f	39	47
2	41	504	f	39	48
1	42	505	f	39	49
2	43	506	f	39	50
1	44	507	f	39	53
1	45	508	f	39	51
1	46	509	f	39	52
1	47	510	f	39	54
10	48	511	f	39	55
10	49	512	f	39	56
10	50	513	f	39	59
9	51	514	f	39	60
10	52	515	f	39	61
10	53	516	f	39	57
10	54	517	f	39	58
9	55	518	f	39	62
9	56	519	f	39	63
9	57	520	f	39	64
9	58	521	f	39	65
9	59	522	f	39	66
9	60	523	f	39	67
9	61	524	f	39	68
9	62	525	f	39	69
2	0	526	f	40	\N
1	1	527	f	40	1
1	2	528	f	40	2
1	3	529	f	40	3
2	4	530	f	40	5
1	5	531	f	40	6
1	6	532	f	40	7
2	7	533	f	40	8
2	8	534	f	40	9
2	9	535	f	40	10
1	10	536	f	40	11
2	11	537	f	40	12
5	12	538	f	40	13
5	13	539	f	40	14
5	14	540	f	40	15
6	15	541	f	40	16
4	16	542	f	40	22
4	17	543	f	40	23
5	18	544	f	40	24
5	19	545	f	40	25
5	20	546	f	40	26
1	21	547	f	40	27
2	22	548	f	40	28
2	23	549	f	40	29
2	24	550	f	40	30
2	25	551	f	40	31
1	26	552	f	40	32
2	27	553	f	40	33
2	28	554	f	40	34
2	29	555	f	40	35
2	30	556	f	40	36
1	31	557	f	40	37
1	32	558	f	40	38
1	33	559	f	40	39
1	34	560	f	40	40
2	35	561	f	40	41
1	36	562	f	40	42
1	37	563	f	40	43
1	38	564	f	40	44
1	39	565	f	40	45
2	40	566	f	40	46
2	41	567	f	40	47
1	42	568	f	40	48
2	43	569	f	40	49
2	44	570	f	40	50
1	45	571	f	40	53
2	46	572	f	40	51
1	47	573	f	40	52
1	48	574	f	40	54
9	49	575	f	40	55
10	50	576	f	40	56
10	51	577	f	40	59
10	52	578	f	40	60
10	53	579	f	40	61
9	54	580	f	40	57
10	55	581	f	40	58
9	56	582	f	40	62
9	57	583	f	40	63
9	58	584	f	40	64
9	59	585	f	40	65
9	60	586	f	40	66
9	61	587	f	40	67
9	62	588	f	40	68
10	63	589	f	40	69
1	0	590	f	41	1
1	1	591	f	41	2
1	2	592	f	41	3
1	3	593	f	41	5
1	4	594	f	41	6
1	5	595	f	41	7
2	6	596	f	41	8
1	7	597	f	41	9
2	8	598	f	41	10
1	9	599	f	41	11
2	10	600	f	41	12
5	11	601	f	41	13
5	12	602	f	41	14
4	13	603	f	41	15
8	14	604	f	41	16
5	15	605	f	41	17
4	16	606	f	41	18
5	17	607	f	41	19
5	18	608	f	41	20
4	19	609	f	41	21
4	20	610	f	41	22
5	21	611	f	41	23
5	22	612	f	41	24
4	23	613	f	41	25
5	24	614	f	41	26
1	25	615	f	41	27
2	26	616	f	41	28
1	27	617	f	41	29
2	28	618	f	41	30
2	29	619	f	41	31
2	30	620	f	41	32
1	31	621	f	41	33
2	32	622	f	41	34
2	33	623	f	41	35
2	34	624	f	41	36
1	35	625	f	41	37
1	36	626	f	41	38
2	37	627	f	41	39
1	38	628	f	41	40
1	39	629	f	41	41
2	40	630	f	41	42
2	41	631	f	41	43
1	42	632	f	41	44
2	43	633	f	41	45
1	44	634	f	41	46
1	45	635	f	41	47
2	46	636	f	41	48
1	47	637	f	41	49
2	48	638	f	41	50
1	49	639	f	41	53
1	50	640	f	41	51
1	51	641	f	41	52
1	52	642	f	41	54
10	53	643	f	41	55
10	54	644	f	41	56
9	55	645	f	41	59
10	56	646	f	41	60
9	57	647	f	41	61
10	58	648	f	41	57
10	59	649	f	41	58
9	60	650	f	41	62
9	61	651	f	41	63
9	62	652	f	41	64
9	63	653	f	41	65
9	64	654	f	41	66
10	65	655	f	41	67
9	66	656	f	41	68
9	67	657	f	41	69
1	0	658	f	42	1
2	1	659	f	42	2
1	2	660	f	42	3
1	3	661	f	42	5
1	4	662	f	42	6
1	5	663	f	42	7
2	6	664	f	42	8
2	7	665	f	42	9
5	8	666	f	42	13
5	9	667	f	42	14
4	10	668	f	42	15
7	11	669	f	42	16
5	12	670	f	42	17
5	13	671	f	42	18
5	14	672	f	42	19
5	15	673	f	42	20
4	16	674	f	42	21
4	17	675	f	42	22
5	18	676	f	42	23
5	19	677	f	42	24
5	20	678	f	42	25
5	21	679	f	42	26
1	23	681	f	42	54
10	24	682	f	42	55
10	25	683	f	42	56
9	26	684	f	42	59
10	27	685	f	42	60
9	28	686	f	42	61
10	29	687	f	42	57
10	30	688	f	42	58
1	22	680	f	42	27
1	31	689	f	42	28
2	32	690	f	42	29
2	33	691	f	42	30
2	34	692	f	42	31
2	35	693	f	42	32
2	36	694	f	42	33
2	37	695	f	42	34
2	38	696	f	42	35
2	39	697	f	42	36
1	40	698	f	42	37
2	41	699	f	42	38
1	42	700	f	42	39
1	43	701	f	42	40
1	44	702	f	42	41
1	45	703	f	42	42
2	46	704	f	42	43
2	47	705	f	42	44
1	48	706	f	42	45
2	49	707	f	42	46
1	50	708	f	42	47
1	51	709	f	42	48
1	52	710	f	42	49
1	53	711	f	42	50
1	54	712	f	42	53
1	55	713	f	42	51
1	56	714	f	42	52
10	57	715	f	42	62
9	58	716	f	42	63
9	59	717	f	42	64
9	60	718	f	42	65
9	61	719	f	42	66
9	62	720	f	42	67
9	63	721	f	42	68
9	64	722	f	42	69
\.


--
-- Data for Name: proberesource; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesource (name, path, id, label, deleted, probetype) FROM stdin;
\.


--
-- Data for Name: proberesult; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesult (name, id, deleted, classname, positive_c) FROM stdin;
w normie	2	f	inNorm	t
poniżej normy	1	f	belowNorm	f
tak	4	f	someColor	f
nie	5	f	someColor	f
wolne	6	f	someColor	f
normalne	7	f	someColor	f
szybkie	8	f	someColor	f
zaliczone	9	f	someColor	f
niezaliczone	10	f	someColor	f
\.


--
-- Data for Name: proberesultprobetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesultprobetype (id, deleted, probetype, proberesult, default_c) FROM stdin;
205	f	1	1	f
206	f	1	2	f
207	f	2	1	f
208	f	2	2	f
209	f	3	1	f
210	f	3	2	f
211	f	5	1	f
212	f	5	2	f
213	f	6	1	f
214	f	6	2	f
215	f	7	1	f
216	f	7	2	f
217	f	8	1	f
218	f	8	2	f
219	f	9	1	f
220	f	9	2	f
221	f	10	1	f
222	f	10	2	f
223	f	11	1	f
224	f	11	2	f
225	f	12	1	f
226	f	12	2	f
227	f	13	4	f
229	f	14	4	f
234	f	28	1	f
235	f	28	2	f
236	f	29	1	f
237	f	29	2	f
238	f	27	1	f
239	f	27	2	f
240	f	30	1	f
241	f	30	2	f
242	f	31	1	f
243	f	31	2	f
244	f	32	1	f
245	f	32	2	f
246	f	33	1	f
247	f	33	2	f
248	f	34	1	f
249	f	34	2	f
250	f	35	1	f
251	f	35	2	f
252	f	36	1	f
253	f	36	2	f
254	f	37	1	f
255	f	37	2	f
256	f	38	1	f
257	f	38	2	f
258	f	39	1	f
259	f	39	2	f
260	f	40	1	f
261	f	40	2	f
262	f	41	1	f
263	f	41	2	f
264	f	53	1	f
265	f	53	2	f
266	f	54	1	f
267	f	54	2	f
268	f	42	1	f
269	f	42	2	f
270	f	43	1	f
271	f	43	2	f
272	f	44	1	f
273	f	44	2	f
274	f	45	1	f
275	f	45	2	f
276	f	46	1	f
277	f	46	2	f
278	f	47	1	f
279	f	47	2	f
280	f	48	1	f
281	f	48	2	f
282	f	49	1	f
283	f	49	2	f
284	f	50	1	f
285	f	50	2	f
286	f	51	1	f
287	f	51	2	f
288	f	52	1	f
289	f	52	2	f
291	f	55	10	f
293	f	56	10	f
295	f	59	10	f
297	f	60	10	f
299	f	61	10	f
301	f	57	10	f
303	f	58	10	f
305	f	62	10	f
307	f	63	10	f
309	f	64	10	f
311	f	65	10	f
313	f	66	10	f
315	f	67	10	f
317	f	68	10	f
319	f	69	10	f
231	f	16	6	f
233	f	16	8	f
320	f	15	4	f
290	f	55	9	t
292	f	56	9	t
232	f	16	7	t
228	f	13	5	t
230	f	14	5	t
294	f	59	9	t
322	f	17	4	f
324	f	18	4	f
326	f	19	4	f
328	f	20	4	f
330	f	21	4	f
332	f	22	4	f
334	f	23	4	f
336	f	24	4	f
338	f	25	4	f
340	f	26	4	f
342	f	16	4	f
321	f	15	5	t
323	f	17	5	t
325	f	18	5	t
327	f	19	5	t
329	f	20	5	t
331	f	21	5	t
333	f	22	5	t
335	f	23	5	t
337	f	24	5	t
339	f	25	5	t
341	f	26	5	t
343	f	16	5	t
296	f	60	9	t
298	f	61	9	t
300	f	57	9	t
302	f	58	9	t
304	f	62	9	t
306	f	63	9	t
308	f	64	9	t
310	f	65	9	t
312	f	66	9	t
314	f	67	9	t
316	f	68	9	t
318	f	69	9	t
\.


--
-- Data for Name: probetreenode; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetreenode (id, deleted, nodeid, probe, childnodeid, probetype) FROM stdin;
\.


--
-- Data for Name: probetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetype (name, id, deleted, initial_c, dependson, listnumber) FROM stdin;
a	28	f	f	27	28
e	29	f	f	27	28
Artykulacja	27	f	t	\N	27
i	30	f	f	27	28
u	31	f	f	27	28
ę	32	f	f	27	28
ą	33	f	f	27	28
y	34	f	f	27	28
p	35	f	f	27	35
b	36	f	f	27	35
w	37	f	f	27	37
wspolruchy	17	f	f	5	5
f	38	f	f	27	37
tiki	18	f	f	5	5
s	39	f	f	27	39
z	40	f	f	27	39
drzenia	19	f	f	5	5
bawienie sie przedmiotami/palacmi	20	f	f	5	5
c	41	f	f	27	39
g	53	f	f	27	51
Motoryka artykulacyjna	54	f	t	\N	54
brak kontaktu wzrokowego	21	f	f	5	5
nosowy glos	22	f	f	6	6
próba jezyka 1	55	f	f	54	55
próba jezyka 2	56	f	f	54	55
próba jezyka 5	59	f	f	54	55
próba jezyka 6	60	f	f	54	55
próba jezyka 7	61	f	f	54	55
próba jezyka 3	57	f	f	54	55
próba jezyka 4	58	f	f	54	55
proba warg 1	62	f	f	54	62
proba warg 2	63	f	f	54	62
ochryply glos	23	f	f	6	6
zbyt wyskoki glos	24	f	f	6	6
proba warg 3	64	f	f	54	62
proba warg 3	65	f	f	54	62
zbyt niski glos	25	f	f	6	6
załamiania głosu	26	f	f	6	6
Słownictwo i formy wypowiedzi	1	f	t	\N	0
Tempo mowy i oddech	2	f	t	\N	0
Zachowania towrzyszace mowieniu	3	f	t	\N	0
Elementy Prozodyczne	5	f	t	\N	0
Fonacja	6	f	t	\N	0
Formy wypoiedzi	7	f	f	1	1
Rozumienie wypowiedzi	8	f	f	1	1
Myslenie przyczynowo skutkowe	9	f	f	1	1
Akcent	10	f	f	2	2
Intonacja	11	f	f	2	2
Rytm	12	f	f	2	2
oddech przyspieszony	13	f	f	3	3
oddech przerywany	14	f	f	3	3
oddech przez usta	15	f	f	3	3
temo mowy	16	f	f	3	3
dz	42	f	f	27	39
sz	43	f	f	27	43
ż	44	f	f	27	43
cz	45	f	f	27	43
dż	46	f	f	27	43
ś	47	f	f	27	47
ź	48	f	f	27	47
ć	49	f	f	27	47
dź	50	f	f	27	47
h	51	f	f	27	51
k	52	f	f	27	51
proba warg 4	66	f	f	54	62
proba warg 5	67	f	f	54	62
proba warg 6	68	f	f	54	62
proba warg 7	69	f	f	54	62
\.


--
-- Data for Name: probetypeprobetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetypeprobetype (id, parentprobe, childprobe) FROM stdin;
1	1	7
2	1	8
3	1	9
4	5	10
5	5	11
6	5	12
7	2	13
8	2	14
9	2	15
10	2	16
11	3	17
12	3	18
13	3	19
14	3	20
15	3	21
16	6	22
17	6	23
18	6	24
19	6	25
20	6	26
21	27	28
22	27	29
23	27	30
24	27	31
25	27	32
26	27	33
27	27	34
28	27	35
29	27	36
30	27	37
31	27	38
32	27	39
33	27	40
34	27	41
35	27	42
36	27	43
37	27	44
38	27	45
39	27	46
40	27	47
41	27	48
42	27	49
43	27	50
44	27	51
45	27	52
46	27	53
47	54	55
48	54	56
49	54	59
50	54	60
51	54	61
52	54	57
53	54	58
54	54	62
55	54	63
56	54	64
57	54	65
58	54	66
59	54	67
60	54	68
61	54	69
\.


--
-- Data for Name: probetypetag; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetypetag (id, tag, probetype) FROM stdin;
1	2	7
2	2	8
3	2	9
4	3	17
5	3	18
6	3	19
7	3	20
8	3	21
9	4	13
10	4	14
11	4	15
12	4	16
13	5	10
14	5	11
15	5	12
16	6	22
17	6	23
18	6	24
19	6	25
20	6	26
21	27	28
22	27	29
23	27	30
24	27	31
25	27	32
26	27	33
27	27	34
28	27	35
29	27	36
30	27	37
31	27	38
32	27	39
33	27	40
34	27	41
35	27	42
36	27	43
37	27	44
38	27	45
39	27	46
40	27	47
41	27	48
42	27	49
43	27	50
44	27	51
45	27	52
46	27	53
47	28	55
48	28	56
49	28	59
50	28	60
51	28	61
52	28	57
53	28	58
54	28	62
55	28	63
56	28	64
57	28	65
58	28	66
59	28	67
60	28	68
61	28	69
62	69	55
63	69	56
64	69	59
65	69	60
66	69	61
67	69	57
68	69	58
69	70	62
70	70	63
71	70	64
72	70	65
73	70	66
74	70	67
75	70	68
76	70	69
77	71	28
78	71	29
79	71	30
80	71	31
81	71	32
82	71	33
83	71	34
84	72	35
85	72	36
86	73	37
87	73	38
88	74	39
89	74	40
90	74	41
91	74	42
92	75	43
93	75	44
94	75	45
95	75	46
96	76	47
97	76	48
98	76	49
99	76	50
100	77	53
101	77	51
102	77	52
\.


--
-- Data for Name: school; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY school (name, id, deleted) FROM stdin;
\.


--
-- Data for Name: session_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY session_t (hash, open_c, id, owner, deleted, creationdate, school, examinedate, ispubliceditable) FROM stdin;
u3lEri6t8a4Wv5eQcazuew==	t	12	1	f	2012-12-07	\N	\N	f
TqnL2EqUmJnl/orovFWkMQ==	t	13	1	f	2012-12-07	\N	\N	f
\.


--
-- Data for Name: suggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY suggestion (name, id, description, deleted) FROM stdin;
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY tag (name, id) FROM stdin;
g	1
Słownictwo i formy wypowiedzi	2
Zachowania towrzyszace mowieniu	3
Tempo mowy i oddech	4
Elementy Prozodyczne	5
Fonacja	6
Formy wypoiedzi	7
Rozumienie wypowiedzi	8
Myslenie przyczynowo skutkowe	9
Akcent	10
Intonacja	11
Rytm	12
oddech przyspieszony	13
oddech przerywany	14
oddech przez usta	15
temo mowy	16
wspolruchy	17
tiki	18
drzenia	19
bawienie sie przedmiotami/palacmi	20
brak kontaktu wzrokowego	21
nosowy glos	22
ochryply glos	23
zbyt wyskoki glos	24
zbyt niski glos	25
załamiania głosu	26
Artykulacja	27
Motoryka artykulacyjna	28
a	29
e	30
i	31
u	32
ę	33
ą	34
y	35
p	36
b	37
w	38
f	39
s	40
z	41
c	42
dz	43
sz	44
ż	45
cz	46
dż	47
ś	48
ź	49
ć	50
dź	51
h	52
k	53
próba jezyka 1	54
próba jezyka 2	55
próba jezyka 5	56
próba jezyka 6	57
próba jezyka 7	58
próba jezyka 3	59
próba jezyka 4	60
proba warg 1	61
proba warg 2	62
proba warg 3	63
proba warg 3	64
proba warg 4	65
proba warg 5	66
proba warg 6	67
proba warg 7	68
próba jezyka	69
proba warg	70
samogłoski	71
wargowe	72
wargowo zębowe	73
przednio jezykowo zebowe	74
przednio jezykowo dziąsłowe	75
średnio językowe	76
tylno językowe	77
\.


--
-- Data for Name: user_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY user_t (locale, id, validated, password_pw, password_slt, title, uniqueid, email, timezone, firstname, lastname, superuser) FROM stdin;
pl_PL	1	f	*	T3AT21ZMQNZARB2Y	1	QSP1VNAESAMQE0PSAQ3FFTAOZB5SIBOH		Europe/Warsaw	Default		f
\.


--
-- Name: case_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY case_t
    ADD CONSTRAINT case_t_pk PRIMARY KEY (id);


--
-- Name: child_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY child
    ADD CONSTRAINT child_pk PRIMARY KEY (id);


--
-- Name: diagnosis_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosis
    ADD CONSTRAINT diagnosis_pk PRIMARY KEY (id);


--
-- Name: diagnosisdisorder_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosisdisorder
    ADD CONSTRAINT diagnosisdisorder_pk PRIMARY KEY (id);


--
-- Name: diagnosissuggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosissuggestion
    ADD CONSTRAINT diagnosissuggestion_pk PRIMARY KEY (id);


--
-- Name: disorder_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY disorder
    ADD CONSTRAINT disorder_pk PRIMARY KEY (id);


--
-- Name: disordersuggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY disordersuggestion
    ADD CONSTRAINT disordersuggestion_pk PRIMARY KEY (id);


--
-- Name: probe_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probe
    ADD CONSTRAINT probe_pk PRIMARY KEY (id);


--
-- Name: proberesource_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesource
    ADD CONSTRAINT proberesource_pk PRIMARY KEY (id);


--
-- Name: proberesult_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesult
    ADD CONSTRAINT proberesult_pk PRIMARY KEY (id);


--
-- Name: proberesultprobetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesultprobetype
    ADD CONSTRAINT proberesultprobetype_pk PRIMARY KEY (id);


--
-- Name: probetreenode_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetreenode
    ADD CONSTRAINT probetreenode_pk PRIMARY KEY (id);


--
-- Name: probetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetype
    ADD CONSTRAINT probetype_pk PRIMARY KEY (id);


--
-- Name: probetypeprobetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetypeprobetype
    ADD CONSTRAINT probetypeprobetype_pk PRIMARY KEY (id);


--
-- Name: probetypetag_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetypetag
    ADD CONSTRAINT probetypetag_pk PRIMARY KEY (id);


--
-- Name: school_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY school
    ADD CONSTRAINT school_pk PRIMARY KEY (id);


--
-- Name: session_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY session_t
    ADD CONSTRAINT session_t_pk PRIMARY KEY (id);


--
-- Name: suggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY suggestion
    ADD CONSTRAINT suggestion_pk PRIMARY KEY (id);


--
-- Name: tag_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pk PRIMARY KEY (id);


--
-- Name: user_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY user_t
    ADD CONSTRAINT user_t_pk PRIMARY KEY (id);


--
-- Name: case_t_childfield; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX case_t_childfield ON case_t USING btree (childfield);


--
-- Name: case_t_session_c; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX case_t_session_c ON case_t USING btree (session_c);


--
-- Name: diagnosis_mycase; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosis_mycase ON diagnosis USING btree (mycase);


--
-- Name: diagnosisdisorder_diagnosis; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosisdisorder_diagnosis ON diagnosisdisorder USING btree (diagnosis);


--
-- Name: diagnosisdisorder_disorder; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosisdisorder_disorder ON diagnosisdisorder USING btree (disorder);


--
-- Name: diagnosissuggestion_diagnosis; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosissuggestion_diagnosis ON diagnosissuggestion USING btree (diagnosis);


--
-- Name: diagnosissuggestion_suggestion; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosissuggestion_suggestion ON diagnosissuggestion USING btree (suggestion);


--
-- Name: disordersuggestion_disorder; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX disordersuggestion_disorder ON disordersuggestion USING btree (disorder);


--
-- Name: disordersuggestion_suggestion; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX disordersuggestion_suggestion ON disordersuggestion USING btree (suggestion);


--
-- Name: probe_mycase; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_mycase ON probe USING btree (mycase);


--
-- Name: probe_mytype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_mytype ON probe USING btree (mytype);


--
-- Name: probe_result; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_result ON probe USING btree (result);


--
-- Name: proberesource_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesource_probetype ON proberesource USING btree (probetype);


--
-- Name: proberesultprobetype_proberesult; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesultprobetype_proberesult ON proberesultprobetype USING btree (proberesult);


--
-- Name: proberesultprobetype_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesultprobetype_probetype ON proberesultprobetype USING btree (probetype);


--
-- Name: probetreenode_probe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetreenode_probe ON probetreenode USING btree (probe);


--
-- Name: probetreenode_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetreenode_probetype ON probetreenode USING btree (probetype);


--
-- Name: probetype_dependson; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetype_dependson ON probetype USING btree (dependson);


--
-- Name: probetypeprobetype_childprobe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypeprobetype_childprobe ON probetypeprobetype USING btree (childprobe);


--
-- Name: probetypeprobetype_parentprobe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypeprobetype_parentprobe ON probetypeprobetype USING btree (parentprobe);


--
-- Name: probetypetag_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypetag_probetype ON probetypetag USING btree (probetype);


--
-- Name: probetypetag_tag; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypetag_tag ON probetypetag USING btree (tag);


--
-- Name: session_t_owner; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX session_t_owner ON session_t USING btree (owner);


--
-- Name: session_t_school; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX session_t_school ON session_t USING btree (school);


--
-- Name: user_t_email; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX user_t_email ON user_t USING btree (email);


--
-- Name: user_t_uniqueid; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX user_t_uniqueid ON user_t USING btree (uniqueid);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

