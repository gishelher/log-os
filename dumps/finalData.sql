--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: case_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE case_t (
    id bigint NOT NULL,
    session_c bigint,
    deleted boolean,
    childfield bigint,
    diagnosistext character varying(8192),
    casereviewtext character varying(8192)
);


ALTER TABLE public.case_t OWNER TO sieve;

--
-- Name: case_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE case_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.case_t_id_seq OWNER TO sieve;

--
-- Name: case_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE case_t_id_seq OWNED BY case_t.id;


--
-- Name: child; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE child (
    name character varying(64),
    id bigint NOT NULL,
    deleted boolean,
    pesel character varying(16),
    borndate date
);


ALTER TABLE public.child OWNER TO sieve;

--
-- Name: child_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE child_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.child_id_seq OWNER TO sieve;

--
-- Name: child_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE child_id_seq OWNED BY child.id;


--
-- Name: diagnosis; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosis (
    id bigint NOT NULL,
    deleted boolean,
    mycase bigint
);


ALTER TABLE public.diagnosis OWNER TO sieve;

--
-- Name: diagnosis_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosis_id_seq OWNER TO sieve;

--
-- Name: diagnosis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosis_id_seq OWNED BY diagnosis.id;


--
-- Name: diagnosisdisorder; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosisdisorder (
    id bigint NOT NULL,
    deleted boolean,
    diagnosis bigint,
    disorder bigint
);


ALTER TABLE public.diagnosisdisorder OWNER TO sieve;

--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosisdisorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosisdisorder_id_seq OWNER TO sieve;

--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosisdisorder_id_seq OWNED BY diagnosisdisorder.id;


--
-- Name: diagnosissuggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE diagnosissuggestion (
    id bigint NOT NULL,
    suggestion bigint,
    deleted boolean,
    diagnosis bigint
);


ALTER TABLE public.diagnosissuggestion OWNER TO sieve;

--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE diagnosissuggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagnosissuggestion_id_seq OWNER TO sieve;

--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE diagnosissuggestion_id_seq OWNED BY diagnosissuggestion.id;


--
-- Name: disorder; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE disorder (
    name character varying(128),
    id bigint NOT NULL,
    deleted boolean,
    disordergroup character varying(128)
);


ALTER TABLE public.disorder OWNER TO sieve;

--
-- Name: disorder_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE disorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disorder_id_seq OWNER TO sieve;

--
-- Name: disorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE disorder_id_seq OWNED BY disorder.id;


--
-- Name: disordersuggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE disordersuggestion (
    id bigint NOT NULL,
    suggestion bigint,
    deleted boolean,
    disorder bigint
);


ALTER TABLE public.disordersuggestion OWNER TO sieve;

--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE disordersuggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disordersuggestion_id_seq OWNER TO sieve;

--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE disordersuggestion_id_seq OWNED BY disordersuggestion.id;


--
-- Name: probe; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probe (
    result bigint,
    number_c integer,
    id bigint NOT NULL,
    deleted boolean,
    mycase bigint,
    mytype bigint
);


ALTER TABLE public.probe OWNER TO sieve;

--
-- Name: probe_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probe_id_seq OWNER TO sieve;

--
-- Name: probe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probe_id_seq OWNED BY probe.id;


--
-- Name: proberesource; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesource (
    name character varying(128),
    path character varying(128),
    id bigint NOT NULL,
    label character varying(128),
    deleted boolean,
    probetype bigint
);


ALTER TABLE public.proberesource OWNER TO sieve;

--
-- Name: proberesource_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesource_id_seq OWNER TO sieve;

--
-- Name: proberesource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesource_id_seq OWNED BY proberesource.id;


--
-- Name: proberesult; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesult (
    name character varying(124),
    id bigint NOT NULL,
    deleted boolean,
    classname character varying(32),
    positive_c boolean
);


ALTER TABLE public.proberesult OWNER TO sieve;

--
-- Name: proberesult_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesult_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesult_id_seq OWNER TO sieve;

--
-- Name: proberesult_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesult_id_seq OWNED BY proberesult.id;


--
-- Name: proberesultprobetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE proberesultprobetype (
    id bigint NOT NULL,
    deleted boolean,
    probetype bigint,
    proberesult bigint,
    default_c boolean
);


ALTER TABLE public.proberesultprobetype OWNER TO sieve;

--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE proberesultprobetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proberesultprobetype_id_seq OWNER TO sieve;

--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE proberesultprobetype_id_seq OWNED BY proberesultprobetype.id;


--
-- Name: probetreenode; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetreenode (
    id bigint NOT NULL,
    deleted boolean,
    nodeid integer,
    probe bigint,
    childnodeid integer,
    probetype bigint
);


ALTER TABLE public.probetreenode OWNER TO sieve;

--
-- Name: probetreenode_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetreenode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetreenode_id_seq OWNER TO sieve;

--
-- Name: probetreenode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetreenode_id_seq OWNED BY probetreenode.id;


--
-- Name: probetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetype (
    name character varying(124),
    id bigint NOT NULL,
    deleted boolean,
    initial_c boolean,
    dependson bigint,
    listnumber integer
);


ALTER TABLE public.probetype OWNER TO sieve;

--
-- Name: probetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetype_id_seq OWNER TO sieve;

--
-- Name: probetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetype_id_seq OWNED BY probetype.id;


--
-- Name: probetypeprobetype; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetypeprobetype (
    id bigint NOT NULL,
    parentprobe bigint,
    childprobe bigint
);


ALTER TABLE public.probetypeprobetype OWNER TO sieve;

--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetypeprobetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetypeprobetype_id_seq OWNER TO sieve;

--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetypeprobetype_id_seq OWNED BY probetypeprobetype.id;


--
-- Name: probetypetag; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE probetypetag (
    id bigint NOT NULL,
    tag bigint,
    probetype bigint
);


ALTER TABLE public.probetypetag OWNER TO sieve;

--
-- Name: probetypetag_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE probetypetag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.probetypetag_id_seq OWNER TO sieve;

--
-- Name: probetypetag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE probetypetag_id_seq OWNED BY probetypetag.id;


--
-- Name: school; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE school (
    name character varying(128),
    id bigint NOT NULL,
    deleted boolean
);


ALTER TABLE public.school OWNER TO sieve;

--
-- Name: school_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.school_id_seq OWNER TO sieve;

--
-- Name: school_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE school_id_seq OWNED BY school.id;


--
-- Name: session_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE session_t (
    hash character varying(64),
    open_c boolean,
    id bigint NOT NULL,
    owner bigint,
    deleted boolean,
    creationdate date,
    school bigint,
    examinedate date,
    ispubliceditable boolean
);


ALTER TABLE public.session_t OWNER TO sieve;

--
-- Name: session_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE session_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.session_t_id_seq OWNER TO sieve;

--
-- Name: session_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE session_t_id_seq OWNED BY session_t.id;


--
-- Name: suggestion; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE suggestion (
    name character varying(128),
    id bigint NOT NULL,
    description character varying(1024),
    deleted boolean,
    suggestiongroup character varying(128)
);


ALTER TABLE public.suggestion OWNER TO sieve;

--
-- Name: suggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE suggestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.suggestion_id_seq OWNER TO sieve;

--
-- Name: suggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE suggestion_id_seq OWNED BY suggestion.id;


--
-- Name: tag; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE tag (
    name character varying(128),
    id bigint NOT NULL,
    vectorpos integer
);


ALTER TABLE public.tag OWNER TO sieve;

--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO sieve;

--
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE tag_id_seq OWNED BY tag.id;


--
-- Name: user_t; Type: TABLE; Schema: public; Owner: sieve; Tablespace: 
--

CREATE TABLE user_t (
    locale character varying(16),
    id bigint NOT NULL,
    validated boolean,
    password_pw character varying(48),
    password_slt character varying(20),
    title bigint,
    uniqueid character varying(32),
    email character varying(48),
    timezone character varying(32),
    firstname character varying(32),
    lastname character varying(32),
    superuser boolean
);


ALTER TABLE public.user_t OWNER TO sieve;

--
-- Name: user_t_id_seq; Type: SEQUENCE; Schema: public; Owner: sieve
--

CREATE SEQUENCE user_t_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_t_id_seq OWNER TO sieve;

--
-- Name: user_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sieve
--

ALTER SEQUENCE user_t_id_seq OWNED BY user_t.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY case_t ALTER COLUMN id SET DEFAULT nextval('case_t_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY child ALTER COLUMN id SET DEFAULT nextval('child_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosis ALTER COLUMN id SET DEFAULT nextval('diagnosis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosisdisorder ALTER COLUMN id SET DEFAULT nextval('diagnosisdisorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY diagnosissuggestion ALTER COLUMN id SET DEFAULT nextval('diagnosissuggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY disorder ALTER COLUMN id SET DEFAULT nextval('disorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY disordersuggestion ALTER COLUMN id SET DEFAULT nextval('disordersuggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probe ALTER COLUMN id SET DEFAULT nextval('probe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesource ALTER COLUMN id SET DEFAULT nextval('proberesource_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesult ALTER COLUMN id SET DEFAULT nextval('proberesult_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY proberesultprobetype ALTER COLUMN id SET DEFAULT nextval('proberesultprobetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetreenode ALTER COLUMN id SET DEFAULT nextval('probetreenode_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetype ALTER COLUMN id SET DEFAULT nextval('probetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetypeprobetype ALTER COLUMN id SET DEFAULT nextval('probetypeprobetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY probetypetag ALTER COLUMN id SET DEFAULT nextval('probetypetag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY school ALTER COLUMN id SET DEFAULT nextval('school_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY session_t ALTER COLUMN id SET DEFAULT nextval('session_t_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY suggestion ALTER COLUMN id SET DEFAULT nextval('suggestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY tag ALTER COLUMN id SET DEFAULT nextval('tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sieve
--

ALTER TABLE ONLY user_t ALTER COLUMN id SET DEFAULT nextval('user_t_id_seq'::regclass);


--
-- Data for Name: case_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY case_t (id, session_c, deleted, childfield, diagnosistext, casereviewtext) FROM stdin;
117	13	f	58	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMarianna Idzikowska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">25-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMarianna Idzikowska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">25-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
94	13	f	36		
64	13	f	9		
95	13	f	37		
65	13	f	10		
93	13	f	35		
66	13	f	11		
68	13	f	12		
69	13	f	13		
70	13	f	14		
98	13	f	39	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tJoanna Kr&oacute;l</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">12-2-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tJoanna Kr&oacute;l</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">12-2-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
72	13	f	16		
55	13	f	4		
56	13	f	6		
73	13	f	17		
57	13	f	7		
74	13	f	18		
58	13	f	5		
59	13	f	1		
76	13	f	19		
77	13	f	20		
62	13	f	8		
79	13	f	21		
80	13	f	22		
81	13	f	23		
82	13	f	24		
83	13	f	25		
84	13	f	26		
85	13	f	27		
86	13	f	28		
87	13	f	29		
88	13	f	30		
89	13	f	31		
90	13	f	32		
91	13	f	33		
92	13	f	34		
34	13	f	4	\N	\N
100	13	f	41	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAnita Maciąg</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">25-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAnita Maciąg</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">25-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
101	13	f	42	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tEwelina Szymczak</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">6-5-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tEwelina Szymczak</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">6-5-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
102	13	f	43	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMateusz Sikora</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">2-11-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMateusz Sikora</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">2-11-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
103	13	f	44	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tKornelia Mączyńska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">28-1-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tKornelia Mączyńska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">28-1-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
104	13	f	45	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tOliwia Truszkowska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">15-6-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tOliwia Truszkowska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">15-6-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
105	13	f	46	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tSebastian Gąsiorek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">24-10-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tSebastian Gąsiorek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">24-10-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
106	13	f	47	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMikołaj Pączek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">22-12-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n<p>\n\t&nbsp;</p>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMikołaj Pączek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">22-12-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n<p>\n\t&nbsp;</p>\n
107	13	f	48	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tKatarzyna Stelikowska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">10-4-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tKatarzyna Stelikowska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">10-4-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
108	13	f	49	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tPiotr Doniec</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">30-7-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tPiotr Doniec</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">30-7-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
109	13	f	50	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWeronika Kozak</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">12-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWeronika Kozak</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">12-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
110	13	f	51	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tRobert Pocica</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">25-5-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tRobert Pocica</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">25-5-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
111	13	f	52	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tKamil Lis</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">31-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tKamil Lis</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">31-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
112	13	f	53	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMichał Żarski</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">23-9-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMichał Żarski</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">23-9-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
113	13	f	54	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tIgnacy Cichocki</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">28-10-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tIgnacy Cichocki</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">28-10-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
114	13	f	55	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAntonina Kłos</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">12-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAntonina Kłos</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">12-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
115	13	f	56	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tJulian Koźmiński</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">22-5-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tJulian Koźmiński</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">22-5-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
116	13	f	57	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWioletta Mackiewicz</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">17-8-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWioletta Mackiewicz</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">17-8-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
119	13	f	59	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tZuzanna Watorek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">1-10-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tZuzanna Watorek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">1-10-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
120	13	f	60	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAmelia Pucyńska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">19-2-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAmelia Pucyńska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">19-2-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
121	13	f	61	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tDariusz Chalaba</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">26-6-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tDariusz Chalaba</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">26-6-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
122	13	f	62	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMarta Wislak</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">5-12-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMarta Wislak</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">5-12-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
123	13	f	63	<div style="max-width: 800">\n    <div style="margin-left: 550px; margin-top: 30px">\n        <div>\n            <span id="where">Tuchów</span>,\n            <span>dnia</span>\n            <span id="creation-date">12-12-2012</span>\n        </div>\n        <div>\n            <span>Numer teczki:</span>\n            <span>PPPP-T-421-………/12</span>\n        </div>\n    </div>\n    <div style="margin: 100px 200px 60px 0px; font-size: 20px">\n        <div style="margin-left: -10px; font-weight: bold; font-size: 24px">Opinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n        <div style="font-style: italic; margin: 0px 20px 20px">po przesiewowym badniu mowy</div>\n\n        <div id="child-name">Przemysław Utręba </div>\n        <div>urodzona: <span id="born-date">22-5-2005</span></div>\n        <div>zam. <span id="child-address">Jastrzebia 54</span></div>\n        <div>\n            <div id="school-name">Publiczne Przedszkole w Jastrzębi</div>\n            <div id="school-address">33-191 Jastrzębia 180/A</div>\n        </div>\n        <div>\n            Data badnia:\n            <span id="examinationDate">12-12-2012</span>\n        </div>\n    </div>\n\n    <div class="diagnosis-content">\n        <p>Opinia w sprawie objęcia dziecka pomocą psychologiczno – pedagogiczną na terenie\n            przedszkola w formie zajęć logopedycznych.\n        </p>\n\n        <p>Wyniki badnia logopedycznego:</p>\n        <ul>\n            \n            \n        </ul>\n\n        <p>Zalecenia:</p>\n        <ul>\n            <li>Jakieś zaburzenie</li>\n        </ul>\n    </div>\n    <div> Podstawa prawna:</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>	<div style="max-width: 800">\n    <div style="margin-left: 550px; margin-top: 30px">\n        <div>\n            <span id="where">Tuchów</span>,\n            <span>dnia</span>\n            <span id="creation-date">12-12-2012</span>\n        </div>\n        <div>\n            <span>Numer teczki:</span>\n            <span>PPPP-T-421-………/12</span>\n        </div>\n    </div>\n    <div style="margin: 100px 200px 60px 0px; font-size: 20px">\n        <div style="margin-left: -10px; font-weight: bold; font-size: 24px">Opinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n        <div style="font-style: italic; margin: 0px 20px 20px">po przesiewowym badniu mowy</div>\n\n        <div id="child-name">Przemysław Utręba </div>\n        <div>urodzona: <span id="born-date">22-5-2005</span></div>\n        <div>zam. <span id="child-address">Jastrzebia 54</span></div>\n        <div>\n            <div id="school-name">Publiczne Przedszkole w Jastrzębi</div>\n            <div id="school-address">33-191 Jastrzębia 180/A</div>\n        </div>\n        <div>\n            Data badnia:\n            <span id="examinationDate">12-12-2012</span>\n        </div>\n    </div>\n\n    <div class="diagnosis-content">\n        <p>Opinia w sprawie objęcia dziecka pomocą psychologiczno – pedagogiczną na terenie\n            przedszkola w formie zajęć logopedycznych.\n        </p>\n\n        <p>Wyniki badnia logopedycznego:</p>\n        <ul>\n            \n            \n        </ul>\n\n        <p>Zalecenia:</p>\n        <ul>\n            <li>Jakieś zaburzenie</li>\n        </ul>\n    </div>\n    <div> Podstawa prawna:</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>
124	13	f	64	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWiktor Szoniec</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">11-6-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWiktor Szoniec</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">11-6-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
125	13	f	65	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tStanisława Suchy</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">18-11-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tStanisława Suchy</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">18-11-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
126	13	f	66	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tStefania Maczek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">30-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tStefania Maczek</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">30-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
127	13	f	67	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAgata Żądło</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">14-11-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t\t<li class="disorder-group">\n\t\t\t\t<ul>\n\t\t\t\t\t<li class="disorder-group-item">\n\t\t\t\t\t\tkappacyzm</li>\n\t\t\t\t</ul>\n\t\t\t</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAgata Żądło</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">14-11-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\trotacyzm</li>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t\t<li class="disorder-group">\n\t\t\t\t<ul>\n\t\t\t\t\t<li class="disorder-group-item">\n\t\t\t\t\t\tkappacyzm</li>\n\t\t\t\t</ul>\n\t\t\t</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
128	13	f	68	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMalwina Terlińska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">2-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n<p>\n\t&nbsp;</p>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMalwina Terlińska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">2-3-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n<p>\n\t&nbsp;</p>\n
129	13	f	69	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAnna Grudzień</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">27-7-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tAnna Grudzień</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">27-7-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
130	13	f	70	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tFilip Murczyński</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">8-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tFilip Murczyński</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">8-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
71	13	f	15	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMajka Budzik</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">1-8-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tMajka Budzik</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">1-8-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
132	13	f	71	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tNatalia Grąbczyńska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">13-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tNatalia Grąbczyńska</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">13-12-2005</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
133	13	f	72	<div style="max-width: 800">\n    <div style="margin-left: 550px; margin-top: 30px">\n        <div>\n            <span id="where">Tuchów</span>,\n            <span>dnia</span>\n            <span id="creation-date">12-12-2012</span>\n        </div>\n        <div>\n            <span>Numer teczki:</span>\n            <span>PPPP-T-421-………/12</span>\n        </div>\n    </div>\n    <div style="margin: 100px 200px 60px 0px; font-size: 20px">\n        <div style="margin-left: -10px; font-weight: bold; font-size: 24px">Opinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n        <div style="font-style: italic; margin: 0px 20px 20px">po przesiewowym badniu mowy</div>\n\n        <div id="child-name">Hanna Murawska</div>\n        <div>urodzona: <span id="born-date">9-11-2006</span></div>\n        <div>zam. <span id="child-address">Jastrzebia 54</span></div>\n        <div>\n            <div id="school-name">Publiczne Przedszkole w Jastrzębi</div>\n            <div id="school-address">33-191 Jastrzębia 180/A</div>\n        </div>\n        <div>\n            Data badnia:\n            <span id="examinationDate">12-12-2012</span>\n        </div>\n    </div>\n\n    <div class="diagnosis-content">\n        <p>Opinia w sprawie objęcia dziecka pomocą psychologiczno – pedagogiczną na terenie\n            przedszkola w formie zajęć logopedycznych.\n        </p>\n\n        <p>Wyniki badnia logopedycznego:</p>\n        <ul>\n            \n            \n        </ul>\n\n        <p>Zalecenia:</p>\n        <ul>\n            <li>Jakieś zaburzenie</li>\n        </ul>\n    </div>\n    <div> Podstawa prawna:</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>	<div style="max-width: 800">\n    <div style="margin-left: 550px; margin-top: 30px">\n        <div>\n            <span id="where">Tuchów</span>,\n            <span>dnia</span>\n            <span id="creation-date">12-12-2012</span>\n        </div>\n        <div>\n            <span>Numer teczki:</span>\n            <span>PPPP-T-421-………/12</span>\n        </div>\n    </div>\n    <div style="margin: 100px 200px 60px 0px; font-size: 20px">\n        <div style="margin-left: -10px; font-weight: bold; font-size: 24px">Opinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n        <div style="font-style: italic; margin: 0px 20px 20px">po przesiewowym badniu mowy</div>\n\n        <div id="child-name">Hanna Murawska</div>\n        <div>urodzona: <span id="born-date">9-11-2006</span></div>\n        <div>zam. <span id="child-address">Jastrzebia 54</span></div>\n        <div>\n            <div id="school-name">Publiczne Przedszkole w Jastrzębi</div>\n            <div id="school-address">33-191 Jastrzębia 180/A</div>\n        </div>\n        <div>\n            Data badnia:\n            <span id="examinationDate">12-12-2012</span>\n        </div>\n    </div>\n\n    <div class="diagnosis-content">\n        <p>Opinia w sprawie objęcia dziecka pomocą psychologiczno – pedagogiczną na terenie\n            przedszkola w formie zajęć logopedycznych.\n        </p>\n\n        <p>Wyniki badnia logopedycznego:</p>\n        <ul>\n            \n            \n        </ul>\n\n        <p>Zalecenia:</p>\n        <ul>\n            <li>Jakieś zaburzenie</li>\n        </ul>\n    </div>\n    <div> Podstawa prawna:</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>
134	13	f	73	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWiktor Tetla</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">15-8-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n	<div style="max-width: 800">\n\t<div style="margin-left: 550px; margin-top: 30px">\n\t\t<div>\n\t\t\t<span id="where">Tuch&oacute;w</span>, <span>dnia</span> <span id="creation-date">12-12-2012</span></div>\n\t\t<div>\n\t\t\t<span>Numer teczki:</span> <span>PPPP-T-421-&hellip;&hellip;&hellip;/12</span></div>\n\t</div>\n\t<div style="margin: 100px 200px 60px 0px; font-size: 20px">\n\t\t<div style="margin-left: -10px; font-weight: bold; font-size: 24px">\n\t\t\tOpinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n\t\t<div style="font-style: italic; margin: 0px 20px 20px">\n\t\t\tpo przesiewowym badniu mowy</div>\n\t\t<div id="child-name">\n\t\t\tWiktor Tetla</div>\n\t\t<div>\n\t\t\turodzona: <span id="born-date">15-8-2006</span></div>\n\t\t<div>\n\t\t\tzam. <span id="child-address">Jastrzebia 54</span></div>\n\t\t<div>\n\t\t\t<div id="school-name">\n\t\t\t\tPubliczne Przedszkole w Jastrzębi</div>\n\t\t\t<div id="school-address">\n\t\t\t\t33-191 Jastrzębia 180/A</div>\n\t\t</div>\n\t\t<div>\n\t\t\tData badnia: <span id="examinationDate">12-12-2012</span></div>\n\t</div>\n\t<div class="diagnosis-content">\n\t\t<p>\n\t\t\tOpinia w sprawie objęcia dziecka pomocą psychologiczno &ndash; pedagogiczną na terenie przedszkola w formie zajęć logopedycznych.</p>\n\t\t<p>\n\t\t\tWyniki badnia logopedycznego:</p>\n\t\t<ul>\n\t\t\t<li class="simple-disorder">\n\t\t\t\tszeplenienie</li>\n\t\t</ul>\n\t\t<p>\n\t\t\tZalecenia:</p>\n\t\t<ul>\n\t\t\t<li>\n\t\t\t\tJakieś zaburzenie</li>\n\t\t</ul>\n\t</div>\n\t<div>\n\t\tPodstawa prawna:</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n\t<div>\n\t\tRozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>\n
135	13	f	74	<div style="max-width: 800">\n    <div style="margin-left: 550px; margin-top: 30px">\n        <div>\n            <span id="where">Tuchów</span>,\n            <span>dnia</span>\n            <span id="creation-date">12-12-2012</span>\n        </div>\n        <div>\n            <span>Numer teczki:</span>\n            <span>PPPP-T-421-………/12</span>\n        </div>\n    </div>\n    <div style="margin: 100px 200px 60px 0px; font-size: 20px">\n        <div style="margin-left: -10px; font-weight: bold; font-size: 24px">Opinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n        <div style="font-style: italic; margin: 0px 20px 20px">po przesiewowym badniu mowy</div>\n\n        <div id="child-name">Damian Kuleba</div>\n        <div>urodzona: <span id="born-date">17-9-2005</span></div>\n        <div>zam. <span id="child-address">Jastrzebia 54</span></div>\n        <div>\n            <div id="school-name">Publiczne Przedszkole w Jastrzębi</div>\n            <div id="school-address">33-191 Jastrzębia 180/A</div>\n        </div>\n        <div>\n            Data badnia:\n            <span id="examinationDate">12-12-2012</span>\n        </div>\n    </div>\n\n    <div class="diagnosis-content">\n        <p>Opinia w sprawie objęcia dziecka pomocą psychologiczno – pedagogiczną na terenie\n            przedszkola w formie zajęć logopedycznych.\n        </p>\n\n        <p>Wyniki badnia logopedycznego:</p>\n        <ul>\n            \n            \n        </ul>\n\n        <p>Zalecenia:</p>\n        <ul>\n            <li>Jakieś zaburzenie</li>\n        </ul>\n    </div>\n    <div> Podstawa prawna:</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>	<div style="max-width: 800">\n    <div style="margin-left: 550px; margin-top: 30px">\n        <div>\n            <span id="where">Tuchów</span>,\n            <span>dnia</span>\n            <span id="creation-date">12-12-2012</span>\n        </div>\n        <div>\n            <span>Numer teczki:</span>\n            <span>PPPP-T-421-………/12</span>\n        </div>\n    </div>\n    <div style="margin: 100px 200px 60px 0px; font-size: 20px">\n        <div style="margin-left: -10px; font-weight: bold; font-size: 24px">Opinia Logopedyczna nr <span id="diagnosis-nr">12133/12</span></div>\n        <div style="font-style: italic; margin: 0px 20px 20px">po przesiewowym badniu mowy</div>\n\n        <div id="child-name">Damian Kuleba</div>\n        <div>urodzona: <span id="born-date">17-9-2005</span></div>\n        <div>zam. <span id="child-address">Jastrzebia 54</span></div>\n        <div>\n            <div id="school-name">Publiczne Przedszkole w Jastrzębi</div>\n            <div id="school-address">33-191 Jastrzębia 180/A</div>\n        </div>\n        <div>\n            Data badnia:\n            <span id="examinationDate">12-12-2012</span>\n        </div>\n    </div>\n\n    <div class="diagnosis-content">\n        <p>Opinia w sprawie objęcia dziecka pomocą psychologiczno – pedagogiczną na terenie\n            przedszkola w formie zajęć logopedycznych.\n        </p>\n\n        <p>Wyniki badnia logopedycznego:</p>\n        <ul>\n            \n            \n        </ul>\n\n        <p>Zalecenia:</p>\n        <ul>\n            <li>Jakieś zaburzenie</li>\n        </ul>\n    </div>\n    <div> Podstawa prawna:</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1487).</div>\n    <div>Rozp. MEN z dn. 17.11.2010 r. (Dz. U. z 2010 r. Nr 228, poz. 1488).</div>\n</div>
\.


--
-- Name: case_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('case_t_id_seq', 136, true);


--
-- Data for Name: child; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY child (name, id, deleted, pesel, borndate) FROM stdin;
Marta Nowak	1	t	12324	\N
Jola Ula	2	f	123	\N
Rak Martyna	3	f		\N
Katarzyna Głąb	4	f		\N
Joanna Makowska 	5	f		\N
Mateusz Karpiński	6	f		2006-12-15
Tomasz Partyka	7	f		2006-02-10
Jolanta Pietrzak 	8	f		2006-11-02
Alicja Olearczyk	9	f		2006-10-16
Edyta Pyrkowska	10	f		2005-07-19
Anna Opioła	11	f		2005-02-05
Karolina Kulig	12	f		2006-01-09
Robert Srebro	13	f		2005-05-31
Maria Stus 	14	f		2006-12-25
Majka Budzik	15	f		2006-08-01
Renata Kotuła	16	f		2005-03-16
Magdalena Bodzioch 	17	f		\N
Lucyna Szwedo 	18	f		\N
Kamil Szczęsny	19	f		\N
Bartosz Bzdek	20	f		2006-08-22
Kamila Matłaszek	21	f		2006-09-28
Piotr Pancerz	22	f		2005-08-25
Filip Majewicz	23	f		2006-06-08
Kajetan Płonka	24	f		2005-11-25
Paweł Kossewski	25	f		2006-06-13
Bartłomiej Sikorski 	26	f		2005-05-01
Jan Kulig 	27	f		2006-07-06
Stefan Kijak 	28	f		2006-01-27
Mateusz Radzik	29	f		2006-04-14
Wojciech Bachara	30	f		2006-08-14
Magdalena Oszust	31	f		2006-10-15
Karolina Wołek	32	f		2005-10-31
Kamil Sowa	33	f		2006-05-19
Patryk Kurek	34	f		2006-05-20
Piotr Kubala	35	f		2006-07-19
Konrad Jakubowski	36	f		2006-03-14
Marta Sroka	37	f		2005-04-03
	38	f		2006-02-15
Joanna Król	39	f		2005-02-12
Malwina Stępniak	40	f		2006-05-24
Anita Maciąg	41	f		2006-03-25
Ewelina Szymczak	42	f		2005-05-06
Mateusz Sikora 	43	f		2006-11-02
Kornelia Mączyńska	44	f		2005-01-28
Oliwia Truszkowska	45	f		2006-06-15
Sebastian Gąsiorek	46	f		2005-10-24
Mikołaj Pączek	47	f		2006-12-22
Katarzyna Stelikowska	48	f		2005-04-10
Piotr Doniec	49	f		2006-07-30
Weronika Kozak	50	f		2006-03-12
Robert Pocica	51	f		2005-05-25
Kamil Lis	52	f		2006-03-31
Michał Żarski	53	f		2005-09-23
Ignacy Cichocki	54	f		2006-10-28
Antonina Kłos	55	f		2006-03-12
Julian Koźmiński	56	f		2005-05-22
Wioletta Mackiewicz	57	f		2006-08-17
Marianna Idzikowska	58	f		2005-12-25
Zuzanna Watorek	59	f		2006-10-01
Amelia Pucyńska	60	f		2006-02-19
Dariusz Chalaba	61	f		2005-06-26
Marta Wislak	62	f		2006-12-05
Przemysław Utręba 	63	f		2005-05-22
Wiktor Szoniec	64	f		2005-06-11
Stanisława Suchy	65	f		2006-11-18
Stefania Maczek	66	f		2005-12-30
Agata Żądło	67	f		2005-11-14
Malwina Terlińska	68	f		2006-03-02
Anna Grudzień	69	f		2006-07-27
Filip Murczyński	70	f		2005-12-08
Natalia Grąbczyńska	71	f		2005-12-13
Hanna Murawska	72	f		2006-11-09
Wiktor Tetla	73	f		2006-08-15
Damian Kuleba	74	f		2005-09-17
\.


--
-- Name: child_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('child_id_seq', 74, true);


--
-- Data for Name: diagnosis; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosis (id, deleted, mycase) FROM stdin;
12	f	32
13	f	32
14	f	33
15	f	34
16	f	35
17	f	36
18	f	37
19	f	39
20	f	40
21	f	41
22	f	38
23	f	42
24	f	33
25	f	44
26	f	34
27	f	34
28	f	34
29	f	34
30	f	34
31	f	43
32	f	43
33	f	49
34	f	49
35	f	49
36	f	49
37	f	50
38	f	50
39	f	51
40	f	53
41	f	54
42	f	54
43	f	55
44	f	56
45	f	57
46	f	58
47	f	59
48	f	62
49	f	64
50	f	65
51	f	66
52	f	68
53	f	69
54	f	70
55	f	71
56	f	72
57	f	73
58	f	74
59	f	77
60	f	76
61	f	75
62	f	79
63	f	80
64	f	81
65	f	82
66	f	83
67	f	84
68	f	85
69	f	86
70	f	87
71	f	88
72	f	89
73	f	90
74	f	91
75	f	92
76	f	93
77	f	94
78	f	95
79	f	96
80	f	96
81	f	96
82	f	96
83	f	96
84	f	96
85	f	96
86	f	96
87	f	96
88	f	98
89	f	99
90	f	100
91	f	101
92	f	102
93	f	103
94	f	104
95	f	105
96	f	106
97	f	107
98	f	108
99	f	109
100	f	110
101	f	111
102	f	112
103	f	113
104	f	114
105	f	115
106	f	116
107	f	117
108	f	119
109	f	120
110	f	121
111	f	122
112	f	123
113	f	124
114	f	125
115	f	126
116	f	127
117	f	128
118	f	129
119	f	130
120	f	132
121	f	133
122	f	134
123	f	135
124	f	136
\.


--
-- Name: diagnosis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosis_id_seq', 124, true);


--
-- Data for Name: diagnosisdisorder; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosisdisorder (id, deleted, diagnosis, disorder) FROM stdin;
20	f	14	6
21	f	14	7
22	f	15	6
23	f	15	7
24	f	18	6
25	f	18	7
26	f	19	6
27	f	19	7
28	f	20	6
29	f	20	7
30	f	21	6
31	f	21	7
32	f	23	6
33	f	23	7
34	f	33	6
35	f	43	7
36	f	44	7
37	f	45	7
38	f	46	7
39	f	47	7
40	f	48	7
41	f	48	8
42	f	49	7
43	f	49	6
44	f	50	7
45	f	50	9
46	f	51	7
47	f	52	7
48	f	69	6
49	f	70	6
50	f	71	6
51	f	72	6
52	f	73	6
53	f	74	6
54	f	75	6
55	f	76	6
56	f	77	6
57	f	78	6
58	f	89	8
59	f	90	8
60	f	91	8
61	f	92	8
62	f	93	8
63	f	95	8
64	f	95	9
65	f	96	9
66	f	97	8
67	f	98	8
68	f	98	9
69	f	99	9
70	f	101	8
71	f	102	9
72	f	105	9
73	f	106	8
74	f	109	9
75	f	110	8
76	f	110	9
77	f	111	8
78	f	111	9
79	f	115	9
80	f	116	7
81	f	116	8
82	f	116	9
83	f	122	9
\.


--
-- Name: diagnosisdisorder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosisdisorder_id_seq', 83, true);


--
-- Data for Name: diagnosissuggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY diagnosissuggestion (id, suggestion, deleted, diagnosis) FROM stdin;
\.


--
-- Name: diagnosissuggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('diagnosissuggestion_id_seq', 12, true);


--
-- Data for Name: disorder; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY disorder (name, id, deleted, disordergroup) FROM stdin;
seplenienie międzyzębowe	6	f	\N
kappacyzm	7	f	\N
rotacyzm	8	f	brak
szeplenienie	9	f	brak
\.


--
-- Name: disorder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('disorder_id_seq', 9, true);


--
-- Data for Name: disordersuggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY disordersuggestion (id, suggestion, deleted, disorder) FROM stdin;
\.


--
-- Name: disordersuggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('disordersuggestion_id_seq', 9, true);


--
-- Data for Name: probe; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probe (result, number_c, id, deleted, mycase, mytype) FROM stdin;
1	0	218	f	33	1
1	1	219	f	33	2
1	2	220	f	33	3
2	3	221	f	33	5
1	4	222	f	33	6
1	5	223	f	33	7
2	6	224	f	33	8
2	7	225	f	33	9
2	8	226	f	33	10
2	9	227	f	33	11
1	10	228	f	33	12
5	11	229	f	33	13
4	12	230	f	33	14
5	13	231	f	33	15
6	14	232	f	33	16
4	15	233	f	33	22
5	16	234	f	33	23
5	17	235	f	33	24
5	18	236	f	33	25
5	19	237	f	33	26
1	20	238	f	33	27
2	21	239	f	33	28
1	22	240	f	33	29
2	23	241	f	33	30
2	24	242	f	33	31
1	25	243	f	33	32
2	26	244	f	33	33
2	27	245	f	33	34
2	28	246	f	33	35
2	29	247	f	33	36
2	30	248	f	33	37
2	31	249	f	33	38
1	32	250	f	33	39
1	33	251	f	33	40
1	34	252	f	33	41
1	35	253	f	33	42
1	36	254	f	33	43
1	37	255	f	33	44
1	38	256	f	33	45
1	39	257	f	33	46
2	40	258	f	33	47
2	41	259	f	33	48
2	42	260	f	33	49
2	43	261	f	33	50
2	44	262	f	33	53
2	45	263	f	33	51
2	46	264	f	33	52
1	47	265	f	33	54
10	48	266	f	33	55
10	49	267	f	33	56
10	50	268	f	33	59
10	51	269	f	33	60
10	52	270	f	33	61
10	53	271	f	33	57
10	54	272	f	33	58
9	55	273	f	33	62
9	56	274	f	33	63
9	57	275	f	33	64
9	58	276	f	33	65
9	59	277	f	33	66
9	60	278	f	33	67
9	61	279	f	33	68
9	62	280	f	33	69
1	0	281	f	34	1
1	1	282	f	34	2
1	2	283	f	34	3
1	3	284	f	34	5
1	4	285	f	34	6
1	5	286	f	34	7
2	6	287	f	34	8
2	7	288	f	34	9
2	8	289	f	34	10
2	9	290	f	34	11
2	10	291	f	34	12
5	11	292	f	34	13
5	12	293	f	34	14
5	13	294	f	34	15
7	14	295	f	34	16
5	15	296	f	34	17
5	16	297	f	34	18
5	17	298	f	34	19
5	18	299	f	34	20
4	19	300	f	34	21
4	20	301	f	34	22
5	21	302	f	34	23
5	22	303	f	34	24
5	23	304	f	34	25
5	24	305	f	34	26
1	25	306	f	34	27
2	26	307	f	34	28
2	27	308	f	34	29
2	28	309	f	34	30
2	29	310	f	34	31
2	30	311	f	34	32
2	31	312	f	34	33
2	32	313	f	34	34
2	33	314	f	34	35
2	34	315	f	34	36
2	35	316	f	34	37
2	36	317	f	34	38
1	37	318	f	34	39
1	38	319	f	34	40
1	39	320	f	34	41
1	40	321	f	34	42
1	41	322	f	34	43
1	42	323	f	34	44
1	43	324	f	34	45
1	44	325	f	34	46
2	45	326	f	34	47
2	46	327	f	34	48
1	47	328	f	34	49
2	48	329	f	34	50
2	49	330	f	34	53
2	50	331	f	34	51
1	51	332	f	34	52
1	52	333	f	34	54
10	53	334	f	34	55
10	54	335	f	34	56
10	55	336	f	34	59
10	56	337	f	34	60
10	57	338	f	34	61
9	58	339	f	34	57
10	59	340	f	34	58
9	60	341	f	34	62
9	61	342	f	34	63
9	62	343	f	34	64
10	63	344	f	34	65
9	64	345	f	34	66
9	65	346	f	34	67
9	66	347	f	34	68
9	67	348	f	34	69
2	0	349	f	35	1
2	1	350	f	35	2
1	2	351	f	35	3
2	3	352	f	35	5
2	4	353	f	35	6
5	5	354	f	35	13
5	6	355	f	35	14
5	7	356	f	35	15
8	8	357	f	35	16
2	9	358	f	35	27
2	10	359	f	35	54
2	0	360	f	36	1
2	1	361	f	36	2
2	2	362	f	36	3
2	3	363	f	36	5
1	4	364	f	36	6
5	5	365	f	36	22
4	6	366	f	36	23
5	7	367	f	36	24
5	8	368	f	36	25
5	9	369	f	36	26
1	10	370	f	36	27
2	11	371	f	36	28
2	12	372	f	36	29
2	13	373	f	36	30
2	14	374	f	36	31
2	15	375	f	36	32
2	16	376	f	36	33
2	17	377	f	36	34
2	18	378	f	36	35
1	19	379	f	36	36
2	20	380	f	36	37
2	21	381	f	36	38
2	22	382	f	36	39
1	23	383	f	36	40
2	24	384	f	36	41
1	25	385	f	36	42
2	26	386	f	36	43
2	27	387	f	36	44
2	28	388	f	36	45
2	29	389	f	36	46
2	30	390	f	36	47
2	31	391	f	36	48
2	32	392	f	36	49
2	33	393	f	36	50
2	34	394	f	36	53
2	35	395	f	36	51
2	36	396	f	36	52
2	37	397	f	36	54
1	0	398	f	37	1
2	1	399	f	37	2
1	2	400	f	37	3
1	3	401	f	37	5
1	4	402	f	37	6
1	5	403	f	37	7
2	6	404	f	37	8
2	7	405	f	37	9
5	8	406	f	37	13
5	9	407	f	37	14
5	10	408	f	37	15
7	11	409	f	37	16
5	12	410	f	37	17
5	13	411	f	37	18
5	14	412	f	37	19
5	15	413	f	37	20
4	16	414	f	37	21
4	17	415	f	37	22
5	18	416	f	37	23
5	19	417	f	37	24
5	20	418	f	37	25
5	21	419	f	37	26
1	22	420	f	37	27
2	23	421	f	37	28
2	24	422	f	37	29
2	25	423	f	37	30
2	26	424	f	37	31
2	27	425	f	37	32
2	28	426	f	37	33
2	29	427	f	37	34
2	30	428	f	37	35
2	31	429	f	37	36
1	32	430	f	37	37
1	33	431	f	37	38
1	34	432	f	37	39
1	35	433	f	37	40
1	36	434	f	37	41
1	37	435	f	37	42
1	38	436	f	37	43
1	39	437	f	37	44
1	40	438	f	37	45
1	41	439	f	37	46
2	42	440	f	37	47
2	43	441	f	37	48
2	44	442	f	37	49
2	45	443	f	37	50
1	46	444	f	37	53
1	47	445	f	37	51
1	48	446	f	37	52
1	49	447	f	37	54
10	50	448	f	37	55
10	51	449	f	37	56
10	52	450	f	37	59
10	53	451	f	37	60
10	54	452	f	37	61
10	55	453	f	37	57
10	56	454	f	37	58
9	57	455	f	37	62
9	58	456	f	37	63
9	59	457	f	37	64
9	60	458	f	37	65
9	61	459	f	37	66
9	62	460	f	37	67
9	63	461	f	37	68
9	64	462	f	37	69
1	0	463	f	39	1
1	1	464	f	39	2
1	2	465	f	39	3
2	3	466	f	39	5
1	4	467	f	39	6
1	5	468	f	39	7
2	6	469	f	39	8
2	7	470	f	39	9
2	8	471	f	39	10
2	9	472	f	39	11
1	10	473	f	39	12
5	11	474	f	39	13
5	12	475	f	39	14
5	13	476	f	39	15
8	14	477	f	39	16
4	15	478	f	39	22
5	16	479	f	39	23
5	17	480	f	39	24
5	18	481	f	39	25
4	19	482	f	39	26
1	20	483	f	39	27
2	21	484	f	39	28
2	22	485	f	39	29
2	23	486	f	39	30
2	24	487	f	39	31
1	25	488	f	39	32
2	26	489	f	39	33
2	27	490	f	39	34
2	28	491	f	39	35
2	29	492	f	39	36
1	30	493	f	39	37
1	31	494	f	39	38
1	32	495	f	39	39
1	33	496	f	39	40
1	34	497	f	39	41
1	35	498	f	39	42
2	36	499	f	39	43
2	37	500	f	39	44
2	38	501	f	39	45
2	39	502	f	39	46
2	40	503	f	39	47
2	41	504	f	39	48
1	42	505	f	39	49
2	43	506	f	39	50
1	44	507	f	39	53
1	45	508	f	39	51
1	46	509	f	39	52
1	47	510	f	39	54
10	48	511	f	39	55
10	49	512	f	39	56
10	50	513	f	39	59
9	51	514	f	39	60
10	52	515	f	39	61
10	53	516	f	39	57
10	54	517	f	39	58
9	55	518	f	39	62
9	56	519	f	39	63
9	57	520	f	39	64
9	58	521	f	39	65
9	59	522	f	39	66
9	60	523	f	39	67
9	61	524	f	39	68
9	62	525	f	39	69
2	0	526	f	40	\N
1	1	527	f	40	1
1	2	528	f	40	2
1	3	529	f	40	3
2	4	530	f	40	5
1	5	531	f	40	6
1	6	532	f	40	7
2	7	533	f	40	8
2	8	534	f	40	9
2	9	535	f	40	10
1	10	536	f	40	11
2	11	537	f	40	12
5	12	538	f	40	13
5	13	539	f	40	14
5	14	540	f	40	15
6	15	541	f	40	16
4	16	542	f	40	22
4	17	543	f	40	23
5	18	544	f	40	24
5	19	545	f	40	25
5	20	546	f	40	26
1	21	547	f	40	27
2	22	548	f	40	28
2	23	549	f	40	29
2	24	550	f	40	30
2	25	551	f	40	31
1	26	552	f	40	32
2	27	553	f	40	33
2	28	554	f	40	34
2	29	555	f	40	35
2	30	556	f	40	36
1	31	557	f	40	37
1	32	558	f	40	38
1	33	559	f	40	39
1	34	560	f	40	40
2	35	561	f	40	41
1	36	562	f	40	42
1	37	563	f	40	43
1	38	564	f	40	44
1	39	565	f	40	45
2	40	566	f	40	46
2	41	567	f	40	47
1	42	568	f	40	48
2	43	569	f	40	49
2	44	570	f	40	50
1	45	571	f	40	53
2	46	572	f	40	51
1	47	573	f	40	52
1	48	574	f	40	54
9	49	575	f	40	55
10	50	576	f	40	56
10	51	577	f	40	59
10	52	578	f	40	60
10	53	579	f	40	61
9	54	580	f	40	57
10	55	581	f	40	58
9	56	582	f	40	62
9	57	583	f	40	63
9	58	584	f	40	64
9	59	585	f	40	65
9	60	586	f	40	66
9	61	587	f	40	67
9	62	588	f	40	68
10	63	589	f	40	69
1	0	590	f	41	1
1	1	591	f	41	2
1	2	592	f	41	3
1	3	593	f	41	5
1	4	594	f	41	6
1	5	595	f	41	7
2	6	596	f	41	8
1	7	597	f	41	9
2	8	598	f	41	10
1	9	599	f	41	11
2	10	600	f	41	12
5	11	601	f	41	13
5	12	602	f	41	14
4	13	603	f	41	15
8	14	604	f	41	16
5	15	605	f	41	17
4	16	606	f	41	18
5	17	607	f	41	19
5	18	608	f	41	20
4	19	609	f	41	21
4	20	610	f	41	22
5	21	611	f	41	23
5	22	612	f	41	24
4	23	613	f	41	25
5	24	614	f	41	26
1	25	615	f	41	27
2	26	616	f	41	28
1	27	617	f	41	29
2	28	618	f	41	30
2	29	619	f	41	31
2	30	620	f	41	32
1	31	621	f	41	33
2	32	622	f	41	34
2	33	623	f	41	35
2	34	624	f	41	36
1	35	625	f	41	37
1	36	626	f	41	38
2	37	627	f	41	39
1	38	628	f	41	40
1	39	629	f	41	41
2	40	630	f	41	42
2	41	631	f	41	43
1	42	632	f	41	44
2	43	633	f	41	45
1	44	634	f	41	46
1	45	635	f	41	47
2	46	636	f	41	48
1	47	637	f	41	49
2	48	638	f	41	50
1	49	639	f	41	53
1	50	640	f	41	51
1	51	641	f	41	52
1	52	642	f	41	54
10	53	643	f	41	55
10	54	644	f	41	56
9	55	645	f	41	59
10	56	646	f	41	60
9	57	647	f	41	61
10	58	648	f	41	57
10	59	649	f	41	58
9	60	650	f	41	62
9	61	651	f	41	63
9	62	652	f	41	64
9	63	653	f	41	65
9	64	654	f	41	66
10	65	655	f	41	67
9	66	656	f	41	68
9	67	657	f	41	69
1	0	658	f	42	1
2	1	659	f	42	2
1	2	660	f	42	3
1	3	661	f	42	5
1	4	662	f	42	6
1	5	663	f	42	7
2	6	664	f	42	8
2	7	665	f	42	9
5	8	666	f	42	13
5	9	667	f	42	14
4	10	668	f	42	15
7	11	669	f	42	16
5	12	670	f	42	17
5	13	671	f	42	18
5	14	672	f	42	19
5	15	673	f	42	20
4	16	674	f	42	21
4	17	675	f	42	22
5	18	676	f	42	23
5	19	677	f	42	24
5	20	678	f	42	25
5	21	679	f	42	26
1	23	681	f	42	54
10	24	682	f	42	55
10	25	683	f	42	56
9	26	684	f	42	59
10	27	685	f	42	60
9	28	686	f	42	61
10	29	687	f	42	57
10	30	688	f	42	58
1	22	680	f	42	27
1	31	689	f	42	28
2	32	690	f	42	29
2	33	691	f	42	30
2	34	692	f	42	31
2	35	693	f	42	32
2	36	694	f	42	33
2	37	695	f	42	34
2	38	696	f	42	35
2	39	697	f	42	36
1	40	698	f	42	37
2	41	699	f	42	38
1	42	700	f	42	39
1	43	701	f	42	40
1	44	702	f	42	41
1	45	703	f	42	42
2	46	704	f	42	43
2	47	705	f	42	44
1	48	706	f	42	45
2	49	707	f	42	46
1	50	708	f	42	47
1	51	709	f	42	48
1	52	710	f	42	49
1	53	711	f	42	50
1	54	712	f	42	53
1	55	713	f	42	51
1	56	714	f	42	52
10	57	715	f	42	62
9	58	716	f	42	63
9	59	717	f	42	64
9	60	718	f	42	65
9	61	719	f	42	66
9	62	720	f	42	67
9	63	721	f	42	68
9	64	722	f	42	69
1	0	723	f	52	1
2	1	724	f	52	2
2	2	725	f	52	3
2	3	726	f	52	5
2	4	727	f	52	6
2	0	728	f	53	1
2	1	729	f	53	2
1	2	730	f	53	3
2	3	731	f	53	5
2	4	732	f	53	6
4	5	733	f	53	13
5	6	734	f	53	14
4	7	735	f	53	15
8	8	736	f	53	16
2	9	737	f	53	27
1	10	738	f	53	54
9	11	739	f	53	55
10	12	740	f	53	56
10	13	741	f	53	59
9	14	742	f	53	60
10	15	743	f	53	61
9	16	744	f	53	57
9	17	745	f	53	58
9	18	746	f	53	62
10	19	747	f	53	63
9	20	748	f	53	64
9	21	749	f	53	65
9	22	750	f	53	66
9	23	751	f	53	67
10	24	752	f	53	68
9	25	753	f	53	69
2	0	754	f	54	1
2	1	755	f	54	2
2	2	756	f	54	3
1	3	757	f	54	5
1	4	758	f	54	6
4	5	759	f	54	17
5	6	760	f	54	18
5	7	761	f	54	19
4	8	762	f	54	20
4	9	763	f	54	21
4	10	764	f	54	22
5	11	765	f	54	23
5	12	766	f	54	24
4	13	767	f	54	25
5	14	768	f	54	26
1	15	769	f	54	27
2	16	770	f	54	28
2	17	771	f	54	29
2	18	772	f	54	30
2	19	773	f	54	31
1	20	774	f	54	32
1	21	775	f	54	33
1	22	776	f	54	34
2	23	777	f	54	35
1	24	778	f	54	36
2	25	779	f	54	37
2	26	780	f	54	38
2	27	781	f	54	39
2	28	782	f	54	40
2	29	783	f	54	41
1	30	784	f	54	42
2	31	785	f	54	43
2	32	786	f	54	44
2	33	787	f	54	45
1	34	788	f	54	46
2	35	789	f	54	47
2	36	790	f	54	48
2	37	791	f	54	49
1	38	792	f	54	50
1	39	793	f	54	53
2	40	794	f	54	51
2	41	795	f	54	52
1	42	796	f	54	54
9	43	797	f	54	55
9	44	798	f	54	56
9	45	799	f	54	59
10	46	800	f	54	60
10	47	801	f	54	61
10	48	802	f	54	57
9	49	803	f	54	58
9	50	804	f	54	62
9	51	805	f	54	63
9	52	806	f	54	64
9	53	807	f	54	65
9	54	808	f	54	66
9	55	809	f	54	67
9	56	810	f	54	68
9	57	811	f	54	69
1	0	812	f	55	1
2	1	813	f	55	2
2	2	814	f	55	3
1	3	815	f	55	5
1	4	816	f	55	6
2	5	817	f	55	7
2	6	818	f	55	8
2	7	819	f	55	9
4	8	820	f	55	17
5	9	821	f	55	18
5	10	822	f	55	19
4	11	823	f	55	20
5	12	824	f	55	21
5	13	825	f	55	22
5	14	826	f	55	23
5	15	827	f	55	24
5	16	828	f	55	25
4	17	829	f	55	26
1	18	830	f	55	27
2	19	831	f	55	28
2	20	832	f	55	29
2	21	833	f	55	30
2	22	834	f	55	31
2	23	835	f	55	32
2	24	836	f	55	33
2	25	837	f	55	34
2	26	838	f	55	35
2	27	839	f	55	36
2	28	840	f	55	37
2	29	841	f	55	38
2	30	842	f	55	39
2	31	843	f	55	40
2	32	844	f	55	41
2	33	845	f	55	42
2	34	846	f	55	43
2	35	847	f	55	44
2	36	848	f	55	45
2	37	849	f	55	46
2	38	850	f	55	47
2	39	851	f	55	48
2	40	852	f	55	49
2	41	853	f	55	50
2	42	854	f	55	53
2	43	855	f	55	51
1	44	856	f	55	52
1	45	857	f	55	54
9	46	858	f	55	55
9	47	859	f	55	56
9	48	860	f	55	59
10	49	861	f	55	60
9	50	862	f	55	61
9	51	863	f	55	57
10	52	864	f	55	58
9	53	865	f	55	62
9	54	866	f	55	63
9	55	867	f	55	64
9	56	868	f	55	65
9	57	869	f	55	66
9	58	870	f	55	67
9	59	871	f	55	68
10	60	872	f	55	69
1	0	873	f	56	1
2	1	874	f	56	2
2	2	875	f	56	3
2	3	876	f	56	5
1	4	877	f	56	6
2	5	878	f	56	7
2	6	879	f	56	8
1	7	880	f	56	9
4	8	881	f	56	22
5	9	882	f	56	23
5	10	883	f	56	24
5	11	884	f	56	25
5	12	885	f	56	26
1	13	886	f	56	27
2	14	887	f	56	28
2	15	888	f	56	29
2	16	889	f	56	30
2	17	890	f	56	31
2	18	891	f	56	32
2	19	892	f	56	33
2	20	893	f	56	34
2	21	894	f	56	35
2	22	895	f	56	36
2	23	896	f	56	37
2	24	897	f	56	38
2	25	898	f	56	39
2	26	899	f	56	40
2	27	900	f	56	41
2	28	901	f	56	42
2	29	902	f	56	43
2	30	903	f	56	44
2	31	904	f	56	45
2	32	905	f	56	46
2	33	906	f	56	47
2	34	907	f	56	48
2	35	908	f	56	49
2	36	909	f	56	50
2	37	910	f	56	53
2	38	911	f	56	51
1	39	912	f	56	52
2	40	913	f	56	54
2	0	914	f	57	1
2	1	915	f	57	2
2	2	916	f	57	3
2	3	917	f	57	5
1	4	918	f	57	6
5	5	919	f	57	22
5	6	920	f	57	23
5	7	921	f	57	24
5	8	922	f	57	25
4	9	923	f	57	26
1	10	924	f	57	27
2	11	925	f	57	28
2	12	926	f	57	29
2	13	927	f	57	30
2	14	928	f	57	31
2	15	929	f	57	32
2	16	930	f	57	33
2	17	931	f	57	34
2	18	932	f	57	35
2	19	933	f	57	36
2	20	934	f	57	37
2	21	935	f	57	38
2	22	936	f	57	39
2	23	937	f	57	40
2	24	938	f	57	41
2	25	939	f	57	42
2	26	940	f	57	43
2	27	941	f	57	44
2	28	942	f	57	45
2	29	943	f	57	46
2	30	944	f	57	47
2	31	945	f	57	48
2	32	946	f	57	49
2	33	947	f	57	50
2	34	948	f	57	53
2	35	949	f	57	51
1	36	950	f	57	52
1	37	951	f	57	54
9	38	952	f	57	55
9	39	953	f	57	56
9	40	954	f	57	59
10	41	955	f	57	60
9	42	956	f	57	61
9	43	957	f	57	57
9	44	958	f	57	58
9	45	959	f	57	62
9	46	960	f	57	63
9	47	961	f	57	64
9	48	962	f	57	65
9	49	963	f	57	66
9	50	964	f	57	67
10	51	965	f	57	68
9	52	966	f	57	69
2	0	967	f	58	1
1	1	968	f	58	2
2	2	969	f	58	3
2	3	970	f	58	5
2	4	971	f	58	6
1	5	972	f	58	10
2	6	973	f	58	11
2	7	974	f	58	12
1	8	975	f	58	27
2	9	976	f	58	28
2	10	977	f	58	29
2	11	978	f	58	30
2	12	979	f	58	31
2	13	980	f	58	32
2	14	981	f	58	33
2	15	982	f	58	34
2	16	983	f	58	35
2	17	984	f	58	36
2	18	985	f	58	37
2	19	986	f	58	38
2	20	987	f	58	39
2	21	988	f	58	40
2	22	989	f	58	41
2	23	990	f	58	42
2	24	991	f	58	43
2	25	992	f	58	44
2	26	993	f	58	45
2	27	994	f	58	46
2	28	995	f	58	47
2	29	996	f	58	48
2	30	997	f	58	49
2	31	998	f	58	50
1	32	999	f	58	53
2	33	1000	f	58	51
1	34	1001	f	58	52
2	35	1002	f	58	54
2	0	1003	f	59	1
2	1	1004	f	59	2
1	2	1005	f	59	3
2	3	1006	f	59	5
1	4	1007	f	59	6
5	5	1008	f	59	13
5	6	1009	f	59	14
5	7	1010	f	59	15
6	8	1011	f	59	16
5	9	1012	f	59	22
5	10	1013	f	59	23
4	11	1014	f	59	24
5	12	1015	f	59	25
5	13	1016	f	59	26
1	14	1017	f	59	27
2	15	1018	f	59	28
2	16	1019	f	59	29
2	17	1020	f	59	30
2	18	1021	f	59	31
2	19	1022	f	59	32
1	20	1023	f	59	33
2	21	1024	f	59	34
2	22	1025	f	59	35
2	23	1026	f	59	36
2	24	1027	f	59	37
2	25	1028	f	59	38
2	26	1029	f	59	39
2	27	1030	f	59	40
2	28	1031	f	59	41
2	29	1032	f	59	42
2	30	1033	f	59	43
2	31	1034	f	59	44
2	32	1035	f	59	45
2	33	1036	f	59	46
2	34	1037	f	59	47
2	35	1038	f	59	48
2	36	1039	f	59	49
2	37	1040	f	59	50
2	38	1041	f	59	53
2	39	1042	f	59	51
1	40	1043	f	59	52
1	41	1044	f	59	54
9	42	1045	f	59	55
9	43	1046	f	59	56
10	44	1047	f	59	59
9	45	1048	f	59	60
9	46	1049	f	59	61
9	47	1050	f	59	57
9	48	1051	f	59	58
9	49	1052	f	59	62
9	50	1053	f	59	63
9	51	1054	f	59	64
9	52	1055	f	59	65
9	53	1056	f	59	66
9	54	1057	f	59	67
9	55	1058	f	59	68
9	56	1059	f	59	69
2	0	1060	f	60	1
2	1	1061	f	60	2
1	2	1062	f	60	3
2	3	1063	f	60	5
2	4	1064	f	60	6
5	5	1065	f	60	13
5	6	1066	f	60	14
4	7	1067	f	60	15
7	8	1068	f	60	16
1	9	1069	f	60	27
2	10	1070	f	60	28
2	11	1071	f	60	29
2	12	1072	f	60	30
2	13	1073	f	60	31
2	14	1074	f	60	32
2	15	1075	f	60	33
2	16	1076	f	60	34
2	17	1077	f	60	35
2	18	1078	f	60	36
2	19	1079	f	60	37
2	20	1080	f	60	38
2	21	1081	f	60	39
2	22	1082	f	60	40
2	23	1083	f	60	41
2	24	1084	f	60	42
2	25	1085	f	60	43
2	26	1086	f	60	44
2	27	1087	f	60	45
2	28	1088	f	60	46
2	29	1089	f	60	47
2	30	1090	f	60	48
2	31	1091	f	60	49
2	32	1092	f	60	50
2	33	1093	f	60	53
2	34	1094	f	60	51
1	35	1095	f	60	52
2	0	1096	f	61	1
2	1	1097	f	61	2
1	2	1098	f	61	3
2	3	1099	f	61	5
1	4	1100	f	61	6
5	5	1101	f	61	13
5	6	1102	f	61	14
4	7	1103	f	61	15
7	8	1104	f	61	16
5	9	1105	f	61	22
4	10	1106	f	61	23
5	11	1107	f	61	24
5	12	1108	f	61	25
5	13	1109	f	61	26
1	14	1110	f	61	27
2	15	1111	f	61	28
2	16	1112	f	61	29
2	17	1113	f	61	30
2	18	1114	f	61	31
2	19	1115	f	61	32
2	20	1116	f	61	33
2	21	1117	f	61	34
2	22	1118	f	61	35
2	23	1119	f	61	36
2	24	1120	f	61	37
2	25	1121	f	61	38
2	26	1122	f	61	39
2	27	1123	f	61	40
2	28	1124	f	61	41
2	29	1125	f	61	42
2	30	1126	f	61	43
2	31	1127	f	61	44
2	32	1128	f	61	45
2	33	1129	f	61	46
2	34	1130	f	61	47
2	35	1131	f	61	48
2	36	1132	f	61	49
2	37	1133	f	61	50
2	38	1134	f	61	53
2	39	1135	f	61	51
2	40	1136	f	61	52
2	0	1137	f	62	1
2	1	1138	f	62	2
2	2	1139	f	62	3
1	3	1140	f	62	5
1	4	1141	f	62	6
5	5	1142	f	62	17
4	6	1143	f	62	18
5	7	1144	f	62	19
5	8	1145	f	62	20
5	9	1146	f	62	21
5	10	1147	f	62	22
4	11	1148	f	62	23
5	12	1149	f	62	24
5	13	1150	f	62	25
5	14	1151	f	62	26
1	15	1152	f	62	27
2	16	1153	f	62	28
2	17	1154	f	62	29
2	18	1155	f	62	30
2	19	1156	f	62	31
2	20	1157	f	62	32
2	21	1158	f	62	33
2	22	1159	f	62	34
2	23	1160	f	62	35
2	24	1161	f	62	36
2	25	1162	f	62	37
2	26	1163	f	62	38
2	27	1164	f	62	39
2	28	1165	f	62	40
2	29	1166	f	62	41
2	30	1167	f	62	42
2	31	1168	f	62	43
2	32	1169	f	62	44
2	33	1170	f	62	45
2	34	1171	f	62	46
2	35	1172	f	62	47
2	36	1173	f	62	48
2	37	1174	f	62	49
2	38	1175	f	62	50
2	39	1176	f	62	53
2	40	1177	f	62	51
1	41	1178	f	62	52
1	42	1179	f	62	71
1	43	1180	f	62	54
9	44	1181	f	62	55
9	45	1182	f	62	56
9	46	1183	f	62	59
10	47	1184	f	62	60
9	48	1185	f	62	61
9	49	1186	f	62	57
10	50	1187	f	62	58
9	51	1188	f	62	62
9	52	1189	f	62	63
9	53	1190	f	62	64
9	54	1191	f	62	65
9	55	1192	f	62	66
9	56	1193	f	62	67
9	57	1194	f	62	68
10	58	1195	f	62	69
2	0	1196	f	63	1
2	1	1197	f	63	2
2	2	1198	f	63	3
2	3	1199	f	63	5
2	4	1200	f	63	6
1	0	1201	f	64	1
2	1	1202	f	64	2
2	2	1203	f	64	3
2	3	1204	f	64	5
1	4	1205	f	64	6
2	5	1206	f	64	71
1	6	1207	f	64	7
2	7	1208	f	64	8
2	8	1209	f	64	9
4	9	1210	f	64	22
5	10	1211	f	64	23
5	11	1212	f	64	24
5	12	1213	f	64	25
5	13	1214	f	64	26
1	14	1215	f	64	27
2	15	1216	f	64	28
2	16	1217	f	64	29
2	17	1218	f	64	30
2	18	1219	f	64	31
2	19	1220	f	64	32
2	20	1221	f	64	33
2	21	1222	f	64	34
2	22	1223	f	64	35
2	23	1224	f	64	36
2	24	1225	f	64	37
2	25	1226	f	64	38
2	26	1227	f	64	39
1	27	1228	f	64	40
1	28	1229	f	64	41
1	29	1230	f	64	42
2	30	1231	f	64	43
2	31	1232	f	64	44
1	32	1233	f	64	45
1	33	1234	f	64	46
2	34	1235	f	64	47
2	35	1236	f	64	48
2	36	1237	f	64	49
2	37	1238	f	64	50
2	38	1239	f	64	53
2	39	1240	f	64	51
1	40	1241	f	64	52
2	41	1242	f	64	54
2	0	1243	f	65	1
2	1	1244	f	65	2
1	2	1245	f	65	3
2	3	1246	f	65	5
2	4	1247	f	65	6
2	5	1248	f	65	71
5	6	1249	f	65	13
5	7	1250	f	65	14
5	8	1251	f	65	15
6	9	1252	f	65	16
1	10	1253	f	65	27
2	11	1254	f	65	28
2	12	1255	f	65	29
2	13	1256	f	65	30
2	14	1257	f	65	31
2	15	1258	f	65	32
2	16	1259	f	65	33
2	17	1260	f	65	34
2	18	1261	f	65	35
2	19	1262	f	65	36
2	20	1263	f	65	37
2	21	1264	f	65	38
1	22	1265	f	65	39
2	23	1266	f	65	40
2	24	1267	f	65	41
2	25	1268	f	65	42
1	26	1269	f	65	43
2	27	1270	f	65	44
2	28	1271	f	65	45
2	29	1272	f	65	46
2	30	1273	f	65	47
2	31	1274	f	65	48
2	32	1275	f	65	49
2	33	1276	f	65	50
2	34	1277	f	65	53
2	35	1278	f	65	51
1	36	1279	f	65	52
2	37	1280	f	65	54
1	0	1281	f	66	1
2	1	1282	f	66	2
2	2	1283	f	66	3
2	3	1284	f	66	5
2	4	1285	f	66	6
2	5	1286	f	66	71
2	6	1287	f	66	7
1	7	1288	f	66	8
1	8	1289	f	66	9
1	9	1290	f	66	27
2	10	1291	f	66	28
2	11	1292	f	66	29
2	12	1293	f	66	30
2	13	1294	f	66	31
2	14	1295	f	66	32
2	15	1296	f	66	33
2	16	1297	f	66	34
2	17	1298	f	66	35
2	18	1299	f	66	36
2	19	1300	f	66	37
2	20	1301	f	66	38
2	21	1302	f	66	39
2	22	1303	f	66	40
2	23	1304	f	66	41
2	24	1305	f	66	42
2	25	1306	f	66	43
2	26	1307	f	66	44
2	27	1308	f	66	45
2	28	1309	f	66	46
2	29	1310	f	66	47
2	30	1311	f	66	48
2	31	1312	f	66	49
2	32	1313	f	66	50
2	33	1314	f	66	53
2	34	1315	f	66	51
1	35	1316	f	66	52
1	36	1317	f	66	54
9	37	1318	f	66	55
9	38	1319	f	66	56
9	39	1320	f	66	59
10	40	1321	f	66	60
9	41	1322	f	66	61
9	42	1323	f	66	57
9	43	1324	f	66	58
9	44	1325	f	66	62
9	45	1326	f	66	63
9	46	1327	f	66	64
9	47	1328	f	66	65
9	48	1329	f	66	66
9	49	1330	f	66	67
10	50	1331	f	66	68
9	51	1332	f	66	69
1	0	1333	f	68	1
2	1	1334	f	68	2
2	2	1335	f	68	3
1	3	1336	f	68	5
2	4	1337	f	68	6
2	5	1338	f	68	71
1	6	1339	f	68	7
2	7	1340	f	68	8
1	8	1341	f	68	9
5	9	1342	f	68	17
5	10	1343	f	68	18
5	11	1344	f	68	19
4	12	1345	f	68	20
4	13	1346	f	68	21
1	14	1347	f	68	27
2	15	1348	f	68	28
2	16	1349	f	68	29
2	17	1350	f	68	30
2	18	1351	f	68	31
2	19	1352	f	68	32
2	20	1353	f	68	33
2	21	1354	f	68	34
2	22	1355	f	68	35
2	23	1356	f	68	36
2	24	1357	f	68	37
2	25	1358	f	68	38
2	26	1359	f	68	39
2	27	1360	f	68	40
2	28	1361	f	68	41
2	29	1362	f	68	42
2	30	1363	f	68	43
2	31	1364	f	68	44
2	32	1365	f	68	45
2	33	1366	f	68	46
2	34	1367	f	68	47
2	35	1368	f	68	48
2	36	1369	f	68	49
2	37	1370	f	68	50
2	38	1371	f	68	53
2	39	1372	f	68	51
1	40	1373	f	68	52
2	41	1374	f	68	54
1	0	1375	f	69	1
1	1	1376	f	69	2
2	2	1377	f	69	3
2	3	1378	f	69	5
2	4	1379	f	69	6
2	5	1380	f	69	71
1	6	1381	f	69	7
1	7	1382	f	69	8
1	8	1383	f	69	9
2	9	1384	f	69	10
2	10	1385	f	69	11
1	11	1386	f	69	12
2	12	1387	f	69	27
2	13	1388	f	69	54
2	0	1389	f	70	1
1	1	1390	f	70	2
1	2	1391	f	70	3
2	3	1392	f	70	5
2	4	1393	f	70	6
2	5	1394	f	70	71
1	6	1395	f	70	10
2	7	1396	f	70	11
2	8	1397	f	70	12
4	9	1398	f	70	13
5	10	1399	f	70	14
4	11	1400	f	70	15
8	12	1401	f	70	16
2	13	1402	f	70	27
2	14	1403	f	70	54
2	0	1404	f	71	1
2	1	1405	f	71	2
1	2	1406	f	71	3
2	3	1407	f	71	5
2	4	1408	f	71	6
2	5	1409	f	71	71
5	6	1410	f	71	13
5	7	1411	f	71	14
5	8	1412	f	71	15
8	9	1413	f	71	16
1	10	1414	f	71	27
2	11	1415	f	71	28
2	12	1416	f	71	29
2	13	1417	f	71	30
2	14	1418	f	71	31
2	15	1419	f	71	32
1	16	1420	f	71	33
2	17	1421	f	71	34
2	18	1422	f	71	35
2	19	1423	f	71	36
2	20	1424	f	71	37
2	21	1425	f	71	38
2	22	1426	f	71	39
2	23	1427	f	71	40
2	24	1428	f	71	41
2	25	1429	f	71	42
2	26	1430	f	71	43
2	27	1431	f	71	44
2	28	1432	f	71	45
2	29	1433	f	71	46
2	30	1434	f	71	47
2	31	1435	f	71	48
2	32	1436	f	71	49
2	33	1437	f	71	50
2	34	1438	f	71	53
2	35	1439	f	71	51
2	36	1440	f	71	52
1	37	1441	f	71	54
9	38	1442	f	71	55
9	39	1443	f	71	56
10	40	1444	f	71	59
9	41	1445	f	71	60
9	42	1446	f	71	61
9	43	1447	f	71	57
9	44	1448	f	71	58
9	45	1449	f	71	62
9	46	1450	f	71	63
9	47	1451	f	71	64
9	48	1452	f	71	65
9	49	1453	f	71	66
9	50	1454	f	71	67
9	51	1455	f	71	68
9	52	1456	f	71	69
1	0	1457	f	72	1
2	1	1458	f	72	2
2	2	1459	f	72	3
2	3	1460	f	72	5
2	4	1461	f	72	6
2	5	1462	f	72	71
2	6	1463	f	72	7
2	7	1464	f	72	8
1	8	1465	f	72	9
2	9	1466	f	72	27
2	10	1467	f	72	54
2	0	1468	f	73	1
1	1	1469	f	73	2
2	2	1470	f	73	3
1	3	1471	f	73	5
2	4	1472	f	73	6
2	5	1473	f	73	71
2	6	1474	f	73	10
1	7	1475	f	73	11
2	8	1476	f	73	12
5	9	1477	f	73	17
5	10	1478	f	73	18
5	11	1479	f	73	19
5	12	1480	f	73	20
4	13	1481	f	73	21
2	14	1482	f	73	27
1	15	1483	f	73	54
9	16	1484	f	73	55
9	17	1485	f	73	56
9	18	1486	f	73	59
9	19	1487	f	73	60
9	20	1488	f	73	61
9	21	1489	f	73	57
9	22	1490	f	73	58
10	23	1491	f	73	62
9	24	1492	f	73	63
9	25	1493	f	73	64
9	26	1494	f	73	65
9	27	1495	f	73	66
9	28	1496	f	73	67
9	29	1497	f	73	68
9	30	1498	f	73	69
2	0	1499	f	74	1
2	1	1500	f	74	2
2	2	1501	f	74	3
1	3	1502	f	74	5
1	4	1503	f	74	6
2	5	1504	f	74	71
5	6	1505	f	74	17
5	7	1506	f	74	18
5	8	1507	f	74	19
4	9	1508	f	74	20
5	10	1509	f	74	21
5	11	1510	f	74	22
5	12	1511	f	74	23
5	13	1512	f	74	24
4	14	1513	f	74	25
5	15	1514	f	74	26
2	16	1515	f	74	27
2	17	1516	f	74	54
1	0	1517	f	77	1
2	1	1518	f	77	2
2	2	1519	f	77	3
2	3	1520	f	77	5
2	4	1521	f	77	6
2	5	1522	f	77	71
2	6	1523	f	77	7
1	7	1524	f	77	8
2	8	1525	f	77	9
2	9	1526	f	77	27
2	10	1527	f	77	54
2	0	1528	f	79	1
1	1	1529	f	79	2
2	2	1530	f	79	3
2	3	1531	f	79	5
2	4	1532	f	79	6
2	5	1533	f	79	71
2	6	1534	f	79	10
1	7	1535	f	79	11
2	8	1536	f	79	12
2	9	1537	f	79	27
2	10	1538	f	79	54
2	0	1539	f	80	1
2	1	1540	f	80	2
2	2	1541	f	80	3
2	3	1542	f	80	5
2	4	1543	f	80	6
2	5	1544	f	80	71
2	6	1545	f	80	27
1	7	1546	f	80	54
9	8	1547	f	80	55
9	9	1548	f	80	56
9	10	1549	f	80	59
10	11	1550	f	80	60
9	12	1551	f	80	61
9	13	1552	f	80	57
9	14	1553	f	80	58
9	15	1554	f	80	62
9	16	1555	f	80	63
9	17	1556	f	80	64
9	18	1557	f	80	65
9	19	1558	f	80	66
9	20	1559	f	80	67
9	21	1560	f	80	68
9	22	1561	f	80	69
1	0	1562	f	81	1
1	1	1563	f	81	2
2	2	1564	f	81	3
2	3	1565	f	81	5
2	4	1566	f	81	6
2	5	1567	f	81	71
2	6	1568	f	81	7
2	7	1569	f	81	8
1	8	1570	f	81	9
2	9	1571	f	81	10
1	10	1572	f	81	11
2	11	1573	f	81	12
2	12	1574	f	81	27
2	13	1575	f	81	54
2	0	1576	f	82	1
2	1	1577	f	82	2
1	2	1578	f	82	3
2	3	1579	f	82	5
1	4	1580	f	82	6
2	5	1581	f	82	71
5	6	1582	f	82	13
5	7	1583	f	82	14
5	8	1584	f	82	15
8	9	1585	f	82	16
5	10	1586	f	82	22
5	11	1587	f	82	23
4	12	1588	f	82	24
5	13	1589	f	82	25
5	14	1590	f	82	26
2	15	1591	f	82	27
2	16	1592	f	82	54
2	0	1593	f	83	1
2	1	1594	f	83	2
1	2	1595	f	83	3
2	3	1596	f	83	5
2	4	1597	f	83	6
2	5	1598	f	83	71
4	6	1599	f	83	13
5	7	1600	f	83	14
5	8	1601	f	83	15
7	9	1602	f	83	16
2	10	1603	f	83	27
2	11	1604	f	83	54
1	0	1605	f	84	1
2	1	1606	f	84	2
2	2	1607	f	84	3
2	3	1608	f	84	5
2	4	1609	f	84	6
2	5	1610	f	84	71
2	6	1611	f	84	7
2	7	1612	f	84	8
1	8	1613	f	84	9
2	9	1614	f	84	27
2	10	1615	f	84	54
2	0	1616	f	85	1
1	1	1617	f	85	2
1	2	1618	f	85	3
2	3	1619	f	85	5
2	4	1620	f	85	6
2	5	1621	f	85	71
2	6	1622	f	85	10
1	7	1623	f	85	11
2	8	1624	f	85	12
5	9	1625	f	85	13
5	10	1626	f	85	14
4	11	1627	f	85	15
6	12	1628	f	85	16
2	13	1629	f	85	27
2	14	1630	f	85	54
1	0	1631	f	86	1
2	1	1632	f	86	2
2	2	1633	f	86	3
2	3	1634	f	86	5
1	4	1635	f	86	6
2	5	1636	f	86	71
1	6	1637	f	86	7
2	7	1638	f	86	8
2	8	1639	f	86	9
5	9	1640	f	86	22
5	10	1641	f	86	23
5	11	1642	f	86	24
5	12	1643	f	86	25
4	13	1644	f	86	26
1	14	1645	f	86	27
2	15	1646	f	86	28
2	16	1647	f	86	29
2	17	1648	f	86	30
2	18	1649	f	86	31
2	19	1650	f	86	32
2	20	1651	f	86	33
2	21	1652	f	86	34
2	22	1653	f	86	35
2	23	1654	f	86	36
2	24	1655	f	86	37
2	25	1656	f	86	38
1	26	1657	f	86	39
1	27	1658	f	86	40
1	28	1659	f	86	41
1	29	1660	f	86	42
1	30	1661	f	86	43
1	31	1662	f	86	44
1	32	1663	f	86	45
1	33	1664	f	86	46
2	34	1665	f	86	47
2	35	1666	f	86	48
2	36	1667	f	86	49
2	37	1668	f	86	50
2	38	1669	f	86	53
2	39	1670	f	86	51
2	40	1671	f	86	52
2	41	1672	f	86	54
2	0	1673	f	87	1
2	1	1674	f	87	2
2	2	1675	f	87	3
1	3	1676	f	87	5
2	4	1677	f	87	6
2	5	1678	f	87	71
4	6	1679	f	87	17
5	7	1680	f	87	18
4	8	1681	f	87	19
5	9	1682	f	87	20
5	10	1683	f	87	21
1	11	1684	f	87	27
2	12	1685	f	87	28
2	13	1686	f	87	29
2	14	1687	f	87	30
2	15	1688	f	87	31
2	16	1689	f	87	32
2	17	1690	f	87	33
2	18	1691	f	87	34
2	19	1692	f	87	35
2	20	1693	f	87	36
2	21	1694	f	87	37
2	22	1695	f	87	38
1	23	1696	f	87	39
1	24	1697	f	87	40
1	25	1698	f	87	41
1	26	1699	f	87	42
2	27	1700	f	87	43
2	28	1701	f	87	44
2	29	1702	f	87	45
2	30	1703	f	87	46
1	31	1704	f	87	47
1	32	1705	f	87	48
1	33	1706	f	87	49
1	34	1707	f	87	50
2	35	1708	f	87	53
2	36	1709	f	87	51
2	37	1710	f	87	52
2	38	1711	f	87	54
1	0	1712	f	88	1
2	1	1713	f	88	2
2	2	1714	f	88	3
1	3	1715	f	88	5
2	4	1716	f	88	6
2	5	1717	f	88	71
1	6	1718	f	88	7
1	7	1719	f	88	8
2	8	1720	f	88	9
4	9	1721	f	88	17
5	10	1722	f	88	18
5	11	1723	f	88	19
5	12	1724	f	88	20
5	13	1725	f	88	21
1	14	1726	f	88	27
2	15	1727	f	88	28
2	16	1728	f	88	29
2	17	1729	f	88	30
2	18	1730	f	88	31
2	19	1731	f	88	32
2	20	1732	f	88	33
2	21	1733	f	88	34
2	22	1734	f	88	35
2	23	1735	f	88	36
2	24	1736	f	88	37
2	25	1737	f	88	38
2	26	1738	f	88	39
2	27	1739	f	88	40
1	28	1740	f	88	41
1	29	1741	f	88	42
2	30	1742	f	88	43
2	31	1743	f	88	44
2	32	1744	f	88	45
1	33	1745	f	88	46
2	34	1746	f	88	47
2	35	1747	f	88	48
2	36	1748	f	88	49
1	37	1749	f	88	50
2	38	1750	f	88	53
2	39	1751	f	88	51
2	40	1752	f	88	52
2	41	1753	f	88	54
2	0	1754	f	89	1
2	1	1755	f	89	2
2	2	1756	f	89	3
2	3	1757	f	89	5
2	4	1758	f	89	6
2	5	1759	f	89	71
1	6	1760	f	89	27
2	7	1761	f	89	28
2	8	1762	f	89	29
2	9	1763	f	89	30
2	10	1764	f	89	31
2	11	1765	f	89	32
2	12	1766	f	89	33
2	13	1767	f	89	34
2	14	1768	f	89	35
2	15	1769	f	89	36
2	16	1770	f	89	37
2	17	1771	f	89	38
2	18	1772	f	89	39
2	19	1773	f	89	40
2	20	1774	f	89	41
1	21	1775	f	89	42
2	22	1776	f	89	43
2	23	1777	f	89	44
2	24	1778	f	89	45
1	25	1779	f	89	46
2	26	1780	f	89	47
2	27	1781	f	89	48
2	28	1782	f	89	49
1	29	1783	f	89	50
2	30	1784	f	89	53
2	31	1785	f	89	51
2	32	1786	f	89	52
1	33	1787	f	89	54
9	34	1788	f	89	55
9	35	1789	f	89	56
9	36	1790	f	89	59
10	37	1791	f	89	60
9	38	1792	f	89	61
9	39	1793	f	89	57
9	40	1794	f	89	58
9	41	1795	f	89	62
9	42	1796	f	89	63
9	43	1797	f	89	64
9	44	1798	f	89	65
9	45	1799	f	89	66
9	46	1800	f	89	67
9	47	1801	f	89	68
9	48	1802	f	89	69
2	0	1803	f	90	1
2	1	1804	f	90	2
2	2	1805	f	90	3
1	3	1806	f	90	5
2	4	1807	f	90	6
2	5	1808	f	90	71
5	6	1809	f	90	17
5	7	1810	f	90	18
5	8	1811	f	90	19
4	9	1812	f	90	20
5	10	1813	f	90	21
1	11	1814	f	90	27
2	12	1815	f	90	28
2	13	1816	f	90	29
2	14	1817	f	90	30
2	15	1818	f	90	31
2	16	1819	f	90	32
2	17	1820	f	90	33
2	18	1821	f	90	34
2	19	1822	f	90	35
2	20	1823	f	90	36
2	21	1824	f	90	37
2	22	1825	f	90	38
2	23	1826	f	90	39
1	24	1827	f	90	40
2	25	1828	f	90	41
1	26	1829	f	90	42
1	27	1830	f	90	43
1	28	1831	f	90	44
1	29	1832	f	90	45
1	30	1833	f	90	46
2	31	1834	f	90	47
2	32	1835	f	90	48
2	33	1836	f	90	49
2	34	1837	f	90	50
2	35	1838	f	90	53
2	36	1839	f	90	51
2	37	1840	f	90	52
2	38	1841	f	90	54
1	0	1842	f	91	1
2	1	1843	f	91	2
2	2	1844	f	91	3
1	3	1845	f	91	5
1	4	1846	f	91	6
2	5	1847	f	91	71
2	6	1848	f	91	7
2	7	1849	f	91	8
1	8	1850	f	91	9
5	9	1851	f	91	17
5	10	1852	f	91	18
5	11	1853	f	91	19
5	12	1854	f	91	20
4	13	1855	f	91	21
5	14	1856	f	91	22
5	15	1857	f	91	23
4	16	1858	f	91	24
5	17	1859	f	91	25
5	18	1860	f	91	26
1	19	1861	f	91	27
2	20	1862	f	91	28
2	21	1863	f	91	29
2	22	1864	f	91	30
2	23	1865	f	91	31
2	24	1866	f	91	32
2	25	1867	f	91	33
2	26	1868	f	91	34
2	27	1869	f	91	35
2	28	1870	f	91	36
2	29	1871	f	91	37
2	30	1872	f	91	38
2	31	1873	f	91	39
2	32	1874	f	91	40
1	33	1875	f	91	41
1	34	1876	f	91	42
2	35	1877	f	91	43
2	36	1878	f	91	44
1	37	1879	f	91	45
1	38	1880	f	91	46
2	39	1881	f	91	47
2	40	1882	f	91	48
1	41	1883	f	91	49
1	42	1884	f	91	50
2	43	1885	f	91	53
2	44	1886	f	91	51
2	45	1887	f	91	52
2	46	1888	f	91	54
2	0	1889	f	92	1
2	1	1890	f	92	2
2	2	1891	f	92	3
2	3	1892	f	92	5
1	4	1893	f	92	6
2	5	1894	f	92	71
5	6	1895	f	92	22
5	7	1896	f	92	23
4	8	1897	f	92	24
5	9	1898	f	92	25
5	10	1899	f	92	26
1	11	1900	f	92	27
2	12	1901	f	92	28
2	13	1902	f	92	29
2	14	1903	f	92	30
2	15	1904	f	92	31
2	16	1905	f	92	32
2	17	1906	f	92	33
2	18	1907	f	92	34
2	19	1908	f	92	35
2	20	1909	f	92	36
2	21	1910	f	92	37
2	22	1911	f	92	38
1	23	1912	f	92	39
1	24	1913	f	92	40
2	25	1914	f	92	41
2	26	1915	f	92	42
1	27	1916	f	92	43
1	28	1917	f	92	44
2	29	1918	f	92	45
2	30	1919	f	92	46
1	31	1920	f	92	47
1	32	1921	f	92	48
2	33	1922	f	92	49
2	34	1923	f	92	50
2	35	1924	f	92	53
2	36	1925	f	92	51
2	37	1926	f	92	52
2	38	1927	f	92	54
2	0	1928	f	93	1
2	1	1929	f	93	2
2	2	1930	f	93	3
1	3	1931	f	93	5
1	4	1932	f	93	6
2	5	1933	f	93	71
5	6	1934	f	93	17
4	7	1935	f	93	18
5	8	1936	f	93	19
5	9	1937	f	93	20
5	10	1938	f	93	21
4	11	1939	f	93	22
5	12	1940	f	93	23
5	13	1941	f	93	24
5	14	1942	f	93	25
5	15	1943	f	93	26
1	16	1944	f	93	27
2	17	1945	f	93	28
2	18	1946	f	93	29
2	19	1947	f	93	30
2	20	1948	f	93	31
2	21	1949	f	93	32
2	22	1950	f	93	33
2	23	1951	f	93	34
2	24	1952	f	93	35
2	25	1953	f	93	36
2	26	1954	f	93	37
2	27	1955	f	93	38
2	28	1956	f	93	39
2	29	1957	f	93	40
2	30	1958	f	93	41
1	31	1959	f	93	42
2	32	1960	f	93	43
2	33	1961	f	93	44
2	34	1962	f	93	45
1	35	1963	f	93	46
2	36	1964	f	93	47
2	37	1965	f	93	48
1	38	1966	f	93	49
1	39	1967	f	93	50
2	40	1968	f	93	53
2	41	1969	f	93	51
2	42	1970	f	93	52
2	0	1971	f	94	1
2	1	1972	f	94	2
2	2	1973	f	94	3
2	3	1974	f	94	5
2	4	1975	f	94	6
2	5	1976	f	94	71
1	6	1977	f	94	27
2	7	1978	f	94	28
2	8	1979	f	94	29
2	9	1980	f	94	30
2	10	1981	f	94	31
2	11	1982	f	94	32
2	12	1983	f	94	33
2	13	1984	f	94	34
2	14	1985	f	94	35
2	15	1986	f	94	36
2	16	1987	f	94	37
2	17	1988	f	94	38
2	18	1989	f	94	39
1	19	1990	f	94	40
1	20	1991	f	94	41
1	21	1992	f	94	42
2	22	1993	f	94	43
1	23	1994	f	94	44
1	24	1995	f	94	45
1	25	1996	f	94	46
2	26	1997	f	94	47
1	27	1998	f	94	48
1	28	1999	f	94	49
1	29	2000	f	94	50
2	30	2001	f	94	53
2	31	2002	f	94	51
1	32	2003	f	94	52
1	33	2004	f	94	54
9	34	2005	f	94	55
9	35	2006	f	94	56
9	36	2007	f	94	59
10	37	2008	f	94	60
9	38	2009	f	94	61
9	39	2010	f	94	57
10	40	2011	f	94	58
9	41	2012	f	94	62
9	42	2013	f	94	63
9	43	2014	f	94	64
9	44	2015	f	94	65
9	45	2016	f	94	66
9	46	2017	f	94	67
9	47	2018	f	94	68
9	48	2019	f	94	69
1	0	2020	f	95	1
1	1	2021	f	95	2
2	2	2022	f	95	3
2	3	2023	f	95	5
2	4	2024	f	95	6
2	5	2025	f	95	71
1	6	2026	f	95	7
2	7	2027	f	95	8
1	8	2028	f	95	9
2	9	2029	f	95	10
1	10	2030	f	95	11
1	11	2031	f	95	12
1	12	2032	f	95	27
2	13	2033	f	95	28
2	14	2034	f	95	29
2	15	2035	f	95	30
2	16	2036	f	95	31
2	17	2037	f	95	32
2	18	2038	f	95	33
2	19	2039	f	95	34
2	20	2040	f	95	35
2	21	2041	f	95	36
2	22	2042	f	95	37
2	23	2043	f	95	38
2	24	2044	f	95	39
2	25	2045	f	95	40
2	26	2046	f	95	41
2	27	2047	f	95	42
1	28	2048	f	95	43
1	29	2049	f	95	44
1	30	2050	f	95	45
1	31	2051	f	95	46
2	32	2052	f	95	47
2	33	2053	f	95	48
2	34	2054	f	95	49
2	35	2055	f	95	50
2	36	2056	f	95	53
2	37	2057	f	95	51
2	38	2058	f	95	52
2	0	2059	f	98	1
2	1	2060	f	98	2
1	2	2061	f	98	3
2	3	2062	f	98	5
2	4	2063	f	98	6
2	5	2064	f	98	71
4	6	2065	f	98	13
5	7	2066	f	98	14
4	8	2067	f	98	15
7	9	2068	f	98	16
2	10	2069	f	98	27
2	11	2070	f	98	54
2	0	2071	f	99	1
2	1	2072	f	99	2
2	2	2073	f	99	3
1	3	2074	f	99	5
2	4	2075	f	99	6
1	5	2076	f	99	71
5	6	2077	f	99	17
4	7	2078	f	99	18
5	8	2079	f	99	19
4	9	2080	f	99	20
5	10	2081	f	99	21
1	11	2082	f	99	27
2	12	2083	f	99	28
2	13	2084	f	99	29
2	14	2085	f	99	30
2	15	2086	f	99	31
2	16	2087	f	99	32
2	17	2088	f	99	33
2	18	2089	f	99	34
2	19	2090	f	99	35
2	20	2091	f	99	36
2	21	2092	f	99	37
2	22	2093	f	99	38
2	23	2094	f	99	39
2	24	2095	f	99	40
2	25	2096	f	99	41
2	26	2097	f	99	42
2	27	2098	f	99	43
2	28	2099	f	99	44
2	29	2100	f	99	45
2	30	2101	f	99	46
2	31	2102	f	99	47
2	32	2103	f	99	48
2	33	2104	f	99	49
2	34	2105	f	99	50
2	35	2106	f	99	53
2	36	2107	f	99	51
2	37	2108	f	99	52
1	38	2109	f	99	54
10	39	2110	f	99	55
9	40	2111	f	99	56
9	41	2112	f	99	59
10	42	2113	f	99	60
9	43	2114	f	99	61
9	44	2115	f	99	57
10	45	2116	f	99	58
9	46	2117	f	99	62
10	47	2118	f	99	63
10	48	2119	f	99	64
9	49	2120	f	99	65
10	50	2121	f	99	66
10	51	2122	f	99	67
9	52	2123	f	99	68
9	53	2124	f	99	69
2	0	2125	f	100	1
2	1	2126	f	100	2
1	2	2127	f	100	3
2	3	2128	f	100	5
2	4	2129	f	100	6
1	5	2130	f	100	71
4	6	2131	f	100	13
5	7	2132	f	100	14
5	8	2133	f	100	15
8	9	2134	f	100	16
1	10	2135	f	100	27
2	11	2136	f	100	28
2	12	2137	f	100	29
2	13	2138	f	100	30
2	14	2139	f	100	31
2	15	2140	f	100	32
2	16	2141	f	100	33
2	17	2142	f	100	34
2	18	2143	f	100	35
2	19	2144	f	100	36
2	20	2145	f	100	37
2	21	2146	f	100	38
2	22	2147	f	100	39
2	23	2148	f	100	40
2	24	2149	f	100	41
2	25	2150	f	100	42
2	26	2151	f	100	43
2	27	2152	f	100	44
2	28	2153	f	100	45
2	29	2154	f	100	46
2	30	2155	f	100	47
2	31	2156	f	100	48
2	32	2157	f	100	49
2	33	2158	f	100	50
2	34	2159	f	100	53
2	35	2160	f	100	51
2	36	2161	f	100	52
1	37	2162	f	100	54
10	38	2163	f	100	55
9	39	2164	f	100	56
9	40	2165	f	100	59
10	41	2166	f	100	60
9	42	2167	f	100	61
10	43	2168	f	100	57
10	44	2169	f	100	58
9	45	2170	f	100	62
9	46	2171	f	100	63
9	47	2172	f	100	64
9	48	2173	f	100	65
9	49	2174	f	100	66
10	50	2175	f	100	67
9	51	2176	f	100	68
10	52	2177	f	100	69
2	0	2178	f	101	1
2	1	2179	f	101	2
1	2	2180	f	101	3
2	3	2181	f	101	5
1	4	2182	f	101	6
1	5	2183	f	101	71
5	6	2184	f	101	13
4	7	2185	f	101	14
5	8	2186	f	101	15
6	9	2187	f	101	16
5	10	2188	f	101	22
5	11	2189	f	101	23
4	12	2190	f	101	24
5	13	2191	f	101	25
5	14	2192	f	101	26
1	15	2193	f	101	27
2	16	2194	f	101	28
2	17	2195	f	101	29
2	18	2196	f	101	30
2	19	2197	f	101	31
2	20	2198	f	101	32
2	21	2199	f	101	33
2	22	2200	f	101	34
2	23	2201	f	101	35
2	24	2202	f	101	36
2	25	2203	f	101	37
2	26	2204	f	101	38
2	27	2205	f	101	39
2	28	2206	f	101	40
2	29	2207	f	101	41
2	30	2208	f	101	42
2	31	2209	f	101	43
2	32	2210	f	101	44
2	33	2211	f	101	45
2	34	2212	f	101	46
2	35	2213	f	101	47
2	36	2214	f	101	48
2	37	2215	f	101	49
2	38	2216	f	101	50
2	39	2217	f	101	53
2	40	2218	f	101	51
2	41	2219	f	101	52
1	42	2220	f	101	54
10	43	2221	f	101	55
10	44	2222	f	101	56
10	45	2223	f	101	59
9	46	2224	f	101	60
9	47	2225	f	101	61
10	48	2226	f	101	57
10	49	2227	f	101	58
9	50	2228	f	101	62
10	51	2229	f	101	63
9	52	2230	f	101	64
9	53	2231	f	101	65
9	54	2232	f	101	66
10	55	2233	f	101	67
10	56	2234	f	101	68
10	57	2235	f	101	69
4	58	2236	f	101	21
2	0	2237	f	102	1
2	1	2238	f	102	2
2	2	2239	f	102	3
2	3	2240	f	102	5
2	4	2241	f	102	6
1	5	2242	f	102	71
1	6	2243	f	102	27
2	7	2244	f	102	28
2	8	2245	f	102	29
2	9	2246	f	102	30
2	10	2247	f	102	31
2	11	2248	f	102	32
2	12	2249	f	102	33
2	13	2250	f	102	34
2	14	2251	f	102	35
2	15	2252	f	102	36
2	16	2253	f	102	37
2	17	2254	f	102	38
2	18	2255	f	102	39
2	19	2256	f	102	40
2	20	2257	f	102	41
2	21	2258	f	102	42
2	22	2259	f	102	43
2	23	2260	f	102	44
2	24	2261	f	102	45
2	25	2262	f	102	46
2	26	2263	f	102	47
2	27	2264	f	102	48
2	28	2265	f	102	49
2	29	2266	f	102	50
2	30	2267	f	102	53
2	31	2268	f	102	51
2	32	2269	f	102	52
1	33	2270	f	102	54
10	34	2271	f	102	55
10	35	2272	f	102	56
10	36	2273	f	102	59
9	37	2274	f	102	60
10	38	2275	f	102	61
10	39	2276	f	102	57
9	40	2277	f	102	58
10	41	2278	f	102	62
10	42	2279	f	102	63
9	43	2280	f	102	64
9	44	2281	f	102	65
9	45	2282	f	102	66
10	46	2283	f	102	67
9	47	2284	f	102	68
10	48	2285	f	102	69
1	0	2286	f	103	1
2	1	2287	f	103	2
1	2	2288	f	103	3
2	3	2289	f	103	5
1	4	2290	f	103	6
1	5	2291	f	103	71
1	6	2292	f	103	7
2	7	2293	f	103	8
1	8	2294	f	103	9
5	9	2295	f	103	13
4	10	2296	f	103	14
5	11	2297	f	103	15
8	12	2298	f	103	16
5	13	2299	f	103	22
4	14	2300	f	103	23
5	15	2301	f	103	24
5	16	2302	f	103	25
4	17	2303	f	103	26
1	18	2304	f	103	27
2	19	2305	f	103	28
2	20	2306	f	103	29
2	21	2307	f	103	30
2	22	2308	f	103	31
2	23	2309	f	103	32
2	24	2310	f	103	33
2	25	2311	f	103	34
2	26	2312	f	103	35
2	27	2313	f	103	36
2	28	2314	f	103	37
2	29	2315	f	103	38
2	30	2316	f	103	39
2	31	2317	f	103	40
2	32	2318	f	103	41
2	33	2319	f	103	42
2	34	2320	f	103	43
2	35	2321	f	103	44
2	36	2322	f	103	45
2	37	2323	f	103	46
2	38	2324	f	103	47
2	39	2325	f	103	48
2	40	2326	f	103	49
2	41	2327	f	103	50
2	42	2328	f	103	53
2	43	2329	f	103	51
2	44	2330	f	103	52
1	45	2331	f	103	54
10	46	2332	f	103	55
10	47	2333	f	103	56
9	48	2334	f	103	59
9	49	2335	f	103	60
10	50	2336	f	103	61
9	51	2337	f	103	57
10	52	2338	f	103	58
10	53	2339	f	103	62
10	54	2340	f	103	63
10	55	2341	f	103	64
9	56	2342	f	103	65
10	57	2343	f	103	66
9	58	2344	f	103	67
10	59	2345	f	103	68
10	60	2346	f	103	69
2	61	2347	f	103	21
1	0	2348	f	104	1
2	1	2349	f	104	2
1	2	2350	f	104	3
2	3	2351	f	104	5
2	4	2352	f	104	6
2	5	2353	f	104	71
2	6	2354	f	104	7
1	7	2355	f	104	8
2	8	2356	f	104	9
4	9	2357	f	104	13
5	10	2358	f	104	14
5	11	2359	f	104	15
7	12	2360	f	104	16
2	13	2361	f	104	27
2	14	2362	f	104	54
2	0	2363	f	105	1
1	1	2364	f	105	2
2	2	2365	f	105	3
2	3	2366	f	105	5
2	4	2367	f	105	6
1	5	2368	f	105	71
2	6	2369	f	105	10
1	7	2370	f	105	11
2	8	2371	f	105	12
1	9	2372	f	105	27
2	10	2373	f	105	28
2	11	2374	f	105	29
2	12	2375	f	105	30
2	13	2376	f	105	31
2	14	2377	f	105	32
2	15	2378	f	105	33
2	16	2379	f	105	34
2	17	2380	f	105	35
2	18	2381	f	105	36
2	19	2382	f	105	37
2	20	2383	f	105	38
2	21	2384	f	105	39
2	22	2385	f	105	40
2	23	2386	f	105	41
2	24	2387	f	105	42
1	25	2388	f	105	43
1	26	2389	f	105	44
1	27	2390	f	105	45
1	28	2391	f	105	46
2	29	2392	f	105	47
2	30	2393	f	105	48
2	31	2394	f	105	49
2	32	2395	f	105	50
2	33	2396	f	105	53
2	34	2397	f	105	51
2	35	2398	f	105	52
1	36	2399	f	105	54
10	37	2400	f	105	55
10	38	2401	f	105	56
10	39	2402	f	105	59
9	40	2403	f	105	60
10	41	2404	f	105	61
10	42	2405	f	105	57
10	43	2406	f	105	58
9	44	2407	f	105	62
9	45	2408	f	105	63
10	46	2409	f	105	64
10	47	2410	f	105	65
9	48	2411	f	105	66
10	49	2412	f	105	67
10	50	2413	f	105	68
9	51	2414	f	105	69
2	0	2415	f	106	1
1	1	2416	f	106	2
1	2	2417	f	106	3
2	3	2418	f	106	5
2	4	2419	f	106	6
2	5	2420	f	106	71
1	6	2421	f	106	10
2	7	2422	f	106	11
1	8	2423	f	106	12
4	9	2424	f	106	13
5	10	2425	f	106	14
4	11	2426	f	106	15
7	12	2427	f	106	16
1	13	2428	f	106	27
2	14	2429	f	106	28
2	15	2430	f	106	29
2	16	2431	f	106	30
2	17	2432	f	106	31
2	18	2433	f	106	32
2	19	2434	f	106	33
2	20	2435	f	106	34
2	21	2436	f	106	35
2	22	2437	f	106	36
2	23	2438	f	106	37
2	24	2439	f	106	38
2	25	2440	f	106	39
2	26	2441	f	106	40
2	27	2442	f	106	41
2	28	2443	f	106	42
1	29	2444	f	106	43
1	30	2445	f	106	44
1	31	2446	f	106	45
1	32	2447	f	106	46
2	33	2448	f	106	47
2	34	2449	f	106	48
2	35	2450	f	106	49
2	36	2451	f	106	50
2	37	2452	f	106	53
2	38	2453	f	106	51
2	39	2454	f	106	52
1	40	2455	f	106	54
10	41	2456	f	106	55
10	42	2457	f	106	56
9	43	2458	f	106	59
10	44	2459	f	106	60
10	45	2460	f	106	61
10	46	2461	f	106	57
9	47	2462	f	106	58
9	48	2463	f	106	62
9	49	2464	f	106	63
9	50	2465	f	106	64
10	51	2466	f	106	65
9	52	2467	f	106	66
9	53	2468	f	106	67
10	54	2469	f	106	68
9	55	2470	f	106	69
2	0	2471	f	107	1
2	1	2472	f	107	2
1	2	2473	f	107	3
2	3	2474	f	107	5
2	4	2475	f	107	6
1	5	2476	f	107	71
5	6	2477	f	107	13
4	7	2478	f	107	14
5	8	2479	f	107	15
7	9	2480	f	107	16
2	10	2481	f	107	27
1	11	2482	f	107	54
10	12	2483	f	107	55
10	13	2484	f	107	56
9	14	2485	f	107	59
10	15	2486	f	107	60
10	16	2487	f	107	61
10	17	2488	f	107	57
10	18	2489	f	107	58
9	19	2490	f	107	62
9	20	2491	f	107	63
10	21	2492	f	107	64
10	22	2493	f	107	65
9	23	2494	f	107	66
10	24	2495	f	107	67
9	25	2496	f	107	68
10	26	2497	f	107	69
4	27	2498	f	107	18
2	0	2499	f	108	1
2	1	2500	f	108	2
2	2	2501	f	108	3
1	3	2502	f	108	5
2	4	2503	f	108	6
1	5	2504	f	108	71
4	6	2505	f	108	17
5	7	2506	f	108	18
5	8	2507	f	108	19
4	9	2508	f	108	20
4	10	2509	f	108	21
1	11	2510	f	108	27
2	12	2511	f	108	28
2	13	2512	f	108	29
2	14	2513	f	108	30
2	15	2514	f	108	31
2	16	2515	f	108	32
2	17	2516	f	108	33
2	18	2517	f	108	34
2	19	2518	f	108	35
2	20	2519	f	108	36
2	21	2520	f	108	37
2	22	2521	f	108	38
2	23	2522	f	108	39
2	24	2523	f	108	40
2	25	2524	f	108	41
2	26	2525	f	108	42
1	27	2526	f	108	43
1	28	2527	f	108	44
1	29	2528	f	108	45
1	30	2529	f	108	46
2	31	2530	f	108	47
2	32	2531	f	108	48
2	33	2532	f	108	49
2	34	2533	f	108	50
2	35	2534	f	108	53
2	36	2535	f	108	51
2	37	2536	f	108	52
1	38	2537	f	108	54
10	39	2538	f	108	55
10	40	2539	f	108	56
10	41	2540	f	108	59
9	42	2541	f	108	60
10	43	2542	f	108	61
9	44	2543	f	108	57
10	45	2544	f	108	58
10	46	2545	f	108	62
10	47	2546	f	108	63
10	48	2547	f	108	64
9	49	2548	f	108	65
9	50	2549	f	108	66
10	51	2550	f	108	67
10	52	2551	f	108	68
10	53	2552	f	108	69
1	0	2553	f	109	1
2	1	2554	f	109	2
2	2	2555	f	109	3
1	3	2556	f	109	5
2	4	2557	f	109	6
2	5	2558	f	109	71
1	6	2559	f	109	7
2	7	2560	f	109	8
1	8	2561	f	109	9
5	9	2562	f	109	17
4	10	2563	f	109	18
5	11	2564	f	109	19
5	12	2565	f	109	20
4	13	2566	f	109	21
1	14	2567	f	109	27
2	15	2568	f	109	28
2	16	2569	f	109	29
2	17	2570	f	109	30
2	18	2571	f	109	31
2	19	2572	f	109	32
2	20	2573	f	109	33
2	21	2574	f	109	34
2	22	2575	f	109	35
2	23	2576	f	109	36
2	24	2577	f	109	37
2	25	2578	f	109	38
2	26	2579	f	109	39
2	27	2580	f	109	40
2	28	2581	f	109	41
2	29	2582	f	109	42
1	30	2583	f	109	43
1	31	2584	f	109	44
1	32	2585	f	109	45
1	33	2586	f	109	46
2	34	2587	f	109	47
2	35	2588	f	109	48
2	36	2589	f	109	49
2	37	2590	f	109	50
2	38	2591	f	109	53
1	39	2592	f	109	51
2	40	2593	f	109	52
1	41	2594	f	109	54
10	42	2595	f	109	55
9	43	2596	f	109	56
10	44	2597	f	109	59
9	45	2598	f	109	60
10	46	2599	f	109	61
9	47	2600	f	109	57
9	48	2601	f	109	58
10	49	2602	f	109	62
9	50	2603	f	109	63
9	51	2604	f	109	64
9	52	2605	f	109	65
10	53	2606	f	109	66
9	54	2607	f	109	67
10	55	2608	f	109	68
9	56	2609	f	109	69
1	0	2610	f	110	1
2	1	2611	f	110	2
2	2	2612	f	110	3
2	3	2613	f	110	5
1	4	2614	f	110	6
2	5	2615	f	110	71
2	6	2616	f	110	7
2	7	2617	f	110	8
1	8	2618	f	110	9
5	9	2619	f	110	22
5	10	2620	f	110	23
5	11	2621	f	110	24
4	12	2622	f	110	25
5	13	2623	f	110	26
2	14	2624	f	110	27
2	15	2625	f	110	54
2	0	2626	f	111	1
2	1	2627	f	111	2
2	2	2628	f	111	3
1	3	2629	f	111	5
2	4	2630	f	111	6
1	5	2631	f	111	71
4	6	2632	f	111	17
5	7	2633	f	111	18
4	8	2634	f	111	19
5	9	2635	f	111	20
4	10	2636	f	111	21
2	11	2637	f	111	27
1	12	2638	f	111	54
10	13	2639	f	111	55
10	14	2640	f	111	56
9	15	2641	f	111	59
9	16	2642	f	111	60
10	17	2643	f	111	61
9	18	2644	f	111	57
10	19	2645	f	111	58
10	20	2646	f	111	62
10	21	2647	f	111	63
10	22	2648	f	111	64
10	23	2649	f	111	65
9	24	2650	f	111	66
9	25	2651	f	111	67
10	26	2652	f	111	68
9	27	2653	f	111	69
2	0	2654	f	112	1
2	1	2655	f	112	2
2	2	2656	f	112	3
2	3	2657	f	112	5
1	4	2658	f	112	6
2	5	2659	f	112	71
5	6	2660	f	112	22
5	7	2661	f	112	23
4	8	2662	f	112	24
5	9	2663	f	112	25
5	10	2664	f	112	26
1	11	2665	f	112	27
2	12	2666	f	112	28
2	13	2667	f	112	29
2	14	2668	f	112	30
2	15	2669	f	112	31
2	16	2670	f	112	32
2	17	2671	f	112	33
2	18	2672	f	112	34
2	19	2673	f	112	35
2	20	2674	f	112	36
2	21	2675	f	112	37
2	22	2676	f	112	38
2	23	2677	f	112	39
2	24	2678	f	112	40
2	25	2679	f	112	41
2	26	2680	f	112	42
1	27	2681	f	112	43
1	28	2682	f	112	44
1	29	2683	f	112	45
1	30	2684	f	112	46
2	31	2685	f	112	47
2	32	2686	f	112	48
2	33	2687	f	112	49
2	34	2688	f	112	50
2	35	2689	f	112	53
2	36	2690	f	112	51
2	37	2691	f	112	52
1	38	2692	f	112	54
9	39	2693	f	112	55
9	40	2694	f	112	56
10	41	2695	f	112	59
9	42	2696	f	112	60
9	43	2697	f	112	61
10	44	2698	f	112	57
9	45	2699	f	112	58
10	46	2700	f	112	62
10	47	2701	f	112	63
9	48	2702	f	112	64
9	49	2703	f	112	65
10	50	2704	f	112	66
9	51	2705	f	112	67
10	52	2706	f	112	68
9	53	2707	f	112	69
1	0	2708	f	113	1
2	1	2709	f	113	2
2	2	2710	f	113	3
1	3	2711	f	113	5
2	4	2712	f	113	6
2	5	2713	f	113	71
1	6	2714	f	113	7
2	7	2715	f	113	8
2	8	2716	f	113	9
5	9	2717	f	113	17
4	10	2718	f	113	18
5	11	2719	f	113	19
5	12	2720	f	113	20
5	13	2721	f	113	21
2	14	2722	f	113	27
2	15	2723	f	113	54
2	0	2724	f	114	1
2	1	2725	f	114	2
1	2	2726	f	114	3
2	3	2727	f	114	5
2	4	2728	f	114	6
2	5	2729	f	114	71
5	6	2730	f	114	13
4	7	2731	f	114	14
4	8	2732	f	114	15
7	9	2733	f	114	16
1	10	2734	f	114	27
2	11	2735	f	114	28
2	12	2736	f	114	29
2	13	2737	f	114	30
2	14	2738	f	114	31
1	15	2739	f	114	32
1	16	2740	f	114	33
2	17	2741	f	114	34
2	18	2742	f	114	35
2	19	2743	f	114	36
2	20	2744	f	114	37
2	21	2745	f	114	38
2	22	2746	f	114	39
2	23	2747	f	114	40
2	24	2748	f	114	41
2	25	2749	f	114	42
1	26	2750	f	114	43
1	27	2751	f	114	44
1	28	2752	f	114	45
1	29	2753	f	114	46
2	30	2754	f	114	47
2	31	2755	f	114	48
2	32	2756	f	114	49
2	33	2757	f	114	50
2	34	2758	f	114	53
2	35	2759	f	114	51
2	36	2760	f	114	52
1	37	2761	f	114	54
10	38	2762	f	114	55
10	39	2763	f	114	56
10	40	2764	f	114	59
9	41	2765	f	114	60
10	42	2766	f	114	61
9	43	2767	f	114	57
10	44	2768	f	114	58
10	45	2769	f	114	62
9	46	2770	f	114	63
9	47	2771	f	114	64
9	48	2772	f	114	65
10	49	2773	f	114	66
9	50	2774	f	114	67
10	51	2775	f	114	68
10	52	2776	f	114	69
2	0	2777	f	115	1
1	1	2778	f	115	2
2	2	2779	f	115	3
1	3	2780	f	115	5
2	4	2781	f	115	6
2	5	2782	f	115	71
1	6	2783	f	115	10
2	7	2784	f	115	11
2	8	2785	f	115	12
5	9	2786	f	115	17
5	10	2787	f	115	18
5	11	2788	f	115	19
4	12	2789	f	115	20
4	13	2790	f	115	21
1	14	2791	f	115	27
2	15	2792	f	115	28
2	16	2793	f	115	29
2	17	2794	f	115	30
2	18	2795	f	115	31
2	19	2796	f	115	32
2	20	2797	f	115	33
2	21	2798	f	115	34
2	22	2799	f	115	35
2	23	2800	f	115	36
2	24	2801	f	115	37
2	25	2802	f	115	38
2	26	2803	f	115	39
2	27	2804	f	115	40
2	28	2805	f	115	41
2	29	2806	f	115	42
1	30	2807	f	115	43
1	31	2808	f	115	44
1	32	2809	f	115	45
1	33	2810	f	115	46
2	34	2811	f	115	47
2	35	2812	f	115	48
2	36	2813	f	115	49
2	37	2814	f	115	50
2	38	2815	f	115	53
2	39	2816	f	115	51
2	40	2817	f	115	52
1	41	2818	f	115	54
9	42	2819	f	115	55
9	43	2820	f	115	56
10	44	2821	f	115	59
9	45	2822	f	115	60
10	46	2823	f	115	61
9	47	2824	f	115	57
9	48	2825	f	115	58
9	49	2826	f	115	62
9	50	2827	f	115	63
10	51	2828	f	115	64
9	52	2829	f	115	65
10	53	2830	f	115	66
10	54	2831	f	115	67
9	55	2832	f	115	68
10	56	2833	f	115	69
2	0	2834	f	116	1
2	1	2835	f	116	2
1	2	2836	f	116	3
2	3	2837	f	116	5
1	4	2838	f	116	6
1	5	2839	f	116	71
5	6	2840	f	116	13
5	7	2841	f	116	14
4	8	2842	f	116	15
7	9	2843	f	116	16
5	10	2844	f	116	22
5	11	2845	f	116	23
5	12	2846	f	116	24
4	13	2847	f	116	25
5	14	2848	f	116	26
2	15	2849	f	116	27
1	16	2850	f	116	54
10	17	2851	f	116	55
10	18	2852	f	116	56
9	19	2853	f	116	59
9	20	2854	f	116	60
10	21	2855	f	116	61
9	22	2856	f	116	57
10	23	2857	f	116	58
10	24	2858	f	116	62
9	25	2859	f	116	63
9	26	2860	f	116	64
10	27	2861	f	116	65
9	28	2862	f	116	66
10	29	2863	f	116	67
10	30	2864	f	116	68
10	31	2865	f	116	69
2	0	2866	f	117	1
2	1	2867	f	117	2
2	2	2868	f	117	3
1	3	2869	f	117	5
2	4	2870	f	117	6
2	5	2871	f	117	71
5	6	2872	f	117	17
4	7	2873	f	117	18
5	8	2874	f	117	19
4	9	2875	f	117	20
5	10	2876	f	117	21
2	11	2877	f	117	27
2	12	2878	f	117	54
2	0	2879	f	119	1
2	1	2880	f	119	2
1	2	2881	f	119	3
2	3	2882	f	119	5
1	4	2883	f	119	6
2	5	2884	f	119	71
4	6	2885	f	119	13
5	7	2886	f	119	14
5	8	2887	f	119	15
7	9	2888	f	119	16
5	10	2889	f	119	22
4	11	2890	f	119	23
5	12	2891	f	119	24
5	13	2892	f	119	25
5	14	2893	f	119	26
2	15	2894	f	119	27
2	16	2895	f	119	54
1	0	2896	f	120	1
2	1	2897	f	120	2
2	2	2898	f	120	3
1	3	2899	f	120	5
2	4	2900	f	120	6
2	5	2901	f	120	71
2	6	2902	f	120	7
2	7	2903	f	120	8
1	8	2904	f	120	9
5	9	2905	f	120	17
4	10	2906	f	120	18
5	11	2907	f	120	19
5	12	2908	f	120	20
4	13	2909	f	120	21
1	14	2910	f	120	27
2	15	2911	f	120	28
2	16	2912	f	120	29
2	17	2913	f	120	30
2	18	2914	f	120	31
2	19	2915	f	120	32
2	20	2916	f	120	33
2	21	2917	f	120	34
2	22	2918	f	120	35
2	23	2919	f	120	36
2	24	2920	f	120	37
2	25	2921	f	120	38
2	26	2922	f	120	39
2	27	2923	f	120	40
2	28	2924	f	120	41
2	29	2925	f	120	42
1	30	2926	f	120	43
1	31	2927	f	120	44
1	32	2928	f	120	45
1	33	2929	f	120	46
2	34	2930	f	120	47
2	35	2931	f	120	48
2	36	2932	f	120	49
2	37	2933	f	120	50
2	38	2934	f	120	53
2	39	2935	f	120	51
2	40	2936	f	120	52
1	41	2937	f	120	54
10	42	2938	f	120	55
9	43	2939	f	120	56
10	44	2940	f	120	59
10	45	2941	f	120	60
9	46	2942	f	120	61
10	47	2943	f	120	57
9	48	2944	f	120	58
10	49	2945	f	120	62
9	50	2946	f	120	63
10	51	2947	f	120	64
10	52	2948	f	120	65
9	53	2949	f	120	66
10	54	2950	f	120	67
9	55	2951	f	120	68
10	56	2952	f	120	69
2	0	2953	f	121	1
2	1	2954	f	121	2
2	2	2955	f	121	3
1	3	2956	f	121	5
2	4	2957	f	121	6
1	5	2958	f	121	71
4	6	2959	f	121	17
5	7	2960	f	121	18
5	8	2961	f	121	19
4	9	2962	f	121	20
5	10	2963	f	121	21
1	11	2964	f	121	27
2	12	2965	f	121	28
2	13	2966	f	121	29
2	14	2967	f	121	30
2	15	2968	f	121	31
2	16	2969	f	121	32
2	17	2970	f	121	33
2	18	2971	f	121	34
2	19	2972	f	121	35
2	20	2973	f	121	36
2	21	2974	f	121	37
2	22	2975	f	121	38
2	23	2976	f	121	39
2	24	2977	f	121	40
2	25	2978	f	121	41
2	26	2979	f	121	42
1	27	2980	f	121	43
1	28	2981	f	121	44
1	29	2982	f	121	45
1	30	2983	f	121	46
2	31	2984	f	121	47
2	32	2985	f	121	48
2	33	2986	f	121	49
2	34	2987	f	121	50
2	35	2988	f	121	53
2	36	2989	f	121	51
2	37	2990	f	121	52
1	38	2991	f	121	54
9	39	2992	f	121	55
10	40	2993	f	121	56
10	41	2994	f	121	59
9	42	2995	f	121	60
10	43	2996	f	121	61
9	44	2997	f	121	57
10	45	2998	f	121	58
10	46	2999	f	121	62
10	47	3000	f	121	63
9	48	3001	f	121	64
10	49	3002	f	121	65
9	50	3003	f	121	66
10	51	3004	f	121	67
10	52	3005	f	121	68
10	53	3006	f	121	69
1	0	3007	f	122	1
2	1	3008	f	122	2
1	2	3009	f	122	3
2	3	3010	f	122	5
2	4	3011	f	122	6
1	5	3012	f	122	71
2	6	3013	f	122	7
1	7	3014	f	122	8
1	8	3015	f	122	9
5	9	3016	f	122	13
4	10	3017	f	122	14
5	11	3018	f	122	15
7	12	3019	f	122	16
1	13	3020	f	122	27
2	14	3021	f	122	28
2	15	3022	f	122	29
2	16	3023	f	122	30
2	17	3024	f	122	31
2	18	3025	f	122	32
2	19	3026	f	122	33
2	20	3027	f	122	34
2	21	3028	f	122	35
2	22	3029	f	122	36
2	23	3030	f	122	37
2	24	3031	f	122	38
2	25	3032	f	122	39
2	26	3033	f	122	40
2	27	3034	f	122	41
2	28	3035	f	122	42
1	29	3036	f	122	43
1	30	3037	f	122	44
1	31	3038	f	122	45
1	32	3039	f	122	46
2	33	3040	f	122	47
2	34	3041	f	122	48
2	35	3042	f	122	49
2	36	3043	f	122	50
2	37	3044	f	122	53
2	38	3045	f	122	51
2	39	3046	f	122	52
1	40	3047	f	122	54
10	41	3048	f	122	55
9	42	3049	f	122	56
10	43	3050	f	122	59
9	44	3051	f	122	60
10	45	3052	f	122	61
10	46	3053	f	122	57
10	47	3054	f	122	58
9	48	3055	f	122	62
10	49	3056	f	122	63
10	50	3057	f	122	64
9	51	3058	f	122	65
10	52	3059	f	122	66
9	53	3060	f	122	67
10	54	3061	f	122	68
10	55	3062	f	122	69
2	0	3063	f	123	1
2	1	3064	f	123	2
1	2	3065	f	123	3
1	3	3066	f	123	5
2	4	3067	f	123	6
2	5	3068	f	123	71
5	6	3069	f	123	13
5	7	3070	f	123	14
4	8	3071	f	123	15
7	9	3072	f	123	16
5	10	3073	f	123	17
4	11	3074	f	123	18
4	12	3075	f	123	19
5	13	3076	f	123	20
5	14	3077	f	123	21
2	15	3078	f	123	27
2	16	3079	f	123	54
2	0	3080	f	124	1
1	1	3081	f	124	2
2	2	3082	f	124	3
2	3	3083	f	124	5
1	4	3084	f	124	6
2	5	3085	f	124	71
2	6	3086	f	124	10
2	7	3087	f	124	11
1	8	3088	f	124	12
4	9	3089	f	124	22
5	10	3090	f	124	23
5	11	3091	f	124	24
5	12	3092	f	124	25
5	13	3093	f	124	26
2	14	3094	f	124	27
2	15	3095	f	124	54
2	0	3096	f	125	1
2	1	3097	f	125	2
1	2	3098	f	125	3
1	3	3099	f	125	5
2	4	3100	f	125	6
2	5	3101	f	125	71
5	6	3102	f	125	13
4	7	3103	f	125	14
5	8	3104	f	125	15
8	9	3105	f	125	16
4	10	3106	f	125	17
5	11	3107	f	125	18
5	12	3108	f	125	19
5	13	3109	f	125	20
4	14	3110	f	125	21
2	15	3111	f	125	27
1	16	3112	f	125	54
9	17	3113	f	125	55
9	18	3114	f	125	56
10	19	3115	f	125	59
9	20	3116	f	125	60
10	21	3117	f	125	61
9	22	3118	f	125	57
9	23	3119	f	125	58
9	24	3120	f	125	62
9	25	3121	f	125	63
9	26	3122	f	125	64
9	27	3123	f	125	65
9	28	3124	f	125	66
9	29	3125	f	125	67
9	30	3126	f	125	68
9	31	3127	f	125	69
2	0	3128	f	126	1
1	1	3129	f	126	2
2	2	3130	f	126	3
2	3	3131	f	126	5
1	4	3132	f	126	6
2	5	3133	f	126	71
2	6	3134	f	126	10
1	7	3135	f	126	11
1	8	3136	f	126	12
5	9	3137	f	126	22
5	10	3138	f	126	23
5	11	3139	f	126	24
4	12	3140	f	126	25
5	13	3141	f	126	26
1	14	3142	f	126	27
2	15	3143	f	126	28
2	16	3144	f	126	29
2	17	3145	f	126	30
2	18	3146	f	126	31
2	19	3147	f	126	32
2	20	3148	f	126	33
2	21	3149	f	126	34
2	22	3150	f	126	35
2	23	3151	f	126	36
2	24	3152	f	126	37
2	25	3153	f	126	38
2	26	3154	f	126	39
2	27	3155	f	126	40
2	28	3156	f	126	41
2	29	3157	f	126	42
1	30	3158	f	126	43
1	31	3159	f	126	44
1	32	3160	f	126	45
1	33	3161	f	126	46
2	34	3162	f	126	47
2	35	3163	f	126	48
2	36	3164	f	126	49
2	37	3165	f	126	50
2	38	3166	f	126	53
2	39	3167	f	126	51
2	40	3168	f	126	52
1	41	3169	f	126	54
10	42	3170	f	126	55
10	43	3171	f	126	56
9	44	3172	f	126	59
10	45	3173	f	126	60
9	46	3174	f	126	61
10	47	3175	f	126	57
10	48	3176	f	126	58
9	49	3177	f	126	62
10	50	3178	f	126	63
9	51	3179	f	126	64
9	52	3180	f	126	65
10	53	3181	f	126	66
10	54	3182	f	126	67
9	55	3183	f	126	68
10	56	3184	f	126	69
2	0	3185	f	127	1
1	1	3186	f	127	2
2	2	3187	f	127	3
1	3	3188	f	127	5
2	4	3189	f	127	6
1	5	3190	f	127	71
2	6	3191	f	127	10
1	7	3192	f	127	11
2	8	3193	f	127	12
5	9	3194	f	127	17
4	10	3195	f	127	18
5	11	3196	f	127	19
5	12	3197	f	127	20
4	13	3198	f	127	21
1	14	3199	f	127	27
2	15	3200	f	127	28
2	16	3201	f	127	29
2	17	3202	f	127	30
2	18	3203	f	127	31
2	19	3204	f	127	32
2	20	3205	f	127	33
2	21	3206	f	127	34
2	22	3207	f	127	35
2	23	3208	f	127	36
2	24	3209	f	127	37
2	25	3210	f	127	38
2	26	3211	f	127	39
2	27	3212	f	127	40
2	28	3213	f	127	41
2	29	3214	f	127	42
1	30	3215	f	127	43
1	31	3216	f	127	44
1	32	3217	f	127	45
1	33	3218	f	127	46
2	34	3219	f	127	47
2	35	3220	f	127	48
2	36	3221	f	127	49
2	37	3222	f	127	50
1	38	3223	f	127	53
2	39	3224	f	127	51
1	40	3225	f	127	52
1	41	3226	f	127	54
10	42	3227	f	127	55
9	43	3228	f	127	56
10	44	3229	f	127	59
10	45	3230	f	127	60
10	46	3231	f	127	61
10	47	3232	f	127	57
10	48	3233	f	127	58
10	49	3234	f	127	62
9	50	3235	f	127	63
10	51	3236	f	127	64
9	52	3237	f	127	65
10	53	3238	f	127	66
10	54	3239	f	127	67
10	55	3240	f	127	68
10	56	3241	f	127	69
2	0	3242	f	128	1
2	1	3243	f	128	2
2	2	3244	f	128	3
1	3	3245	f	128	5
2	4	3246	f	128	6
2	5	3247	f	128	71
5	6	3248	f	128	17
4	7	3249	f	128	18
5	8	3250	f	128	19
5	9	3251	f	128	20
4	10	3252	f	128	21
2	11	3253	f	128	27
1	12	3254	f	128	54
9	13	3255	f	128	55
9	14	3256	f	128	56
9	15	3257	f	128	59
9	16	3258	f	128	60
9	17	3259	f	128	61
9	18	3260	f	128	57
9	19	3261	f	128	58
9	20	3262	f	128	62
9	21	3263	f	128	63
9	22	3264	f	128	64
9	23	3265	f	128	65
9	24	3266	f	128	66
9	25	3267	f	128	67
9	26	3268	f	128	68
9	27	3269	f	128	69
1	0	3270	f	129	1
2	1	3271	f	129	2
2	2	3272	f	129	3
2	3	3273	f	129	5
1	4	3274	f	129	6
2	5	3275	f	129	71
2	6	3276	f	129	7
1	7	3277	f	129	8
2	8	3278	f	129	9
5	9	3279	f	129	22
5	10	3280	f	129	23
5	11	3281	f	129	24
4	12	3282	f	129	25
4	13	3283	f	129	26
2	14	3284	f	129	27
2	15	3285	f	129	54
2	0	3286	f	130	1
1	1	3287	f	130	2
2	2	3288	f	130	3
1	3	3289	f	130	5
2	4	3290	f	130	6
2	5	3291	f	130	71
2	6	3292	f	130	10
1	7	3293	f	130	11
1	8	3294	f	130	12
5	9	3295	f	130	17
5	10	3296	f	130	18
4	11	3297	f	130	19
5	12	3298	f	130	20
5	13	3299	f	130	21
2	14	3300	f	130	27
2	15	3301	f	130	54
1	0	3302	f	132	1
2	1	3303	f	132	2
2	2	3304	f	132	3
2	3	3305	f	132	5
2	4	3306	f	132	6
2	5	3307	f	132	71
2	6	3308	f	132	7
2	7	3309	f	132	8
1	8	3310	f	132	9
1	9	3311	f	132	27
2	10	3312	f	132	28
2	11	3313	f	132	29
2	12	3314	f	132	30
2	13	3315	f	132	31
1	14	3316	f	132	32
1	15	3317	f	132	33
2	16	3318	f	132	34
2	17	3319	f	132	35
2	18	3320	f	132	36
2	19	3321	f	132	37
2	20	3322	f	132	38
2	21	3323	f	132	39
2	22	3324	f	132	40
2	23	3325	f	132	41
2	24	3326	f	132	42
2	25	3327	f	132	43
2	26	3328	f	132	44
2	27	3329	f	132	45
2	28	3330	f	132	46
2	29	3331	f	132	47
2	30	3332	f	132	48
2	31	3333	f	132	49
2	32	3334	f	132	50
2	33	3335	f	132	53
1	34	3336	f	132	51
2	35	3337	f	132	52
2	36	3338	f	132	54
2	0	3339	f	133	1
1	1	3340	f	133	2
2	2	3341	f	133	3
2	3	3342	f	133	5
2	4	3343	f	133	6
2	5	3344	f	133	71
2	6	3345	f	133	10
2	7	3346	f	133	11
1	8	3347	f	133	12
1	9	3348	f	133	27
2	10	3349	f	133	28
2	11	3350	f	133	29
2	12	3351	f	133	30
2	13	3352	f	133	31
2	14	3353	f	133	32
2	15	3354	f	133	33
2	16	3355	f	133	34
2	17	3356	f	133	35
2	18	3357	f	133	36
2	19	3358	f	133	37
2	20	3359	f	133	38
2	21	3360	f	133	39
2	22	3361	f	133	40
2	23	3362	f	133	41
2	24	3363	f	133	42
2	25	3364	f	133	43
2	26	3365	f	133	44
2	27	3366	f	133	45
2	28	3367	f	133	46
2	29	3368	f	133	47
2	30	3369	f	133	48
2	31	3370	f	133	49
2	32	3371	f	133	50
2	33	3372	f	133	53
1	34	3373	f	133	51
2	35	3374	f	133	52
1	36	3375	f	133	54
9	37	3376	f	133	55
9	38	3377	f	133	56
10	39	3378	f	133	59
9	40	3379	f	133	60
10	41	3380	f	133	61
9	42	3381	f	133	57
9	43	3382	f	133	58
9	44	3383	f	133	62
9	45	3384	f	133	63
9	46	3385	f	133	64
9	47	3386	f	133	65
9	48	3387	f	133	66
9	49	3388	f	133	67
9	50	3389	f	133	68
9	51	3390	f	133	69
2	0	3391	f	134	1
1	1	3392	f	134	2
2	2	3393	f	134	3
1	3	3394	f	134	5
2	4	3395	f	134	6
2	5	3396	f	134	71
1	6	3397	f	134	10
2	7	3398	f	134	11
1	8	3399	f	134	12
4	9	3400	f	134	17
5	10	3401	f	134	18
4	11	3402	f	134	19
4	12	3403	f	134	20
5	13	3404	f	134	21
1	14	3405	f	134	27
2	15	3406	f	134	28
2	16	3407	f	134	29
2	17	3408	f	134	30
2	18	3409	f	134	31
2	19	3410	f	134	32
2	20	3411	f	134	33
2	21	3412	f	134	34
2	22	3413	f	134	35
2	23	3414	f	134	36
2	24	3415	f	134	37
2	25	3416	f	134	38
2	26	3417	f	134	39
2	27	3418	f	134	40
2	28	3419	f	134	41
2	29	3420	f	134	42
1	30	3421	f	134	43
1	31	3422	f	134	44
1	32	3423	f	134	45
1	33	3424	f	134	46
2	34	3425	f	134	47
2	35	3426	f	134	48
2	36	3427	f	134	49
2	37	3428	f	134	50
2	38	3429	f	134	53
2	39	3430	f	134	51
2	40	3431	f	134	52
1	41	3432	f	134	54
10	42	3433	f	134	55
10	43	3434	f	134	56
9	44	3435	f	134	59
9	45	3436	f	134	60
10	46	3437	f	134	61
10	47	3438	f	134	57
9	48	3439	f	134	58
10	49	3440	f	134	62
9	50	3441	f	134	63
9	51	3442	f	134	64
10	52	3443	f	134	65
9	53	3444	f	134	66
10	54	3445	f	134	67
10	55	3446	f	134	68
9	56	3447	f	134	69
2	0	3448	f	135	1
2	1	3449	f	135	2
2	2	3450	f	135	3
1	3	3451	f	135	5
2	4	3452	f	135	6
2	5	3453	f	135	71
5	6	3454	f	135	17
4	7	3455	f	135	18
5	8	3456	f	135	19
5	9	3457	f	135	20
4	10	3458	f	135	21
1	11	3459	f	135	27
2	12	3460	f	135	28
2	13	3461	f	135	29
2	14	3462	f	135	30
2	15	3463	f	135	31
2	16	3464	f	135	32
2	17	3465	f	135	33
2	18	3466	f	135	34
2	19	3467	f	135	35
2	20	3468	f	135	36
2	21	3469	f	135	37
2	22	3470	f	135	38
2	23	3471	f	135	39
2	24	3472	f	135	40
2	25	3473	f	135	41
2	26	3474	f	135	42
2	27	3475	f	135	43
2	28	3476	f	135	44
2	29	3477	f	135	45
2	30	3478	f	135	46
2	31	3479	f	135	47
2	32	3480	f	135	48
2	33	3481	f	135	49
2	34	3482	f	135	50
2	35	3483	f	135	53
1	36	3484	f	135	51
2	37	3485	f	135	52
1	38	3486	f	135	54
9	39	3487	f	135	55
9	40	3488	f	135	56
10	41	3489	f	135	59
10	42	3490	f	135	60
9	43	3491	f	135	61
9	44	3492	f	135	57
9	45	3493	f	135	58
9	46	3494	f	135	62
9	47	3495	f	135	63
9	48	3496	f	135	64
9	49	3497	f	135	65
9	50	3498	f	135	66
9	51	3499	f	135	67
10	52	3500	f	135	68
9	53	3501	f	135	69
1	0	3502	f	136	1
1	1	3503	f	136	2
1	2	3504	f	136	3
1	3	3505	f	136	5
1	4	3506	f	136	6
2	5	3507	f	136	7
2	6	3508	f	136	8
2	7	3509	f	136	9
2	8	3510	f	136	10
2	9	3511	f	136	11
2	10	3512	f	136	12
5	11	3513	f	136	13
5	12	3514	f	136	14
5	13	3515	f	136	15
7	14	3516	f	136	16
5	15	3517	f	136	17
5	16	3518	f	136	18
5	17	3519	f	136	19
5	18	3520	f	136	20
5	19	3521	f	136	21
5	20	3522	f	136	22
5	21	3523	f	136	23
5	22	3524	f	136	24
5	23	3525	f	136	25
5	24	3526	f	136	26
1	25	3527	f	136	27
2	26	3528	f	136	28
2	27	3529	f	136	29
2	28	3530	f	136	30
2	29	3531	f	136	31
2	30	3532	f	136	32
2	31	3533	f	136	33
2	32	3534	f	136	34
2	33	3535	f	136	35
2	34	3536	f	136	36
2	35	3537	f	136	37
2	36	3538	f	136	38
2	37	3539	f	136	39
2	38	3540	f	136	40
2	39	3541	f	136	41
2	40	3542	f	136	42
2	41	3543	f	136	43
2	42	3544	f	136	44
2	43	3545	f	136	45
2	44	3546	f	136	46
2	45	3547	f	136	47
2	46	3548	f	136	48
2	47	3549	f	136	49
2	48	3550	f	136	50
2	49	3551	f	136	53
2	50	3552	f	136	51
2	51	3553	f	136	52
2	52	3554	f	136	71
1	53	3555	f	136	54
9	54	3556	f	136	55
9	55	3557	f	136	56
9	56	3558	f	136	59
9	57	3559	f	136	60
9	58	3560	f	136	61
9	59	3561	f	136	57
9	60	3562	f	136	58
9	61	3563	f	136	62
9	62	3564	f	136	63
9	63	3565	f	136	64
9	64	3566	f	136	65
9	65	3567	f	136	66
9	66	3568	f	136	67
9	67	3569	f	136	68
9	68	3570	f	136	69
\.


--
-- Name: probe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probe_id_seq', 3570, true);


--
-- Data for Name: proberesource; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesource (name, path, id, label, deleted, probetype) FROM stdin;
\.


--
-- Name: proberesource_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesource_id_seq', 4, true);


--
-- Data for Name: proberesult; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesult (name, id, deleted, classname, positive_c) FROM stdin;
w normie	2	f	inNorm	t
poniżej normy	1	f	belowNorm	f
tak	4	f	belowNorm	f
wolne	6	f	belowNorm	f
szybkie	8	f	belowNorm	f
niezaliczone	10	f	belowNorm	f
normalne	7	f	someColor	t
nie	5	f	inNorm	t
zaliczone	9	f	inNorm	t
\.


--
-- Name: proberesult_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesult_id_seq', 10, true);


--
-- Data for Name: proberesultprobetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY proberesultprobetype (id, deleted, probetype, proberesult, default_c) FROM stdin;
205	f	1	1	f
206	f	1	2	f
207	f	2	1	f
208	f	2	2	f
209	f	3	1	f
210	f	3	2	f
211	f	5	1	f
212	f	5	2	f
213	f	6	1	f
214	f	6	2	f
215	f	7	1	f
216	f	7	2	f
217	f	8	1	f
218	f	8	2	f
219	f	9	1	f
220	f	9	2	f
221	f	10	1	f
222	f	10	2	f
223	f	11	1	f
224	f	11	2	f
225	f	12	1	f
226	f	12	2	f
227	f	13	4	f
229	f	14	4	f
234	f	28	1	f
235	f	28	2	f
236	f	29	1	f
237	f	29	2	f
238	f	27	1	f
239	f	27	2	f
240	f	30	1	f
241	f	30	2	f
242	f	31	1	f
243	f	31	2	f
244	f	32	1	f
245	f	32	2	f
246	f	33	1	f
247	f	33	2	f
248	f	34	1	f
249	f	34	2	f
250	f	35	1	f
251	f	35	2	f
252	f	36	1	f
253	f	36	2	f
254	f	37	1	f
255	f	37	2	f
256	f	38	1	f
257	f	38	2	f
258	f	39	1	f
259	f	39	2	f
260	f	40	1	f
261	f	40	2	f
262	f	41	1	f
263	f	41	2	f
264	f	53	1	f
265	f	53	2	f
266	f	54	1	f
267	f	54	2	f
268	f	42	1	f
269	f	42	2	f
270	f	43	1	f
271	f	43	2	f
272	f	44	1	f
273	f	44	2	f
274	f	45	1	f
275	f	45	2	f
276	f	46	1	f
277	f	46	2	f
278	f	47	1	f
279	f	47	2	f
280	f	48	1	f
281	f	48	2	f
282	f	49	1	f
283	f	49	2	f
284	f	50	1	f
285	f	50	2	f
286	f	51	1	f
287	f	51	2	f
288	f	52	1	f
289	f	52	2	f
291	f	55	10	f
293	f	56	10	f
295	f	59	10	f
297	f	60	10	f
299	f	61	10	f
301	f	57	10	f
303	f	58	10	f
305	f	62	10	f
307	f	63	10	f
309	f	64	10	f
311	f	65	10	f
313	f	66	10	f
315	f	67	10	f
317	f	68	10	f
319	f	69	10	f
231	f	16	6	f
233	f	16	8	f
320	f	15	4	f
290	f	55	9	t
292	f	56	9	t
232	f	16	7	t
228	f	13	5	t
230	f	14	5	t
294	f	59	9	t
322	f	17	4	f
324	f	18	4	f
326	f	19	4	f
328	f	20	4	f
330	f	21	4	f
332	f	22	4	f
334	f	23	4	f
336	f	24	4	f
338	f	25	4	f
340	f	26	4	f
321	f	15	5	t
323	f	17	5	t
325	f	18	5	t
327	f	19	5	t
329	f	20	5	t
331	f	21	5	t
333	f	22	5	t
335	f	23	5	t
337	f	24	5	t
339	f	25	5	t
341	f	26	5	t
296	f	60	9	t
298	f	61	9	t
300	f	57	9	t
302	f	58	9	t
304	f	62	9	t
306	f	63	9	t
308	f	64	9	t
310	f	65	9	t
312	f	66	9	t
314	f	67	9	t
316	f	68	9	t
318	f	69	9	t
344	f	71	1	f
345	f	71	2	f
\.


--
-- Name: proberesultprobetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('proberesultprobetype_id_seq', 345, true);


--
-- Data for Name: probetreenode; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetreenode (id, deleted, nodeid, probe, childnodeid, probetype) FROM stdin;
\.


--
-- Name: probetreenode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetreenode_id_seq', 75, true);


--
-- Data for Name: probetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetype (name, id, deleted, initial_c, dependson, listnumber) FROM stdin;
a	28	f	f	27	28
e	29	f	f	27	28
Artykulacja	27	f	t	\N	27
i	30	f	f	27	28
u	31	f	f	27	28
ę	32	f	f	27	28
ą	33	f	f	27	28
y	34	f	f	27	28
p	35	f	f	27	35
b	36	f	f	27	35
w	37	f	f	27	37
f	38	f	f	27	37
tiki	18	f	f	5	5
s	39	f	f	27	39
z	40	f	f	27	39
drzenia	19	f	f	5	5
c	41	f	f	27	39
g	53	f	f	27	51
Motoryka artykulacyjna	54	f	t	\N	54
brak kontaktu wzrokowego	21	f	f	5	5
nosowy glos	22	f	f	6	6
próba jezyka 1	55	f	f	54	55
próba jezyka 2	56	f	f	54	55
próba jezyka 5	59	f	f	54	55
próba jezyka 6	60	f	f	54	55
próba jezyka 7	61	f	f	54	55
próba jezyka 3	57	f	f	54	55
próba jezyka 4	58	f	f	54	55
ochryply glos	23	f	f	6	6
zbyt wyskoki glos	24	f	f	6	6
zbyt niski glos	25	f	f	6	6
załamiania głosu	26	f	f	6	6
Słownictwo i formy wypowiedzi	1	f	t	\N	0
Tempo mowy i oddech	2	f	t	\N	0
Fonacja	6	f	t	\N	0
Rozumienie wypowiedzi	8	f	f	1	1
Myslenie przyczynowo skutkowe	9	f	f	1	1
Akcent	10	f	f	2	2
Intonacja	11	f	f	2	2
Rytm	12	f	f	2	2
dz	42	f	f	27	39
sz	43	f	f	27	43
ż	44	f	f	27	43
cz	45	f	f	27	43
dż	46	f	f	27	43
ś	47	f	f	27	47
ź	48	f	f	27	47
ć	49	f	f	27	47
dź	50	f	f	27	47
h	51	f	f	27	51
k	52	f	f	27	51
r	71	f	f	27	52
Formy wypowiedzi	7	f	f	1	1
próba warg 1	62	f	f	54	62
próba warg 2	63	f	f	54	62
próba warg 3	64	f	f	54	62
próba warg 3	65	f	f	54	62
próba warg 4	66	f	f	54	62
próba warg 5	67	f	f	54	62
próba warg 6	68	f	f	54	62
próba warg 7	69	f	f	54	62
Zachowania towarzyszace mówieniu	3	f	t	\N	0
Elementy prozodyczne	5	f	t	\N	0
Oddech przyspieszony	13	f	f	3	3
Oddech przerywany	14	f	f	3	3
Oddech przez usta	15	f	f	3	3
Tempo mowy	16	f	f	3	3
Współruchy	17	f	f	5	5
bawienie sie przedmiotami/palacmi	20	f	f	5	5
\.


--
-- Name: probetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetype_id_seq', 71, true);


--
-- Data for Name: probetypeprobetype; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetypeprobetype (id, parentprobe, childprobe) FROM stdin;
1	1	7
2	1	8
3	1	9
4	5	10
5	5	11
6	5	12
7	2	13
8	2	14
9	2	15
10	2	16
11	3	17
12	3	18
13	3	19
14	3	20
15	3	21
16	6	22
17	6	23
18	6	24
19	6	25
20	6	26
21	27	28
22	27	29
23	27	30
24	27	31
25	27	32
26	27	33
27	27	34
28	27	35
29	27	36
30	27	37
31	27	38
32	27	39
33	27	40
34	27	41
35	27	42
36	27	43
37	27	44
38	27	45
39	27	46
40	27	47
41	27	48
42	27	49
43	27	50
44	27	51
45	27	52
46	27	53
47	54	55
48	54	56
49	54	59
50	54	60
51	54	61
52	54	57
53	54	58
54	54	62
55	54	63
56	54	64
57	54	65
58	54	66
59	54	67
60	54	68
61	54	69
\.


--
-- Name: probetypeprobetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetypeprobetype_id_seq', 61, true);


--
-- Data for Name: probetypetag; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY probetypetag (id, tag, probetype) FROM stdin;
1	2	7
2	2	8
3	2	9
4	3	17
5	3	18
6	3	19
7	3	20
8	3	21
9	4	13
10	4	14
11	4	15
12	4	16
13	5	10
14	5	11
15	5	12
16	6	22
17	6	23
18	6	24
19	6	25
20	6	26
21	27	28
22	27	29
23	27	30
24	27	31
25	27	32
26	27	33
27	27	34
28	27	35
29	27	36
30	27	37
31	27	38
32	27	39
33	27	40
34	27	41
35	27	42
36	27	43
37	27	44
38	27	45
39	27	46
40	27	47
41	27	48
42	27	49
43	27	50
44	27	51
45	27	52
46	27	53
47	28	55
48	28	56
49	28	59
50	28	60
51	28	61
52	28	57
53	28	58
54	28	62
55	28	63
56	28	64
57	28	65
58	28	66
59	28	67
60	28	68
61	28	69
62	69	55
63	69	56
64	69	59
65	69	60
66	69	61
67	69	57
68	69	58
69	70	62
70	70	63
71	70	64
72	70	65
73	70	66
74	70	67
75	70	68
76	70	69
77	71	28
78	71	29
79	71	30
80	71	31
81	71	32
82	71	33
83	71	34
84	72	35
85	72	36
86	73	37
87	73	38
88	74	39
89	74	40
90	74	41
91	74	42
92	75	43
93	75	44
94	75	45
95	75	46
96	76	47
97	76	48
98	76	49
99	76	50
100	77	53
101	77	51
102	77	52
\.


--
-- Name: probetypetag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('probetypetag_id_seq', 102, true);


--
-- Data for Name: school; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY school (name, id, deleted) FROM stdin;
UP - dane uczące	3	f
\.


--
-- Name: school_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('school_id_seq', 3, true);


--
-- Data for Name: session_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY session_t (hash, open_c, id, owner, deleted, creationdate, school, examinedate, ispubliceditable) FROM stdin;
TqnL2EqUmJnl/orovFWkMQ==	t	13	1	f	2012-12-07	3	2013-01-09	f
\.


--
-- Name: session_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('session_t_id_seq', 16, true);


--
-- Data for Name: suggestion; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY suggestion (name, id, description, deleted, suggestiongroup) FROM stdin;
\.


--
-- Name: suggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('suggestion_id_seq', 2, true);


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY tag (name, id, vectorpos) FROM stdin;
g	1	\N
Słownictwo i formy wypowiedzi	2	\N
Zachowania towrzyszace mowieniu	3	\N
Tempo mowy i oddech	4	\N
Elementy Prozodyczne	5	\N
Fonacja	6	\N
Formy wypoiedzi	7	\N
Rozumienie wypowiedzi	8	\N
Myslenie przyczynowo skutkowe	9	\N
Akcent	10	\N
Intonacja	11	\N
Rytm	12	\N
oddech przyspieszony	13	\N
oddech przerywany	14	\N
oddech przez usta	15	\N
temo mowy	16	\N
wspolruchy	17	\N
tiki	18	\N
drzenia	19	\N
bawienie sie przedmiotami/palacmi	20	\N
brak kontaktu wzrokowego	21	\N
nosowy glos	22	\N
ochryply glos	23	\N
zbyt wyskoki glos	24	\N
zbyt niski glos	25	\N
załamiania głosu	26	\N
Artykulacja	27	\N
Motoryka artykulacyjna	28	\N
a	29	\N
e	30	\N
i	31	\N
u	32	\N
ę	33	\N
ą	34	\N
y	35	\N
p	36	\N
b	37	\N
w	38	\N
f	39	\N
s	40	\N
z	41	\N
c	42	\N
dz	43	\N
sz	44	\N
ż	45	\N
cz	46	\N
dż	47	\N
ś	48	\N
ź	49	\N
ć	50	\N
dź	51	\N
h	52	\N
k	53	\N
próba jezyka 1	54	\N
próba jezyka 2	55	\N
próba jezyka 5	56	\N
próba jezyka 6	57	\N
próba jezyka 7	58	\N
próba jezyka 3	59	\N
próba jezyka 4	60	\N
proba warg 1	61	\N
proba warg 2	62	\N
proba warg 3	63	\N
proba warg 3	64	\N
proba warg 4	65	\N
proba warg 5	66	\N
proba warg 6	67	\N
proba warg 7	68	\N
próba jezyka	69	\N
proba warg	70	\N
samogłoski	71	\N
wargowe	72	\N
wargowo zębowe	73	\N
przednio jezykowo zebowe	74	\N
przednio jezykowo dziąsłowe	75	\N
średnio językowe	76	\N
tylno językowe	77	\N
\.


--
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('tag_id_seq', 77, true);


--
-- Data for Name: user_t; Type: TABLE DATA; Schema: public; Owner: sieve
--

COPY user_t (locale, id, validated, password_pw, password_slt, title, uniqueid, email, timezone, firstname, lastname, superuser) FROM stdin;
pl_PL	1	t	l4kbQ5EqwkWCxw4F00JfcBycNHY=	T3AT21ZMQNZARB2Y	1	QSP1VNAESAMQE0PSAQ3FFTAOZB5SIBOH	a@a.pl	Europe/Warsaw	Default		f
\.


--
-- Name: user_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sieve
--

SELECT pg_catalog.setval('user_t_id_seq', 1, true);


--
-- Name: case_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY case_t
    ADD CONSTRAINT case_t_pk PRIMARY KEY (id);


--
-- Name: child_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY child
    ADD CONSTRAINT child_pk PRIMARY KEY (id);


--
-- Name: diagnosis_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosis
    ADD CONSTRAINT diagnosis_pk PRIMARY KEY (id);


--
-- Name: diagnosisdisorder_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosisdisorder
    ADD CONSTRAINT diagnosisdisorder_pk PRIMARY KEY (id);


--
-- Name: diagnosissuggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY diagnosissuggestion
    ADD CONSTRAINT diagnosissuggestion_pk PRIMARY KEY (id);


--
-- Name: disorder_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY disorder
    ADD CONSTRAINT disorder_pk PRIMARY KEY (id);


--
-- Name: disordersuggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY disordersuggestion
    ADD CONSTRAINT disordersuggestion_pk PRIMARY KEY (id);


--
-- Name: probe_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probe
    ADD CONSTRAINT probe_pk PRIMARY KEY (id);


--
-- Name: proberesource_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesource
    ADD CONSTRAINT proberesource_pk PRIMARY KEY (id);


--
-- Name: proberesult_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesult
    ADD CONSTRAINT proberesult_pk PRIMARY KEY (id);


--
-- Name: proberesultprobetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY proberesultprobetype
    ADD CONSTRAINT proberesultprobetype_pk PRIMARY KEY (id);


--
-- Name: probetreenode_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetreenode
    ADD CONSTRAINT probetreenode_pk PRIMARY KEY (id);


--
-- Name: probetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetype
    ADD CONSTRAINT probetype_pk PRIMARY KEY (id);


--
-- Name: probetypeprobetype_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetypeprobetype
    ADD CONSTRAINT probetypeprobetype_pk PRIMARY KEY (id);


--
-- Name: probetypetag_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY probetypetag
    ADD CONSTRAINT probetypetag_pk PRIMARY KEY (id);


--
-- Name: school_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY school
    ADD CONSTRAINT school_pk PRIMARY KEY (id);


--
-- Name: session_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY session_t
    ADD CONSTRAINT session_t_pk PRIMARY KEY (id);


--
-- Name: suggestion_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY suggestion
    ADD CONSTRAINT suggestion_pk PRIMARY KEY (id);


--
-- Name: tag_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pk PRIMARY KEY (id);


--
-- Name: user_t_pk; Type: CONSTRAINT; Schema: public; Owner: sieve; Tablespace: 
--

ALTER TABLE ONLY user_t
    ADD CONSTRAINT user_t_pk PRIMARY KEY (id);


--
-- Name: case_t_childfield; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX case_t_childfield ON case_t USING btree (childfield);


--
-- Name: case_t_session_c; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX case_t_session_c ON case_t USING btree (session_c);


--
-- Name: diagnosis_mycase; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosis_mycase ON diagnosis USING btree (mycase);


--
-- Name: diagnosisdisorder_diagnosis; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosisdisorder_diagnosis ON diagnosisdisorder USING btree (diagnosis);


--
-- Name: diagnosisdisorder_disorder; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosisdisorder_disorder ON diagnosisdisorder USING btree (disorder);


--
-- Name: diagnosissuggestion_diagnosis; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosissuggestion_diagnosis ON diagnosissuggestion USING btree (diagnosis);


--
-- Name: diagnosissuggestion_suggestion; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX diagnosissuggestion_suggestion ON diagnosissuggestion USING btree (suggestion);


--
-- Name: disordersuggestion_disorder; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX disordersuggestion_disorder ON disordersuggestion USING btree (disorder);


--
-- Name: disordersuggestion_suggestion; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX disordersuggestion_suggestion ON disordersuggestion USING btree (suggestion);


--
-- Name: probe_mycase; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_mycase ON probe USING btree (mycase);


--
-- Name: probe_mytype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_mytype ON probe USING btree (mytype);


--
-- Name: probe_result; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probe_result ON probe USING btree (result);


--
-- Name: proberesource_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesource_probetype ON proberesource USING btree (probetype);


--
-- Name: proberesultprobetype_proberesult; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesultprobetype_proberesult ON proberesultprobetype USING btree (proberesult);


--
-- Name: proberesultprobetype_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX proberesultprobetype_probetype ON proberesultprobetype USING btree (probetype);


--
-- Name: probetreenode_probe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetreenode_probe ON probetreenode USING btree (probe);


--
-- Name: probetreenode_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetreenode_probetype ON probetreenode USING btree (probetype);


--
-- Name: probetype_dependson; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetype_dependson ON probetype USING btree (dependson);


--
-- Name: probetypeprobetype_childprobe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypeprobetype_childprobe ON probetypeprobetype USING btree (childprobe);


--
-- Name: probetypeprobetype_parentprobe; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypeprobetype_parentprobe ON probetypeprobetype USING btree (parentprobe);


--
-- Name: probetypetag_probetype; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypetag_probetype ON probetypetag USING btree (probetype);


--
-- Name: probetypetag_tag; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX probetypetag_tag ON probetypetag USING btree (tag);


--
-- Name: session_t_owner; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX session_t_owner ON session_t USING btree (owner);


--
-- Name: session_t_school; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX session_t_school ON session_t USING btree (school);


--
-- Name: user_t_email; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX user_t_email ON user_t USING btree (email);


--
-- Name: user_t_uniqueid; Type: INDEX; Schema: public; Owner: sieve; Tablespace: 
--

CREATE INDEX user_t_uniqueid ON user_t USING btree (uniqueid);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

