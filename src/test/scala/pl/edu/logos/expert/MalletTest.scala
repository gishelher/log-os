package pl.edu.logos.expert

import pl.edu.logos.model.{Disorder, Case}
import pl.edu.logos.util.{CommonTestUtil, CommonTest}
import cc.mallet.classify.NaiveBayesTrainer
import net.liftweb.common.Full
import CommonTestUtil._

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 13.12.12
 * Time: 22:31
 * To change this template use File | Settings | File Templates.
 */
class MalletTest extends CommonTest {
  "Mallet manager" should {
    "do sth" in rolledBackTransaction {

      val data = Case.findAll
        .filterNot(_.diagnosis.isEmpty)

      forall(Disorder.findAll())(
        disorder => {
          var time = System.nanoTime()
          val iList =
            mesure(
              Full("")
                .map(d => mesure(Case.findAll.filterNot(_.diagnosis.isEmpty), "quering"))
                .map(d => mesure(MalletManager.toTrainInstancesList(d), "mappping"))
                .map(d => mesure(MalletManager.addDisorders(d), "adding disorders"))
                .map(d => mesure(MalletManager.markAllForDisorder(disorder, d), "marking for disorder"))
                .openTheBox, "total creating"
            )


          val classifier = (new NaiveBayesTrainer).train(iList)

          0.6 must be_>(0.5)
        }
      )
    }
  }
}
