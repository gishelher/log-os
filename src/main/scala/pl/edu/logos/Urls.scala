package pl.edu.logos

import model.{SieveProps, User}
import net.liftweb.sitemap.{Loc, Menu}
import net.liftweb.http.S
import net.liftweb.sitemap.Loc.LocParam
import java.net.URLEncoder

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 7:57 PM
 * To change this template use File | Settings | File Templates.
 */
object Urls {
  var entries = List[Menu]()


  val SessionList = UrlEntry("sessions" :: "list" :: Nil, User.loginFirst)
  val SessionView = UrlEntry("sessions" :: "view" :: Nil, User.loginFirst)
  val ProbeResourceView = UrlEntry("resources" :: "view" :: Nil, User.loginFirst)
  val Conduct = UrlEntry("case" :: "conduct" :: Nil, User.loginFirst)
  val Diagnosis = UrlEntry("case" :: "diagnosis" :: Nil, User.loginFirst)
  val Suggestions = UrlEntry("case" :: "suggestions" :: Nil, User.loginFirst)
  val SessionCreateView = UrlEntry("sessions" :: "create" :: Nil)
  val SchoolView = UrlEntry("school" :: "view" :: Nil, User.loginFirst)
  val CaseDocs = UrlEntry("case" :: "docs" :: Nil, User.loginFirst)
  val CaseView = UrlEntry("case" :: "case-view" :: Nil, User.loginFirst)
  val ExpertAdmin = UrlEntry("expert" :: "classifierAdmin" :: Nil, User.loginFirst)


  def imagePath(path: String) = SieveProps.URL.is + "/" + path

  val diagnosisPathList = "diagnosis" :: "files" :: Nil

  def diagnosisPath(path: String, id: Long) = SieveProps.URL.is + diagnosisPathList.mkString("/", "/", "&id=%d".format(id))


  val caseReviewPathList = "diagnosis" :: "files" :: Nil

  def caseReviewPath(path: String, id: Long) = SieveProps.URL.is + caseReviewPathList.mkString("/", "/", "&id=%d".format(id))

}

case class UrlEntry(path: List[String], params: LocParam[Unit]*) {
  def entry = Menu(Loc(path.mkString("/"), path, S.?(path.last), params: _*))

  Urls.entries = entry :: Urls.entries

  val is = path.mkString("/", "/", "")

  def params(params: (String, String)*): String =
    "%s?%s".format(is, params.map(makePair _).mkString("&"))


  def makePair(el: (String, String)) = "%s=%s".format(URLEncoder.encode(el._1, "UTF-8"), URLEncoder.encode(el._2, "UTF-8"))
}
