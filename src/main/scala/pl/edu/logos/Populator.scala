package pl.edu.logos

import model.{ProbeResult, Tag, ProbeType}
import net.liftweb.mapper._

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 26.11.12
 * Time: 08:19
 * To change this template use File | Settings | File Templates.
 */
object Populator {


  def addTag(name: String, probes: List[ProbeType]) {

    val tag = Tag.find(By(Tag.name, name)).openOr(Tag.create.name(name).saveMe())

    probes.foreach(pt => {
      pt.tags += tag
      pt.saveMe()
    })
  }

  def assignAnswers() {

    List(
      (1, 12, ProbeResult.standrd),
      (13, 14, ProbeResult.yesNo),
      (15, 15, ProbeResult.speechTempoOpt),
      (27, 54, ProbeResult.standrd),
      (55, 69, ProbeResult.acceptedOpt)
    ).map {
      case (from, to, what) =>
        ProbeType
          .findAll(By_<=(ProbeType.id, to),
          By_>=(ProbeType.id, from)
        ).foreach {
          pt =>
            what.foreach(res => pt.possibleAnswers += res)
            pt.saveMe()
        }
    }


  }


}
