package pl.edu.logos.expert

import pl.edu.logos.model.{Case, Diagnosis}
import cc.mallet.classify.Classifier
import pl.edu.logos.expert.MalletManager._
import pl.edu.logos.expert.MalletClassifierSelector.TrainerActor
import net.liftweb.common.Loggable
import net.liftweb.mapper.By

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/26/12
 * Time: 11:53 PM
 * To change this template use File | Settings | File Templates.
 */
object DiagnosisCreator extends Loggable {


  def createDiagnose(c: Case): Diagnosis = {
    val diagnosis = Diagnosis.create.myCase(c)

    val vector = CaseInstance(c)

    logger.info("got disorders: " + diagnosis.disorders.map(_.name.is).mkString)

    val out = TrainerActor.currentClassifiers.filter(el => hasIt(vector, el._2)).map(_._1)
    logger.info("########### got disorders: " + out)

    diagnosis.disorders ++= out

    diagnosis.saveMe()
  }

}
