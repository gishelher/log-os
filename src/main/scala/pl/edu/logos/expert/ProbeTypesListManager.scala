package pl.edu.logos.expert

import pl.edu.logos.model.{ProbeResult, ProbeType, Case}


/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 03.12.12
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
object ProbeTypesListManager {


  def getCurrentList(myCase: Case): Seq[(ProbeType, ProbeResult)] = {

    val currentTypes = myCase.probes.flatMap(probe => probe.myType.obj.map(el => el -> probe.result.obj.map(_.positive.is).getOrElse(false))).toSet

    //select only types that (don't have dependency or dependency is confirmed) and is not contained in prev results
    ProbeType.findAll().filter(
      pt => pt.dependsOn.obj.map(dep => currentTypes.contains(dep -> false)).getOrElse(true) &&
        !(currentTypes.contains(pt -> true) || currentTypes.contains(pt -> false))
      //groups by list number and gets least
    ).groupBy(_.listNumber.is) match {
      case l if l.isEmpty => Nil
      case l => l.minBy(_._1)._2.map(pt => pt -> pt.defaultResult)
    }
  }

  def getListForEmptyProbes : Seq[(ProbeType, ProbeResult)] = {
    val currentTypes = Nil
    ProbeType.findAll().filter(
      pt => pt.dependsOn.obj.map(dep => currentTypes.contains(dep -> false)).getOrElse(true) &&
        !(currentTypes.contains(pt -> true) || currentTypes.contains(pt -> false))
      //groups by list number and gets least
    ).groupBy(_.listNumber.is) match {
      case l if l.isEmpty => Nil
      case l => l.minBy(_._1)._2.map(pt => pt -> pt.defaultResult)
    }
  }

}
