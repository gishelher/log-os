package pl.edu.logos.expert

import cc.mallet.types._
import pl.edu.logos.model.{Disorder, Tag, Case}
import scala.collection.JavaConversions._
import pl.edu.logos.util.{BadClassifier, DataException}
import cc.mallet.classify.{Classifier, ClassifierTrainer}
import net.liftweb.common.Loggable
import pl.edu.logos.util.CommonTestUtil._


/**
 * mange mallet classificators
 */
object MalletManager extends Loggable {


  /**
   * class for marking source create from case
   * @param c
   * @param disorders
   */
  case class CaseSource(c: Case, disorders: List[Disorder] = Nil)

  /**
   * mallet alphabet for tags
   */
  lazy val tagsAlphabet = new Alphabet(Tag.findAll().map(_.id.is.asInstanceOf[Object]).toArray)
  lazy val tagsMap = Map(Tag.findAll().map(_.id.is).map(id => id -> tagsAlphabet.lookupIndex(id)): _*)


  /**
   * mapping boolean to mallet alphabet
   */
  lazy val outAlphabet = {
    val v = new LabelAlphabet()
    v.lookupLabel(true)
    v.lookupLabel(false)
    v
  }

  /**
   * maps case to mallet vector
   * @param c
   * @return
   */
  def case2Vector(c: Case): FeatureVector = {
    val base = BaseVectorUtil.tagsMap(c)

    val fi = base.map(el => tagsMap(el._1)).toArray

    new FeatureVector(tagsAlphabet, fi, base.map(_._2).toArray)
  }

  /**
   * mallet instance for case
   * @param c
   */
  case class CaseInstance(c: Case) extends Instance(case2Vector(c), null, "case:" + c.id.is, CaseSource(c)) {
    override def getDataAlphabet: Alphabet = tagsAlphabet

    override def getTargetAlphabet: Alphabet = outAlphabet
  }

  /**
   * maps case list to mallet InstanceList
   * @param cases
   * @return
   */
  def toTrainInstancesList(cases: Seq[Case]): InstanceList = {
    val il = new InstanceList(tagsAlphabet, outAlphabet)

    cases.foreach {
      c =>
        val v = CaseInstance(c)
        il.add(v)
    }
    il
  }

  /**
   * adding disorders to instances list
   * @param li
   * @return
   */
  def addDisorders(li: InstanceList): InstanceList = {
    li.foreach {
      is =>
        is.unLock()
        is.getSource match {
          case cs: CaseSource =>
            is.setSource(CaseSource(cs.c, cs.c.diagnosis
              .openOr(throw DataException("adding disorders to case without diagnosis"))
              .disorders.toList))
          case _ => false
        }
    }
    li
  }

  /**
   * mark instances list for given disorder (set target in instance)
   * @param disorder
   * @param li
   * @return
   */
  def markAllForDisorder(disorder: Disorder, li: InstanceList): InstanceList = {
    li.foreach(is => markSingleForDisorder(disorder, is))
    li
  }

  def markSingleForDisorder(disorder: Disorder, is: Instance): Instance = {
    is.unLock()
    is.setTarget(outAlphabet.lookupLabel(is.getSource match {
      case cs: CaseSource =>
        cs.disorders.contains(disorder)
      case _ => false
    }))
    is
  }


  /**
   * prepare data for single disorder
   * @return
   */
  def allDataTrainSet =
    MalletManager.addDisorders(MalletManager.toTrainInstancesList(
      Case.findAll.filterNot(_.diagnosis.isEmpty)
    ))


  /**
   * describe result of classier training
   * @param name
   * @param total
   * @param yy
   * @param yn
   * @param ny
   * @param nn
   * @param train
   * @param time in millis
   */
  case class ClassifierResult(name: String, total: Int, yy: Double, yn: Double, ny: Double, nn: Double, train: Double, time: Double) {

    /**
     * talking average from results in data
     * @param name
     * @param total
     * @param data
     */
    def this(name: String, total: Int, data: List[ClassifierResult]) = {
      this(name, total,
        data.foldLeft(0.0)(_ + _.yy) / data.size,
        data.foldLeft(0.0)(_ + _.yn) / data.size,
        data.foldLeft(0.0)(_ + _.ny) / data.size,
        data.foldLeft(0.0)(_ + _.nn) / data.size,
        data.foldLeft(0.0)(_ + _.train) / data.size,
        data.foldLeft(0.0)(_ + _.time) / data.size
      )
    }

    /**
     * count for manual classification
     * @param name
     * @param time
     * @param train
     * @param results
     */
    def this(name: String, time: Double, train: Double, results: List[(Boolean, Boolean)]) = {
      this(name, results.length,
        results.count(_ == (true -> true)) / results.size.toDouble,
        results.count(_ == (true -> false)) / results.size.toDouble,
        results.count(_ == (false -> true)) / results.size.toDouble,
        results.count(_ == (false -> false)) / results.size.toDouble,
        train,
        time
      )
    }

    def positive = yy + nn

    def negative = yn + ny

    override def toString = "%s positive %s (yy:%s nn:%s) negative %s (yn:%s ny:%s) train: %s in %s ms)".format(name, positive, yy, nn, negative, yn, ny, train, time)
  }

  //how much times perform spliting
  val times = 10

  def hasIt(instance: Instance, classifier: Classifier): Boolean = {
    classifier.classify(instance).getLabeling.getBestLabel.getEntry.asInstanceOf[Boolean]
  }

  def checkClassifier(trainerName: String, data: InstanceList): ClassifierResult = {

    var time = System.nanoTime()
    val trainer = Classifiers.classifiersMap.get(trainerName).getOrElse(throw BadClassifier(trainerName))

    val trainRatio = 0.6
    val trainSize = (data.size * trainRatio).toInt

    new ClassifierResult(trainerName,
      trainSize,
      (1 to times).toList.map {
        prop =>
          data.split(Array(trainRatio, 1 - trainRatio)).toList match {
            case trainData :: testData :: Nil =>



              val classifier = trainer().train(trainData)
              time = System.nanoTime() - time;

              new ClassifierResult(
                name = trainerName,
                time = time.toDouble / (1000.0 * 1000.0),
                train = classifier.getAccuracy(trainData),
                results = trainData.toList.map(inst =>
                  (inst.getTarget.asInstanceOf[Label].getEntry.asInstanceOf[Boolean],
                    classifier.classify(inst).getLabeling.getBestLabel.getEntry.asInstanceOf[Boolean]))
              )
            case _ =>
              throw new RuntimeException("bad split!")
          }
      })
  }
}