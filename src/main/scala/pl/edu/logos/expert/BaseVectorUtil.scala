package pl.edu.logos.expert

import pl.edu.logos.model.Case
import pl.edu.logos.util.CommonTestUtil._


/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 13.12.12
 * Time: 21:30
 * To change this template use File | Settings | File Templates.
 */
object BaseVectorUtil {

  def tagsMap(c: Case): Map[Long, Double] = {
    Case.positiveTags(c.id.is)
      .groupBy(_._1).map(el => el._1 -> (el._2.filterNot(_._2).size * 1.0 / el._2.size))
  }


}
