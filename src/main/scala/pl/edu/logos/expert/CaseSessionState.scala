package pl.edu.logos.expert

import pl.edu.logos.model._
import net.liftweb.common.{Box, Full}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/26/12
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * class representing current session state
 * it is immutable class so all method creates new instance
 * @param currentCase
 * @param currentProbeType list of tupes (probeType, probeNodeId, and ProbeResult) representing list of current probes
 * @param viewedResources  resources that are viewed
 * @param resources resources to for this probe list, head means current resource
 */
case class CaseSessionState(currentCase: Case, currentProbeType: Seq[(ProbeType, ProbeResult)])
                           (viewedResources: List[(ProbeType, ProbeResource)] = Nil,
                            resources: List[(ProbeType, ProbeResource)] = currentProbeType.flatMap(el => ProbeResource.resourceForProbe(el._1).map(el._1 -> _)).toList) {


  /**
   * Answer probe with result
   * @param value
   * @param probeType
   * @return  new session state
   */

  def answer(value: ProbeResult, probeType: ProbeType): CaseSessionState = {

    CaseSessionState(currentCase, currentProbeType.map(el => if (el._1 == probeType) (probeType, value) else el))()
  }

  /**
   * save all selected answers and creates new probe list
   * @return  new session state
   */
  def nextStep(): CaseSessionState = {
    currentProbeType.foreach(el => currentCase.addProbe(el._1, el._2))

    CaseSessionState(currentCase, ProbeTypesListManager.getCurrentList(currentCase.reload))()
  }

  /**
   * add probe to current session list
   * @param newType
   * @return
   */
  def addType(newType: ProbeType): CaseSessionState = {

    CaseSessionState(currentCase, currentProbeType :+ (newType -> ProbeResult.InNorm))(viewedResources,
      ProbeResource.resourceForProbe(newType).map(el => ((newType -> el) :: resources.reverse).reverse).getOrElse(resources))
  }

  /**
   * create diagnosis for this session stateF
   * @return
   */
  def result = DiagnosisCreator.createDiagnose(currentCase.reload)


  /**
   * skip probe - removes its form list
   * @param probeType
   * @return
   */
  def skip(probeType: ProbeType): CaseSessionState = {
    CaseSessionState(currentCase, currentProbeType.filter(_._1 != probeType))(viewedResources, resources)
  }

  /**
   * mark this resource as viewed (move to viewved list) and sets next resource as current
   * (next resource means resource attached to next probe on list)
   * @return   new session state
   */
  def nextProbeResource: CaseSessionState = {

    resources match {
      case head :: tail if tail == Nil =>
        CaseSessionState(currentCase, currentProbeType)(Nil, (head :: viewedResources).reverse)
      case head :: tail =>
        CaseSessionState(currentCase, currentProbeType)(head :: viewedResources, tail)
      case Nil =>
        this
    }
  }

  /**
   * refresh resource for current probe (choose another random resource)
   * @return
   */
  def refreshProbe: CaseSessionState = {
    resources match {
      case head :: tail =>
        ProbeResource.resourceForProbe(head._1) match {
          case Full(pr) => CaseSessionState(currentCase, currentProbeType)(viewedResources, (head._1, pr) :: tail)
          case _ => this
        }
      case _ => this
    }
  }

  /**
   * check it there is more resources
   * @return
   */
  def hasNext = viewedResources.size + resources.size > 1


  /**
   * @return current resource
   */
  def currentResource: Box[ProbeResource] = Box(resources.headOption.map(_._2))


  /**
   * gets history of case for this sessionState
   * @return
   */
  def history = {
    currentCase.probes.refresh
    currentCase.probes.toList
  }

}

object CaseSessionState {


  /**
   * create
   * @param caseItem
   * @return
   */
  def createForCase(caseItem: Case) = {
    if (caseItem.diagnosis.isEmpty) {
      CaseSessionState(currentCase = caseItem,
        currentProbeType = ProbeTypesListManager.getCurrentList(caseItem))()
    }else{
      CaseSessionState(currentCase = caseItem,
        currentProbeType = ProbeTypesListManager.getListForEmptyProbes )()
    }
  }
}