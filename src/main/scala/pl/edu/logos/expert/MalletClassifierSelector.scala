package pl.edu.logos.expert

import cc.mallet.classify._
import pl.edu.logos.model.{Diagnosis, SieveProps, Case, Disorder}
import cc.mallet.types.InstanceList
import java.io._
import net.liftweb.common.Loggable
import pl.edu.logos.expert.MalletManager.ClassifierResult
import net.liftweb.actor.LiftActor
import net.liftweb.http.SessionVar
import net.liftweb.common._
import net.liftweb.util.Helpers

/**
 * Created with IntelliJ IDEA.
 * User: mikolaj
 * Date: 1/2/13
 * Time: 8:31 PM
 */
object MalletClassifierSelector extends Loggable {

  new File(SieveProps.ClassifiersFilesDir.is).mkdirs()

  def classifierFile(disorder: Disorder): File = {
    new File(SieveProps.ClassifiersFilesDir.is + "/classifier-%d.obj".format(disorder.id.is))
  }

  def selectBestPerformingClassifier(disorder: Disorder, baseData: InstanceList): (List[ClassifierResult], ClassifierResult) = {

    val iList = MalletManager.markAllForDisorder(disorder, baseData)

    val output = Classifiers.names.map(
      name =>
        MalletManager.checkClassifier(name, iList)
    )
    val best = output.maxBy(_.positive)
    (output, best)
  }


  def saveClassifier(disorder: Disorder, classifier: Classifier) = {
    val oos = new ObjectOutputStream(new FileOutputStream(classifierFile(disorder)))
    oos.writeObject(classifier)
    oos.close()
  }

  def loadClassifier(disorder: Disorder): Box[Classifier] = {
    Helpers.tryo {
      val ois = new ObjectInputStream(new FileInputStream(classifierFile(disorder)))
      val classifier = ois.readObject().asInstanceOf[Classifier]
      ois.close()
      classifier
    }
  }

  def loadClassifiers() = {
    Map(Disorder.findAll().map(dis => dis -> loadClassifier(dis)).filterNot(_._2.isEmpty).map(el => el._1 -> el._2.openTheBox): _*)
  }


  def forAllDisorder(baseSet: InstanceList = MalletManager.allDataTrainSet) = {
    Disorder.findAll().map {
      disorder => disorder -> selectBestPerformingClassifier(disorder, baseSet)
    }
  }

  type Result = List[(Disorder, (List[ClassifierResult], ClassifierResult))]


  object TrainerActor extends LiftActor {

    private var _currentRes: Box[Result] = Empty

    private var _busy = false

    private var _currentClassifier = Map[Disorder, Classifier]()

    def busy = _busy

    def currentRes = _currentRes

    def currentClassifiers = {
      if (_currentClassifier.isEmpty) {
        _currentClassifier = loadClassifiers()
        logger.info("classifier load: " + _currentClassifier.mkString("\n"))
      }
      _currentClassifier
    }

    override protected def messageHandler: PartialFunction[Any, Unit] = {
      try {
        _busy = true;
        {
          case TrainALL =>
            _currentRes = Empty
            val allData = MalletManager.allDataTrainSet
            val results = forAllDisorder(allData)
            _currentRes = Full(results)
            _currentClassifier = Map(results.map {
              el => el match {
                case (disorder, (_, best)) =>
                  val trainer = Classifiers.get(best.name)
                  val classifier = trainer().train(MalletManager.markAllForDisorder(disorder, allData))
                  saveClassifier(disorder, classifier)
                  disorder -> classifier
              }
            }: _*)
        }
      }

      finally {
        _busy = false
      }
    }

  }

  object TrainALL

}
