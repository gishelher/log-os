package pl.edu.logos.expert

import cc.mallet.classify._
import pl.edu.logos.util.BadClassifier

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 03.01.13
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */
object Classifiers {


  lazy val classifiersMap: Map[String, () => ClassifierTrainer[_ <: Classifier]] = Map(
    List[() => ClassifierTrainer[_ <: Classifier]](
      () => new NaiveBayesTrainer,
      () => new DecisionTreeTrainer,
      () => new BalancedWinnowTrainer,
      () => new C45Trainer,
      () => new MCMaxEntTrainer,
      () => new NaiveBayesEMTrainer,
      () => new AdaBoostTrainer(new NaiveBayesTrainer),
      () => new AdaBoostM2Trainer(new NaiveBayesTrainer),
      () => new WinnowTrainer
    ).map(
      tr =>
        (tr().getClass.getSimpleName.replace("Trainer", "") -> tr)
    ): _*)

  def get(name: String) = classifiersMap.get(name).getOrElse(throw BadClassifier(name))

  def names = classifiersMap.keys.toList

}
