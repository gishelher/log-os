package pl.edu.logos

import model.{Case, SieveProps}
import net.liftweb.http._
import net.liftweb.mapper.By
import util.PDFUtil
import net.liftweb.common.{Box, Full, Empty}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 08.12.12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
object Dispatchers {


  def pdfHeaders(fileName: String) = ("Content-Type" -> "application/pdf") ::
    ("Content-Disposition" -> ("attachment; filename=" + fileName)) :: Nil

  def base(field: Case => String, what: String):Box[LiftResponse] = (S.param("id").map(_.toLong).filter(Case.canView _)
    .flatMap(id => Case.find(By(Case.id, id)))
    .map(c => (field(c) -> "%s-%s.pdf".format(c.childName, what).replaceAll(" ", "-")))
    .filter(_._1 != null)
    .filterNot(_._1.isEmpty)
    .map(el => OutputStreamResponse(PDFUtil.writePdf(el._1) _, pdfHeaders(el._2)))) match {
    case Full(osr) => Full(osr)
    case _ => Full(NoContentResponse())
  }

  val diagnosisPathList = "files" :: "diagnosis" :: Nil

  def diagnosisPath(id: Long) = SieveProps.URL.is + diagnosisPathList.mkString("/", "/", "?id=%s".format(id))

  val caseReviewPathList = "files" :: "caseReview" :: Nil

  def caseReviewPath(id: Long) = SieveProps.URL.is + caseReviewPathList.mkString("/", "/", "?id=%s".format(id))



  def dispatch: LiftRules.DispatchPF = {
    case Req(this.diagnosisPathList, _, GetRequest) =>
      () => base(_.diagnosisText.is, "diagnoza")
    case Req(this.caseReviewPathList, _, GetRequest) =>
      () => base(_.caseReviewText.is, "karta-badnia")
  }
}

