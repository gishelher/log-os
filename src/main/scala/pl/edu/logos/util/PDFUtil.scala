package pl.edu.logos.util

import xml.NodeSeq
import java.io.{OutputStream, InputStream}
import org.xhtmlrenderer.pdf.ITextRenderer
import io.Source
import org.apache.commons.lang.StringEscapeUtils

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 08.12.12
 * Time: 11:48
 * To change this template use File | Settings | File Templates.
 */
object PDFUtil {

  val HTMLhead = """
          <html xmlns="http://www.w3.org/1999/xhtml">
          <head>
         		<style type="text/css">
               @font-face {
                   src: url('/usr/share/fonts/truetype/freefont/FreeSans.ttf');
                   -fs-pdf-font-embed: embed;
                   -fs-pdf-font-encoding: Identity-H;
               }
               @page {
                   size: A4;
               }
               body {
                   font-family: 'FreeSans';
               }
            </style>
          </head><body>"""

  val HTMLend =
    """
      </body></html>
    """

  def generatePDF(in: NodeSeq) {}


  def writePdf(is: InputStream)(out: OutputStream) {
    val renderer = new ITextRenderer();
    renderer.setDocumentFromString(Source.fromInputStream(is).mkString);
    renderer.layout();
    renderer.createPDF(out);
  }

  def writePdf(data: String)(out: OutputStream) {
    StringEscapeUtils.unescapeHtml(data)

    val renderer = new ITextRenderer();
    renderer.setDocumentFromString(HTMLhead + StringEscapeUtils.unescapeHtml(data) + HTMLend);
    renderer.layout();
    renderer.createPDF(out);
  }

}
