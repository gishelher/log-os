package pl.edu.logos.util

/**
 * Created with IntelliJ IDEA.
 * User: jar
 * Date: 18.08.12
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
class SieveException(message: String, th: Throwable = null) extends RuntimeException(message, th) {

}


case class TemplateException(path: List[String]) extends SieveException("template %s not found.".format(path.mkString("/")))

case class PropError(prop: String) extends SieveException("properties %s not found.".format(prop))

case class SessionFlowException(msg: String) extends SieveException("sessionf flow exception: %s".format(msg))

case class DataException(msg: String) extends SieveException("data exception: %s".format(msg))

case class PipeStateException(msg: String) extends SieveException("pipe state exception: %s".format(msg))

case class BadClassifier(name: String) extends SieveException("no such classifier: %s".format(name))

