package pl.edu.logos.util

import net.liftweb.mapper._

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/6/12
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
trait Reloadable[T <: LongKeyedMapper[T]] {
  self: T =>

  def unary_~ : T = {
    getSingleton.find(
      By(primaryKeyField, primaryKeyField.is))
      .getOrElse(throw DataException("cannot relaod entity with PK: " + primaryKeyField.is))
  }

}
