package pl.edu.logos.util

import net.liftweb.mapper._

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/23/12
 * Time: 12:10 PM
 * To change this template use File | Settings | File Templates.
 */
trait KeepDeleted[A <: KeyedMapper[_, A]] {
  self: A =>

  object deleted extends MappedBoolean[A](self) {
    override def defaultValue = false
  }

  def delete = deleted(true).saveMe()

  def restore = deleted(false).saveMe()
}


trait KeepDeletedMeta[A <: LongKeyedMapper[A] with KeepDeleted[A]] extends LongKeyedMetaMapper[A] {

  self: A =>

  def del(del: Boolean) = By(deleted, del)


  def notDeleted(by: QueryParam[A]*) = super.findAll((by :+ del(false)): _*)

  def allDeleted(by: QueryParam[A]*) = super.findAll((by :+ del(true)): _*)


}
