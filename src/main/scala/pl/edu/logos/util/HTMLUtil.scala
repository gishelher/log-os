package pl.edu.logos.util

import net.liftweb.http.{NodeSeqFuncOrSeqNodeSeqFunc, IdMemoizeTransform, Templates, SHtml}
import net.liftweb.common._
import net.liftweb.http.js.{JsCmds, JsCmd}
import xml.{NodeSeq, Text}
import net.liftweb.http.js.JE.{JsRaw, Str}
import net.liftweb.http.js.jquery.JqJsCmds
import net.liftweb.mapper.{MappedField, MappedEnum}
import net.liftweb.http.SHtml.ElemAttr
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime
import java.lang.IllegalArgumentException
import net.liftweb.http.js.JsCmds.{Function, Script}
import java.util.Date
import net.liftweb.json.{JsonAST, JsonParser}
import net.liftweb.json.JsonAST.JObject


/**
 * Created with IntelliJ IDEA.
 * User: jar
 * Date: 16.08.12
 * Time: 21:28
 * To change this template use File | Settings | File Templates.
 */

object HTMLUtil extends Loggable {

  def ajaxCall(cmd: => JsCmd): JsCmd = SHtml.ajaxCall(Str("ignore"), {
    ignore: String => cmd
  })._2.cmd


  def ajaxFunction(name: String, paramNames: String*)(func: Map[String, String] => JsCmd) = {
    Script(Function(name, paramNames.toList, JsCmds.Run(
      "var out = JSON.stringify(%s);".format(paramNames.map(name => "%s: %s".format(name, name)).mkString("{", ",", "}"))
    ) & SHtml.ajaxCall(JsRaw("out"), data => {
      JsonParser.parse(data) match {
        case jObject: JObject =>
          func(Map(jObject.obj.map(field => field.value match {
            case str: JsonAST.JString =>
              (field.name, str.s)
            case any =>
              logger.error("bad json value! %s".format(data))
              ("", "")
          }): _*))
        case any =>
          logger.error("bad json value! %s".format(data))
          func(Map())
      }
    })._2.cmd))
  }

  def toPercent(d: Double): String = {
    ("%2.2f").format(d * 100) + "%"
  }


  def uniqueIdMemoize(f: IdMemoizeTransform => NodeSeqFuncOrSeqNodeSeqFunc): NodeSeqFuncOrSeqNodeSeqFunc = {
    val outer = SHtml.idMemoize(f)
    val id = System.nanoTime().toString

    in: NodeSeq => outer(<div id={id}>
      {in}
    </div>)
  }

  def showStringMessage(what: String, where: String = "message-panel", level: MessageLevel.Value = MessageLevel.ERROR): JsCmd = {
    //TODO 1            dialo
    showMessage(Text(what), where, level)
  }

  def showMessage(what: NodeSeq, where: String = "message-panel", level: MessageLevel.Value = MessageLevel.ERROR): JsCmd = {
    //TODO 1
    JqJsCmds.DisplayMessage(where, what, 5 seconds, 2.seconds)
  }


  def enumSelect[E <: Enumeration](field: MappedEnum[_, E], attrs: ElemAttr*) = SHtml.ajaxSelect(
    field.enum.values.toList.map(v => (v.toString, v.id.toString)), Full(field.is.id.toString),
    str => {
      field(field.enum(str.toInt))
      JsCmds.Noop
    }, attrs: _*)

  def template(path: List[String]): NodeSeq = Templates(path).openOr(throw TemplateException(path))


  case class ToggleItem(id: String, binding: CssSel) {
    var state: Boolean = false

    def bind: CssSel =
      "#%s".format(id) #> SHtml.idMemoize {
        context =>
          if (state) {
            "#toggle-deleted-img [onclick]" #> HTMLUtil.ajaxCall {
              state = false;
              context.setHtml()
            } & binding
          } else {
            "#toggle-deleted-img [onclick]" #> HTMLUtil.ajaxCall {
              state = true;
              context.setHtml()
            } & "#toggleBody" #> NodeSeq.Empty
          }
      }
  }

  def showDialog(title: String, in: NodeSeq): JsCmd =
    JsCmds.Run(
      """try{
        |   $('#modal').dialog('option', 'title', '%s').dialog('show');
        | } catch(e){
        |   $('#modal').dialog({draggable: true, modal: true, width: '450px', title: '%s'});
        | }; """.stripMargin.format(title, title)) & JsCmds.SetHtml("modal", in) & JsCmds.Run("$('.ui-dialog').css('top', '40px');")


  def closeDialog(): JsCmd = JsCmds.Run("$('#modal').dialog('close');") & JsCmds.SetHtml("modal", NodeSeq.Empty)


  val dateFormat = DateTimeFormat.forPattern("d-M-Y")


  def ajaxDate[T <: net.liftweb.mapper.Mapper[T]](dateField: MappedField[Date, T], checkAndSet: Date => JsCmd): NodeSeq = {
    val id = System.nanoTime().toString
      <div id={id + "msg"}/> ++
      SHtml.ajaxText(HTMLUtil.dateFormat.print(new DateTime(dateField.is)), newDate =>
        try {
          checkAndSet(HTMLUtil.dateFormat.parseDateTime(newDate).toDate)
        } catch {
          case e: IllegalArgumentException =>
            JqJsCmds.DisplayMessage(id + "msg", Text("Zły format daty poprawny to: " + HTMLUtil.dateFormat.toString), 5 seconds, 2 seconds)
        }, "id" -> id
      ) ++ Script(JsCmds.Run("$('#%s').datepicker({ dateFormat: 'dd-mm-yy', onSelect: function(){$(this)[0].onblur();}});".format(id)))
  }

}

object MessageLevel extends Enumeration {

  val ERROR = Value(1, "level.error")
  val INFO = Value(2, "level.info")

}
