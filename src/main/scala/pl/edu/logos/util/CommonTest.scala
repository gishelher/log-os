package pl.edu.logos.util

import net.liftweb.db.{DefaultConnectionIdentifier, DB}
import org.specs2.mutable.{Specification, SpecificationWithJUnit}
import org.specs2.execute.Result

class CommonTest extends Specification {


  def rolledBackTransaction[T <% Result](f: => T): T = {

    DB.defineConnectionManager(DefaultConnectionIdentifier, PostgreSQLDBVendor)

    DB.use(DefaultConnectionIdentifier) {
      conn =>
        conn.setAutoCommit(false)
        try {
          f
        } finally {
          conn.rollback()
        }
    }
  }

}

object CommonTestUtil {
  def mesure[T](func: => T, what: String = "-", times: Int = 1): T = {
    val time = System.nanoTime()
    val res = func
    println("Mesuring %s with time: %s sek.".format(what, ((System.nanoTime() - time) * times / 1000) / 1000000.0))
    res
  }
}

