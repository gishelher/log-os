package pl.edu.logos.util


import net.liftweb._
import common.{Loggable, Full}
import mapper.{ConnectionManager, ConnectionIdentifier}
import java.sql.{SQLException, DriverManager, Connection}
import pl.edu.logos.model.SieveProps

object PostgreSQLDBVendor extends ConnectionManager with Loggable {

  // Force load the driver
  Class.forName(SieveProps.DbDriver.is)

  def newConnection(name: ConnectionIdentifier) = {
    val url = SieveProps.DbUrl.is
    val user = SieveProps.DbUser.is
    val password = SieveProps.DbPass.is
    try {
      Full(DriverManager.getConnection(url, user, password))
    } catch {
      case e: SQLException => {
        val msg = "Exception while connecting to DB; url: %s, user: %s, password: %s".format(url, user, password)
        logger.error(msg, e)
        throw new SieveException(msg, e)
      }
    }
  }

  def releaseConnection(conn: Connection) {
    conn.close()
  }
}