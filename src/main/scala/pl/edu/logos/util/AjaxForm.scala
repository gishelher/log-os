package pl.edu.logos.util

import xml.{Text, NodeSeq}
import net.liftweb.http.{SHtml, S}
import net.liftweb.http.js.{JsCmd, JsCmds}
import net.liftweb.util.Helpers._
import net.liftweb.util.FieldError
import net.liftweb.mapper._
import net.liftweb.common.Full
import org.joda.time.DateTime

/**
 * Created with IntelliJ IDEA.
 * User: jar
 * Date: 18.08.12
 * Time: 10:36
 * To change this template use File | Settings | File Templates.
 */
class FullAjaxForm[T <: Mapper[T]](obj: T, viewable: Seq[MappedField[_, T]], editable: Seq[MappedField[_, T]]) {

  var edit = false;

  def submitFunc(onSuccess: () => JsCmd): JsCmd = {
    editable.flatMap(_.validate).toList match {
      case Nil => obj.validate match {
        case Nil =>
          edit = false;
          onSuccess()
        case list =>
          displayMessages(list)
      }
      case list =>
        displayMessages(list)
    }


  }

  private def preBind(fieldName: String) =
    ".field [id]" #> fieldName &
      ".value [id]" #> "%s-value".format(fieldName) &
      ".label [id]" #> "%s-label".format(fieldName) &
      ".errors [id]" #> "%s-errors".format(fieldName) &
      ".label *" #> S.?(obj.getClass.getSimpleName + "." + fieldName)


  def bindEdit(field: MappedField[_, T]) = field match {

    case mappable: AjaxHtmlMappable =>
      mappable.editHtml

    case passField: MappedPassword[T] =>
      SHtml.ajaxText(passField.get, checkAndSet(passField) _,
        "id" -> "%s-input".format(field.name), "type" -> "password")


    case strField: MappedString[T] =>
      SHtml.ajaxText(strField.get, checkAndSet(strField) _)

    case enumField: MappedEnum[T, _] =>
      SHtml.ajaxSelect(enumField.enum.values.map(v => v.id.toString -> v.toString).toSeq, Full(enumField.is.id.toString),
        newVal => checkAndSet(enumField)(enumField.enum(newVal.toInt)))

    case booleanField: MappedBoolean[T] =>
      SHtml.ajaxCheckbox(booleanField.is, newVal => checkAndSet(booleanField)(newVal))

    case dateField: MappedDate[T] => HTMLUtil.ajaxDate(dateField, checkAndSet(dateField) _)

    case dateField: MappedDateTime[T] => HTMLUtil.ajaxDate(dateField, checkAndSet(dateField) _)

    case intField: MappedInt[T] => SHtml.ajaxText(intField.get.toString, txt => try {
      checkAndSet(intField)(txt.toInt)
    } catch {
      case e: NumberFormatException => displayMessages(FieldError(intField, Text("Zły format liczby")) :: Nil)
    })


    case findable: MappedFindable[T, _] =>
      FindForm(findable.findableMeta).view(S.?(obj.getClass.getSimpleName + "." + fieldId(findable)), findable.obj, item => {
        (item match {
          case Full(item) =>
            item.saveMe()
            checkAndSet(findable)(item.id.is)
          case _ =>
            JsCmds.Noop
        })
      })


    case _ =>
      <div>bad value</div>

  }


  def displayMessages(messages: List[FieldError]): JsCmd = {
    messages.map(msg => HTMLUtil.showMessage(msg.msg, "%s-errors".format(msg.field.uniqueFieldId.openOr("def"))))
      .foldLeft(JsCmds.Noop)(_ & _)
  }

  def fieldId(field: MappedField[_, T]) = field.uniqueFieldId.openOr("%s_%s".format(field.fieldOwner.dbName, field.name))

  private def checkAndSet[V](f: MappedField[V, T])(value: V): JsCmd = {
    f(value)
    f.validate match {
      case Nil => JsCmds.Noop
      case list => displayMessages(list)
    }
  }

  def bindFieldValue(field: MappedField[_, T]) = field match {

    case findable: MappedFindable[T, _] =>
      findable.view
    case dateField: MappedDate[T] => HTMLUtil.dateFormat.print(new DateTime(dateField.is))
    case dateField: MappedDateTime[T] => HTMLUtil.dateFormat.print(new DateTime(dateField.is))
    case field => field.is match {
      case null => "null"
      case obj => obj.toString
    }
  }

  val uniqueId = System.nanoTime().toString


  def view(onSave: => JsCmd): NodeSeq = {
    ("#ajax-form-parent" #> HTMLUtil.uniqueIdMemoize {
      context =>
        val (toEdit, toView) = if (edit) (editable, viewable) else (Nil, viewable ++ editable)

        ".editable" #> toEdit.map {
          field =>
            preBind(fieldId(field)) &
              ".value *" #> bindEdit(field)
        } &
          ".viewable" #> toView.map {
            field =>
              preBind(fieldId(field)) &
                ".value *" #> bindFieldValue(field)
          } &
          (if (edit)
            "#submitForm * " #> "Zapisz" &
              "#cancel [onclick]" #> HTMLUtil.ajaxCall {
                edit = false;
                context.setHtml()
              } &
              "#submitForm [onclick]" #> (HTMLUtil.ajaxCall {
                submitFunc(() => onSave & context.setHtml())
              })
          else
            "#submitForm * " #> "Edytuj" &
              "#cancel" #> NodeSeq.Empty &
              "#submitForm [onclick]" #> HTMLUtil.ajaxCall {
                edit = true;
                context.setHtml()
              })
    }
      )(HTMLUtil.template("util" :: "ajaxForm" :: Nil))
  }

}

case class AjaxForm[T <: Mapper[T]](obj: T, fields: MappedField[_, T]*) extends FullAjaxForm[T](obj, Nil, fields) {

}
