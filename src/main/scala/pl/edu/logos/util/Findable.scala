package pl.edu.logos.util

import net.liftweb.mapper._
import net.liftweb.http.js.{JsCmds, JsCmd}
import xml.{Text, NodeSeq}
import net.liftweb.http.SHtml
import net.liftweb.common.{Empty, Box}
import net.liftweb.util.Helpers._
import net.liftweb.common.Full

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/7/12
 * Time: 10:09 AM
 * To change this template use File | Settings | File Templates.
 */

trait FindableMeta[T <: LongKeyedMapper[T]] {

  self: LongKeyedMetaMapper[T] =>

  def searchResult(txt: String): Seq[T]

  def newFields(item: T): List[MappedField[_, T]]

  def baseLine(item: T): String

  def additionalLine(item: T): String

  def searchOnField(field: MappedString[T])(txt: String) = findAll(Like(field, "%" + txt + "%"))


}


object SearchModal {
  def modal[T <: LongKeyedMapper[T]](singleton: MetaMapper[T] with FindableMeta[T],
                                     onSuccess: Box[T] => JsCmd,
                                     findOpt: Box[String => Seq[T]] = Empty): NodeSeq = {
    var text = ""
    var newForm = false
    val newItem = singleton.create
    val form = new AjaxForm(newItem, singleton.newFields(newItem): _*) {
      edit = true
    }

    ("#searchPanel" #> HTMLUtil.uniqueIdMemoize {
      context =>
        "#searchText" #> SHtml.ajaxText(text, txt => {
          text = txt;
          context.setHtml()
        }) &
          (if (newForm)
            "#new-form" #> form.view {
              form.obj.saveMe
              onSuccess(Full(form.obj))
            } &
              "#item" #> NodeSeq.Empty &
              "#newObjButton *" #> "Wyszukaj" &
              "#newObjButton [onclick]" #> HTMLUtil.ajaxCall {
                newForm = false;
                context.setHtml()
              }
          else
            "#new-form" #> NodeSeq.Empty &
              "#item" #> findOpt.getOrElse(singleton.searchResult _)(text).take(10).map {
                item =>
                  "#baseLine *" #> singleton.baseLine(item) &
                    "#additionalLine *" #> singleton.additionalLine(item) &
                    "#item [onclick]" #> HTMLUtil.ajaxCall(onSuccess(Full(item)))
              } &
              "#newObjButton *" #> "Dodaj" &
              "#newObjButton [onclick]" #> HTMLUtil.ajaxCall {
                newForm = true;
                context.setHtml()
              }) &
          "#cancelButton [onclick]" #> HTMLUtil.ajaxCall(onSuccess(Empty))
    })(HTMLUtil.template("util" :: "findForm" :: Nil))
  }
}


case class FindForm[T <: LongKeyedMapper[T]](singleton: MetaMapper[T] with FindableMeta[T]) {

  def view(title: String, init: Box[T], onSuccess: Box[T] => JsCmd): NodeSeq = {
    val id = System.nanoTime().toString
    ("#selected *" #> init.map(singleton.baseLine _).getOrElse("brak") &
      "#selected [id]" #> id &
      "#change [onclick]" #> HTMLUtil.ajaxCall(HTMLUtil.showDialog(title, SearchModal.modal[T](singleton, item => {
        onSuccess(item) &
          HTMLUtil.closeDialog() &
          JsCmds.SetHtml(id, Text(item.map(singleton.baseLine _).getOrElse("brak")))
      })))
      )(<span>
      <span id="selected"></span>
      <a href="javascript://" id="change"> Zamień</a>
    </span>
    )
  }
}

abstract class MappedFindable[T
<: Mapper[T], O
<: LongKeyedMapper[O] with IdPK](owner: T, val findableMeta: LongKeyedMetaMapper[O] with FindableMeta[O])
  extends MappedLongForeignKey(owner, findableMeta) {
  def view = obj.map(findableMeta.baseLine _).getOrElse("brak")
}