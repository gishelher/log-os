package pl.edu.logos.util

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 8/27/12
 * Time: 3:49 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.mapper.{KeyedMapper, Mapper, Schemifier, MetaMapper}

/**
 * trait responsible for shemified classes and future stuff
 */

trait SieveMetaMapper[T <: KeyedMapper[_, T]] {
  self: T =>

  if (SieveMetaMapperConfig.development) {
    val a = getSingleton
    Schemifier.schemify(true, Schemifier.infoF _, getSingleton)
  }

}

/**
 * for mapping non-keyed mappers
 * @tparam T
 */
trait SieveNonKeyedMetaMapper[T <: Mapper[T]] {
  self: T =>

  def getSingleton: MetaMapper[T]

  if (SieveMetaMapperConfig.development) {
    Schemifier.schemify(true, Schemifier.infoF _, getSingleton)
  }

}


object SieveMetaMapperConfig {

  /**
   * for DI
   * @return idicates if  applications runs in development mode
   */
  def development: Boolean = true


}