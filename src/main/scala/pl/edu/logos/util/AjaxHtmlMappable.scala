package pl.edu.logos.util

import net.liftweb.mapper.MappedField
import xml.NodeSeq

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
trait AjaxHtmlMappable {


  def editHtml: NodeSeq

  def viewHtml: NodeSeq
}
