package pl.edu.logos.model


import net.liftweb.http.S
import net.liftweb.mapper._
import net.liftweb.sitemap.Loc.{If, LocParam}
import pl.edu.logos.util.{SieveMetaMapper, SieveException}
import net.liftweb.sitemap.Loc._
import net.liftweb.common.Box
import net.liftweb.sitemap.Loc.If
import pl.edu.logos.model.enums._

/**
 * An O-R mapped "User" class that includes first name, last name, password and we add a "Personal Essay" to it
 */
class User extends MegaProtoUser[User] {
  def meta = User


  object title extends MappedEnum(this, JobTitle)

  def getSingleton = User


  def myEditFields = firstName :: lastName :: email :: title :: Nil

  def displayName = if (firstName.isEmpty || lastName.isEmpty)
    email.is
  else
    "%s %s %s".format(title.is, firstName.is, lastName.is)


  def openSession: List[Session] =
    Session.notDeleted(By(Session.owner, this.id), By(Session.open, true))

  def closedSession: List[Session] =
    Session.notDeleted(By(Session.owner, this.id), By(Session.open, false))

  def deletedSession: List[Session] =
    Session.allDeleted(By(Session.owner, this.id))

  def getSession(id: Long): Box[Session] = Session.find(By(Session.id, id), By(Session.owner, this.id))

}

/**
 * The singleton that has methods for accessing the database
 */
object User extends User with MetaMegaProtoUser[User] with SieveMetaMapper[User] {

  override def skipEmailValidation = true


  //for mapping to our classes
  override protected def changePasswordMenuLocParams: List[LocParam[Unit]] = testLogginIn :: Nil

  override protected def editUserMenuLocParams: List[LocParam[Unit]] = testLogginIn :: Nil

  override protected def loginMenuLocParams: List[LocParam[Unit]] =
    If(notLoggedIn_? _, S.??("already.logged.in")) :: Nil

  override protected def createUserMenuLocParams: List[LocParam[Unit]] =
    If(notLoggedIn_? _, S.??("logout.first")) :: Nil


  override def basePath = "user" :: Nil


  override def signUpSuffix = "sing-up"


  override def logoutCurrentUser {
    super.logoutCurrentUser
  }

  def signUpUrl = signUpPath.mkString("/", "/", "")

  def logOutUrl = logoutPath.mkString("/", "/", "")

  def editUrl = editPath.mkString("/", "/", "")

  def findByEmail(email: String): Box[User] = find(By(User.email, email))

  def loggedUser: User = currentUser.openOr(throw new SieveException("no logged user!"))

}
