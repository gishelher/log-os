package pl.edu.logos.model

import net.liftweb.mapper._
import pl.edu.logos.util.MappedFindable
import java.io.File
import net.liftweb.common.{Full, Empty, Box}
import util.Random
import pl.edu.logos.Urls

/**
 * Created with IntelliJ IDEA.
 * User: mikolaj
 * Date: 10/16/12
 * Time: 9:58 PM
 * To change this template use File | Settings | File Templates.
 */
class ProbeResource extends LongKeyedMapper[ProbeResource] with IdPK with BaseSieveEntity[ProbeResource] {

  def getSingleton = ProbeResource

  object probeType extends MappedFindable(this, ProbeType)

  object path extends MappedString(this, 128)

  object name extends MappedString(this, 128)

  object label extends MappedString(this, 128)

  override def delete = {
    new File("src/main/webapp/" + path).delete()
    super.delete
  }

  def url = Urls.imagePath(path.is)

}

object ProbeResource extends ProbeResource with LongKeyedMetaMapper[ProbeResource] {


  def resourceForProbe(probeType: ProbeType): Box[ProbeResource] = {

    ProbeResource.findAll(By(ProbeResource.probeType, probeType.id.is)) match {
      case Nil => Empty
      case list => Full(list(Random.nextInt(list.size)))
    }

  }

}
