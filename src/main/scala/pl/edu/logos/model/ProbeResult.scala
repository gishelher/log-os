package pl.edu.logos.model

import net.liftweb.mapper._

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/26/12
 * Time: 10:12 PM
 * To change this template use File | Settings | File Templates.
 */
class ProbeResult extends LongKeyedMapper[ProbeResult] with BaseSieveEntity[ProbeResult] with IdPK {

  override def getSingleton = ProbeResult

  object name extends MappedString(this, 124)

  object positive extends MappedBoolean(this)


  object className extends MappedString(this, 32) {
    override def defaultValue = "someColor"
  }

}


object ProbeResult extends ProbeResult with LongKeyedMetaMapper[ProbeResult] {
  def UnderNorm = getOrCreate("poniżej normy")

  def InNorm = getOrCreate("w normie")

  def Yes = getOrCreate("tak")

  def No = getOrCreate("nie")

  def accepted = getOrCreate("zaliczone")

  def notAccepted = getOrCreate("niezaliczone")

  def standrd = UnderNorm :: InNorm :: Nil

  def yesNo = Yes :: No :: Nil

  def acceptedOpt = accepted :: notAccepted :: Nil

  def speechTempoOpt = ("wolne" :: "normalne" :: "szybkie" :: Nil).map(getOrCreate _)


  private def getOrCreate(name: String): ProbeResult =
    find(By(ProbeResult.name, name)).getOrElse(ProbeResult.create.name(name).saveMe())

}