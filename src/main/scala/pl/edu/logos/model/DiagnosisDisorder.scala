package pl.edu.logos.model

import net.liftweb.mapper.{MappedLongForeignKey, LongKeyedMetaMapper, IdPK, LongKeyedMapper}
import pl.edu.logos.util.MappedFindable

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/21/12
 * Time: 6:10 PM
 * To change this template use File | Settings | File Templates.
 */
class DiagnosisDisorder extends LongKeyedMapper[DiagnosisDisorder] with IdPK with BaseSieveEntity[DiagnosisDisorder] {

  def getSingleton = DiagnosisDisorder

  object diagnosis extends MappedLongForeignKey(this, Diagnosis)

  object disorder extends MappedFindable(this, Disorder)


}

object DiagnosisDisorder extends DiagnosisDisorder with LongKeyedMetaMapper[DiagnosisDisorder] {

}