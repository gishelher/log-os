package pl.edu.logos.model

import net.liftweb.mapper.{MappedLongForeignKey, LongKeyedMetaMapper, IdPK, LongKeyedMapper}
import pl.edu.logos.util.MappedFindable

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/21/12
 * Time: 6:10 PM
 * To change this template use File | Settings | File Templates.
 */
class DiagnosisSuggestion extends LongKeyedMapper[DiagnosisSuggestion] with IdPK with BaseSieveEntity[DiagnosisSuggestion] {

  def getSingleton = DiagnosisSuggestion

  object diagnosis extends MappedLongForeignKey(this, Diagnosis)

  object suggestion extends MappedFindable(this, Suggestion)


}

object DiagnosisSuggestion extends DiagnosisSuggestion with LongKeyedMetaMapper[DiagnosisSuggestion] {

}