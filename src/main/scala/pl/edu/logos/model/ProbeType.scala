package pl.edu.logos.model

import net.liftweb.mapper._
import pl.edu.logos.util.FindableMeta

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 12:12 PM
 * To change this template use File | Settings | File Templates.
 */
class ProbeType extends LongKeyedMapper[ProbeType] with IdPK with BaseSieveEntity[ProbeType] with ManyToMany {
  def getSingleton = ProbeType

  object name extends MappedString(this, 124)

  object possibleAnswers extends MappedManyToMany(ProbeResultProbeType, ProbeResultProbeType.probeType,
    ProbeResultProbeType.probeResult, ProbeResult)

  object tags extends MappedManyToMany(ProbeTypeTag, ProbeTypeTag.probeType, ProbeTypeTag.tag, Tag)

  object initial extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object dependsOn extends MappedLongForeignKey(this, ProbeType)

  object listNumber extends MappedInt(this)

  override def save: Boolean = {
    if (possibleAnswers.isEmpty) {
      ProbeResult.standrd.foreach(possibleAnswers.+= _)
    }
    super.save
  }


  def defaultResult: ProbeResult = ProbeResultProbeType
    .find(By(ProbeResultProbeType.probeType, id.is), By(ProbeResultProbeType.default, true))
    .flatMap(_.probeResult)
    .openOr(ProbeResult.InNorm)

}

object ProbeType extends ProbeType with LongKeyedMetaMapper[ProbeType] with FindableMeta[ProbeType] {
  def searchResult(txt: String) = findAll(Like(ProbeType.name, "%" + txt + "%"))

  def newFields(item: ProbeType) = item.name :: Nil

  def baseLine(item: ProbeType) = item.name.is

  def additionalLine(item: ProbeType) = ""
}
