package pl.edu.logos.model

import net.liftweb.mapper._
import net.liftweb.util.FieldError
import net.liftweb.http.S
import pl.edu.logos.util.{HTMLUtil, FindableMeta}
import org.joda.time.DateTime

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 11:40 AM
 * To change this template use File | Settings | File Templates.
 */
class Child extends LongKeyedMapper[Child] with IdPK with BaseSieveEntity[Child] {


  def getSingleton = Child

  object name extends MappedString(this, 64)

  object bornDate extends MappedDate(this)


  object PESEL extends MappedString(this, 16) {


    def check(value: String): List[FieldError] = {
      if (value.size != 11 || !value.filter(!_.isDigit).isEmpty) {
        FieldError(this, S.?("model.child.bad.pesel")) :: Nil
      } else {
        Nil
      }
    }

 //    override def validations = (check _) :: super.validations
  }

}

object Child extends Child with LongKeyedMetaMapper[Child] with FindableMeta[Child] {
  def searchResult(txt: String) = searchOnField(name)(txt)

  def newFields(child: Child) = child.name :: child.bornDate :: Nil

  def baseLine(item: Child) = item.name.is

  def additionalLine(item: Child) = "Urodzona: " + HTMLUtil.dateFormat.print(new DateTime(item.bornDate.is))
}
