package pl.edu.logos.model

import net.liftweb.mapper._

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 18.11.12
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
class ProbeResultProbeType extends LongKeyedMapper[ProbeResultProbeType] with IdPK with BaseSieveEntity[ProbeResultProbeType] {
  def getSingleton = ProbeResultProbeType

  object probeType extends MappedLongForeignKey(this, ProbeType)

  object probeResult extends MappedLongForeignKey(this, ProbeResult)

  object default extends MappedBoolean(this) {
    override def defaultValue = false
  }

}

object ProbeResultProbeType extends ProbeResultProbeType with LongKeyedMetaMapper[ProbeResultProbeType] {

}
