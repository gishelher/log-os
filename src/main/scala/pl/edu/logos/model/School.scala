package pl.edu.logos.model

import net.liftweb.mapper._
import net.liftweb.util.Schedule
import pl.edu.logos.util.FindableMeta

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */

class School extends LongKeyedMapper[School] with OneToMany[Long, School] with IdPK with BaseSieveEntity[School] {
  def getSingleton = School

  object name extends MappedString(this, 128)


  object sessions extends MappedOneToMany(Session, Session.school)

}

object School extends School with LongKeyedMetaMapper[School] with FindableMeta[School] {
  def searchResult(txt: String): Seq[School] = searchOnField(name)(txt)

  def newFields(item: School): List[MappedField[_, School]] = item.name :: Nil

  def baseLine(item: School): String = item.name.is

  def additionalLine(item: School): String = ""
}
