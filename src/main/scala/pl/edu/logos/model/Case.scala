package pl.edu.logos.model

import net.liftweb.mapper._
import pl.edu.logos.util.{MappedFindable, KeepDeletedMeta}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 11:39 AM
 * To change this template use File | Settings | File Templates.
 */
class Case extends LongKeyedMapper[Case] with IdPK with OneToMany[Long, Case] with BaseSieveEntity[Case] {


  def getSingleton = Case

  object session extends MappedLongForeignKey(this, Session)

  object childField extends MappedFindable(this, Child)

  object probes extends MappedOneToMany(Probe, Probe.myCase)

  object diagnosisText extends MappedString(this, 8192)

  object caseReviewText extends MappedString(this, 8192)


  def diagnosis = Diagnosis.find(By(Diagnosis.myCase, id.is))

  def child: Child = childField.obj.getOrElse(Child.create)

  def childName = childField.obj.map(_.name.is).getOrElse("brak dziecka")

  def setEmptyOkDiagnosis = {
    val diagnosis = Diagnosis.create.myCase(this).saveMe()
    this
  }

  def addEmptyProbe = {
    probes.refresh
    probes :+ Probe.create.myCase(this).number(probes.size).result(ProbeResult.InNorm).saveMe()
    this
  }

  def addProbe(probeType: ProbeType, result: ProbeResult) {
    probes.refresh
    probes.filter(_.myType.is == probeType.id.is).toList match {
      case Nil => probes :+ Probe.create.myType(probeType).result(result).number(probes.size).myCase(this).saveMe()
      saveMe()
      case list =>
        list.foreach(_.result(result).saveMe())
    }
  }
}

object Case extends Case with KeepDeletedMeta[Case] {

  //TODO add security
  def canView(id: Long): Boolean = true


  def positiveTags(id: Long): List[(Long, Boolean)] = {
    DB.use(DefaultConnectionIdentifier) {
      connection => {
        val res = connection.connection.createStatement.executeQuery(
          """
            |SELECT
            |  ptt.tag,
            |  pr.positive_c
            |FROM probe p
            |  JOIN probeResult pr
            |    ON p.result = pr.id
            |  JOIN probeType pt
            |    ON p.myType = pt.id
            |  JOIN ProbeTypeTag ptt
            |    ON ptt.probeType = pt.id
            |WHERE
            |  p.myCase = %s;
          """.stripMargin.format(id))

        var rest = List[(Long, Boolean)]()
        while (res.next()) {
          rest = (res.getLong(1) -> res.getBoolean(2)) :: rest
        }
        rest
      }
    }
  }
}

