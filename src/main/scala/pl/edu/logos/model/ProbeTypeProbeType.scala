package pl.edu.logos.model

import net.liftweb.mapper.{MappedLongForeignKey, LongKeyedMetaMapper, IdPK, LongKeyedMapper}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 26.11.12
 * Time: 07:55
 * To change this template use File | Settings | File Templates.
 */
class ProbeTypeProbeType extends LongKeyedMapper[ProbeTypeProbeType] with IdPK {

  override def getSingleton = ProbeTypeProbeType

  object parentProbe extends MappedLongForeignKey(this, ProbeType)

  object childProbe extends MappedLongForeignKey(this, ProbeType)

}


object ProbeTypeProbeType extends ProbeTypeProbeType with LongKeyedMetaMapper[ProbeTypeProbeType] {

}
