package pl.edu.logos.model

import net.liftweb.mapper.{LongKeyedMapper, MappedLongForeignKey, IdPK, LongKeyedMetaMapper}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/21/12
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */
class DisorderSuggestion extends LongKeyedMapper[DisorderSuggestion] with IdPK with BaseSieveEntity[DisorderSuggestion] {


  def getSingleton = DisorderSuggestion

  object suggestion extends MappedLongForeignKey(this, Suggestion)

  object disorder extends MappedLongForeignKey(this, Disorder)


}

object DisorderSuggestion extends DisorderSuggestion with LongKeyedMetaMapper[DisorderSuggestion] {

}
