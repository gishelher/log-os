package pl.edu.logos.model

import net.liftweb.mapper._
import weka.core.Attribute

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 26.11.12
 * Time: 09:05
 * To change this template use File | Settings | File Templates.
 */
class Tag extends LongKeyedMapper[Tag] with IdPK {

  override def getSingleton = Tag

  object name extends MappedString(this, 128)

  object vectorPos extends MappedInt(this) {
    override def defaultValue = id.is.toInt
  }

}


object Tag extends Tag with LongKeyedMetaMapper[Tag] {

  def vectorLength = findAll(OrderBy(vectorPos, Descending)).head.vectorPos + 1

}