package pl.edu.logos.model

import net.liftweb.mapper._
import pl.edu.logos.util.{FindableMeta, MappedFindable}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */
class Probe extends LongKeyedMapper[Probe] with IdPK with BaseSieveEntity[Probe] with ManyToMany {


  def getSingleton = Probe

  object myCase extends MappedLongForeignKey(this, Case)

  object myType extends MappedFindable(this, ProbeType)

  object result extends MappedLongForeignKey(this, ProbeResult)

  object number extends MappedInt(this)



}

object Probe extends Probe with LongKeyedMetaMapper[Probe] with FindableMeta[Probe] {
  def searchResult(txt: String) = null

  def newFields(item: Probe) = null

  def baseLine(item: Probe) = null

  def additionalLine(item: Probe) = null
}
