package pl.edu.logos.model

import net.liftweb.mapper._

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */
class Diagnosis extends LongKeyedMapper[Diagnosis] with IdPK with ManyToMany with BaseSieveEntity[Diagnosis] {

  def getSingleton = Diagnosis

  object myCase extends MappedLongForeignKey(this, Case)

  object disorders
    extends MappedManyToMany(DiagnosisDisorder,
      DiagnosisDisorder.diagnosis, DiagnosisDisorder.disorder, Disorder)

  object suggestions
    extends MappedManyToMany(DiagnosisSuggestion,
      DiagnosisSuggestion.diagnosis, DiagnosisSuggestion.suggestion, Suggestion)

}

object Diagnosis extends Diagnosis with LongKeyedMetaMapper[Diagnosis] {

}

