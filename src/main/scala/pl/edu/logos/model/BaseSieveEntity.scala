package pl.edu.logos.model

import net.liftweb.mapper.{IdPK, LongKeyedMapper}
import pl.edu.logos.util.{Reloadable, KeepDeleted}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/6/12
 * Time: 1:47 PM
 * To change this template use File | Settings | File Templates.
 */
trait BaseSieveEntity[A <: LongKeyedMapper[A]] extends KeepDeleted[A] with Reloadable[A] {
  self: A =>
}