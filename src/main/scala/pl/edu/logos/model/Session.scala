package pl.edu.logos.model

import net.liftweb.mapper._
import pl.edu.logos.util.{MappedFindable, KeepDeletedMeta}
import net.liftweb.util.Helpers
import java.util.Date
import org.joda.time.DateTime

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 11:27 AM
 * To change this template use File | Settings | File Templates.
 */
class Session extends LongKeyedMapper[Session] with OneToMany[Long, Session] with IdPK with BaseSieveEntity[Session] {
  def getSingleton = Session

  object creationDate extends MappedDate(this)

  object examineDate extends MappedDate(this)

  object isPublicEditable extends MappedBoolean(this)

  object hash extends MappedString(this, 64)

  object school extends MappedFindable(this, School)

  object owner extends MappedLongForeignKey(this, User)

  object cases extends MappedOneToMany(Case, Case.session)

  object open extends MappedBoolean(this) {
    override def defaultValue = true
  }

}

object Session extends Session with KeepDeletedMeta[Session] {

  override def create :Session = {
    val s = super.create
    s.creationDate(new DateTime().toDate)
    s.hash( Helpers.md5(new Date().toString() + s.school.name + "sieve") )
  }

}