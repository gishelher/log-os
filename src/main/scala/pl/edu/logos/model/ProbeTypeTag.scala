package pl.edu.logos.model

import net.liftweb.mapper.{MappedLongForeignKey, IdPK, LongKeyedMetaMapper, LongKeyedMapper}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 26.11.12
 * Time: 09:07
 * To change this template use File | Settings | File Templates.
 */
class ProbeTypeTag extends LongKeyedMapper[ProbeTypeTag] with IdPK {

  override def getSingleton = ProbeTypeTag

  object probeType extends MappedLongForeignKey(this, ProbeType)

  object tag extends MappedLongForeignKey(this, Tag)


}

object ProbeTypeTag extends ProbeTypeTag with LongKeyedMetaMapper[ProbeTypeTag] {

}
