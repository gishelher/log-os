package pl.edu.logos.model

import net.liftweb.mapper._
import pl.edu.logos.util.FindableMeta

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/21/12
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */
class Suggestion extends LongKeyedMapper[Suggestion] with IdPK with ManyToMany with BaseSieveEntity[Suggestion] {

  def getSingleton = Suggestion

  object name extends MappedString(this, 128)

  object description extends MappedString(this, 1024)

  object disorders extends MappedManyToMany(DisorderSuggestion, DisorderSuggestion.suggestion, DisorderSuggestion.disorder, Disorder)

  object suggestionGroup extends MappedString(this, 128) {
    override def defaultValue = ""
  }

}

object Suggestion extends Suggestion with LongKeyedMetaMapper[Suggestion] with FindableMeta[Suggestion] {
  def searchResult(txt: String) = searchOnField(name)(txt)

  def newFields(item: Suggestion) = item.name :: item.description :: item.suggestionGroup :: Nil

  def baseLine(item: Suggestion) = item.name.is

  def additionalLine(item: Suggestion) = item.description.is
}
