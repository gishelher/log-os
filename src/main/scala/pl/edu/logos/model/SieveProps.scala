package pl.edu.logos.model

import net.liftweb.util.Props
import pl.edu.logos.util.PropError

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 8/27/12
 * Time: 3:53 PM
 * To change this template use File | Settings | File Templates.
 */
object SieveProps {

  object DbDriver extends StringProp("db.driver")

  object DbUrl extends StringProp("db.url")

  object DbPass extends StringProp("db.password")

  object DbUser extends StringProp("db.user")

  object URL extends StringProp("url")

  object FileDir extends StringProp("fileDir")

  object ClassifiersFilesDir extends StringProp("classifiers.dir")

}

case class StringProp(name: String) {

  val is = Props.get(name).openOr(throw PropError(name))

}
