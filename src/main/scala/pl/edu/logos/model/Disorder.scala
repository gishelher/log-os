package pl.edu.logos.model

import net.liftweb.mapper._
import pl.edu.logos.util.FindableMeta

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/21/12
 * Time: 5:57 PM
 * To change this template use File | Settings | File Templates.
 */
class Disorder extends LongKeyedMapper[Disorder] with IdPK with BaseSieveEntity[Disorder] with ManyToMany {


  def getSingleton = Disorder

  object name extends MappedString(this, 128)

  object suggestions
    extends MappedManyToMany(DisorderSuggestion, DisorderSuggestion.suggestion,
      DisorderSuggestion.disorder, Disorder)

  object disorderGroup extends MappedString(this, 128) {
    override def defaultValue = Disorder.noneGroup
  }

  override def equals(other: Any): Boolean = other match {
    case d: Disorder => id.is == d.id.is
    case _ => false
  }

  override def hashCode(): Int = id.is.hashCode()
}

object Disorder extends Disorder with LongKeyedMetaMapper[Disorder] with FindableMeta[Disorder] {

  val noneGroup = "brak"


  def searchResult(txt: String) = findAll(Like(name, "%" + txt + "%"))


  def newFields(item: Disorder) = item.name :: item.disorderGroup :: Nil

  def baseLine(item: Disorder) = item.name.is

  def additionalLine(item: Disorder) = ""
}
