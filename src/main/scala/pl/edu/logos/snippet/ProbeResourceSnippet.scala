package pl.edu.logos.snippet

import net.liftweb._
import util._
import common._
import http._
import java.io._
import pl.edu.logos.model.{Probe, ProbeType, ProbeResource}
import xml.NodeSeq
import net.liftweb.util.Helpers._
import pl.edu.logos.util.HTMLUtil
import net.liftweb.http.js.JsCmds

/**
 * Created with IntelliJ IDEA.
 * User: mikolaj
 * Date: 10/16/12
 * Time: 10:36 PM
 * To change this template use File | Settings | File Templates.
 */
object ProbeResourceSnippet {

  object img extends RequestVar(ProbeResource.create)
  object imageFile extends RequestVar[Box[FileParamHolder]](Empty)
  object fileName extends RequestVar[Box[String]](Full(Helpers.nextFuncName))

  def render(in: NodeSeq): NodeSeq = {
    // process the form
    def process() {
      (imageFile.is) match {
        case (Empty) => S.error("You forgot to choose a file to upload")
        case (image) => {
          imageFile.is.map{ file => saveFile(file) }
        }
      }
    }

    ("name=image" #> SHtml.fileUpload(s => imageFile(Full(s))) &
      "name=probe" #> SHtml.select(ProbeType.findAll.map { p=> (p.id.toString, p.name.toString)}, Empty, (t => img.is.probeType.set(t.toLong))) &
      "name=label" #> SHtml.text("", (t => img.is.label.set(t))) &
      "type=submit" #> SHtml.onSubmitUnit(process))(in)
  }

  private def saveFile(fp: FileParamHolder) = {
    fp.file match {
      case null =>
      case x if x.length == 0 =>
      case x =>{
        val filePath = "src/main/webapp/probeResources"

        fileName.is.foreach{
          name =>
            img.is.path.set("probeResources/" + name + fp.fileName)
            img.is.name.set(fp.fileName)
            img.saveMe()
        }

        val oFile = new File(filePath,  fileName.is.openOr("BrokenLink") + fp.fileName)
        val output = new FileOutputStream(oFile)
        output.write(fp.file)
        output.close()
        S.notice("Thanks for the upload")
      }
    }
  }

  def list: CssSel = {
    "#list" #> SHtml.idMemoize {
      context =>
        "#probeResourceList" #> ProbeResource.findAll().filter(!_.deleted).map{ pr =>
          "#path *" #>  SHtml.link(
            "http://127.0.0.1:8080/" + //Using open_! because we already made sure it is not Null
              pr.path,
            () => Unit ,
            <span>{pr.name}</span> ) &
            "#name *" #> pr.name &
            "#label *" #> pr.label &
            "#type *" #> ProbeType.find(pr.probeType).get.name &
            "#delete [onclick]" #> HTMLUtil.ajaxCall{
              pr.delete
              context.setHtml()
            }
      }
    }
  }
}
