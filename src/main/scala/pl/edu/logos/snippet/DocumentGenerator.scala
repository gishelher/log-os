package pl.edu.logos.snippet

import xml.NodeSeq
import pl.edu.logos.model.{Disorder, Probe, Case}
import pl.edu.logos.util.{SessionFlowException, HTMLUtil}
import net.liftweb.util.Helpers._
import org.joda.time.DateTime
import net.liftweb.http.S

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 19.11.12
 * Time: 10:55
 * To change this template use File | Settings | File Templates.
 */
object DocumentGenerator {

  /**
   * generuje karete badania dla pacejnta
   * @param myCase
   * @return  karta badania w html:
   */
  def caseReview(myCase: Case): NodeSeq = {
    val (initial, normal) = myCase.probes.partition(_.myType.obj.map(_.initial.is).getOrElse(false))
    val child = myCase.child
    def mapProbes(id: String, probes: Seq[Probe]) = {
      ("#" + id) #> probes.map {
        probe =>
          ".probeName *" #> probe.myType.obj.map(_.name.is).getOrElse("brak") &
            ".probeResult *" #> probe.result.obj.map(_.name.is).getOrElse("brak")
      }
    }
    (
      "#child-name *" #> child.name.is &
        "#born-date *" #> HTMLUtil.dateFormat.print(new DateTime(child.bornDate.is)).toString &
        "#case-date *" #> HTMLUtil.dateFormat.print(myCase.session.obj.map(s => new DateTime(s.examineDate.is)).getOrElse(new DateTime())).toString &
        mapProbes("inital-probes", initial) &
        mapProbes("probes", normal)
      )(HTMLUtil.template("docs" :: "caseReview" :: Nil))
  }

  /**
   * diagnoze dla pacejnta
   * @param myCase
   * @return  diagnoza w html:
   */
  def diagnosis(myCase: Case): NodeSeq = {
    val diagnosis = myCase.diagnosis.getOrElse(throw SessionFlowException("case has no diagnosis!"))
    val child = myCase.child

    val groups = diagnosis.disorders
      .groupBy(_.disorderGroup.is)
      .filterNot(el => el._1 == Disorder.noneGroup)

    ("#child-name *" #> child.name.is &
      "#born-date *" #> HTMLUtil.dateFormat.print(new DateTime(child.bornDate.is)) &
      "#case-date *" #> HTMLUtil.dateFormat.print(myCase.session.obj.map(s => new DateTime(s.examineDate.is)).getOrElse(new DateTime())) &



      (if (diagnosis.disorders.isEmpty) {
        "#disorders-parent" #> <p>Dziecko jest zdrowe</p>
      } else {
        ".simple-disorder" #> diagnosis.disorders.filter(_.disorderGroup.is == Disorder.noneGroup).map {
          disorder =>
            ".simple-disorder *" #> disorder.name.is
        } &
          ".disorder-group" #> groups.map {
            gr =>
              ".group-name" #> S.?(gr._1) &
                ".disorder-group-item" #> gr._2.map {
                  disorder =>
                    ".disorder-group-item *" #> disorder.name.is
                }
          }
      }) &
      (if (diagnosis.suggestions.isEmpty) {
        "#suggesions-parent" #> <p>Dziecko nie posiada żadnych zaleceń</p>
      } else {
        "#suggestions" #> diagnosis.suggestions.map(
          suggestion =>
            ".suggestion-name *" #> suggestion.name.is &
              ".suggestion-description *" #> suggestion.description.is
        )
      })

      )(HTMLUtil.template("docs" :: "diagnosis" :: Nil))
  }

}
