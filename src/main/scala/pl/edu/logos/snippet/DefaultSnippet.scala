package pl.edu.logos.snippet

import xml.NodeSeq
import pl.edu.logos.model.{Session, User}
import net.liftweb.util.Helpers._
import pl.edu.logos.Urls
import pl.edu.logos.util.HTMLUtil
import net.liftweb.http.js.JsCmds

/**
 * Created with IntelliJ IDEA.
 * User: jar
 * Date: 18.08.12
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
class DefaultSnippet {

  def menu(in: NodeSeq): NodeSeq = {
    (
      "#mySession [href]" #> Urls.SessionList.is &
        "#logout [href]" #> User.logOutUrl &
        "#editAccount [href]" #> User.editUrl &
        "#home [href]" #> User.homePage &
        "#probeResources [href]" #> Urls.ProbeResourceView.is &
        "#school [href]" #> Urls.SchoolView.is &
        "#loggedAs *" #> User.loggedUser.displayName
      )(in)
  }


}
