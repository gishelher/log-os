package pl.edu.logos.snippet

import pl.edu.logos.model.{ProbeType, Child, Case}
import xml.NodeSeq
import pl.edu.logos.util.{SearchModal, FindForm, HTMLUtil}
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._
import net.liftweb.http.S
import net.liftweb.mapper.By
import net.liftweb.common.{Loggable, Full}
import pl.edu.logos.expert.CaseSessionState
import net.liftweb.http.js.JsCmds
import pl.edu.logos.{Dispatchers, Urls}

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/26/12
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
object CaseSnippet extends Loggable {


  val caseParamName = "caseId"

  def renderCase(caseItem: Case): CssSel = {
    val childForm = FindForm(Child)
    val href = SessionSnippet.sessionViewUrl(caseItem.session.openTheBox, caseItem.id.is)

    "#caseView" #> HTMLUtil.uniqueIdMemoize {
      context =>
        val item = ~caseItem
        (item.childField.obj match {
          case Full(child) if caseItem.diagnosis.isEmpty =>
            "#conduct [onclick]" #> HTMLUtil.ajaxCall {
              JsCmds.RedirectTo(conductUrl(caseItem))
            } &
              "#case-result" #> NodeSeq.Empty
          case Full(child) =>
            "#conduct" #> NodeSeq.Empty &
              "#disorders-list [href]" #> DiagnosisSnippet.diagnosisUrl(item.diagnosis.openTheBox, href) &
              "#suggestions-list [href]" #> DiagnosisSnippet.suggestionsUrl(item.diagnosis.openTheBox, href) &
              "#dos-edit [href]" #> DiagnosisSnippet.docsUrl(item, href) &
              "#download-case-review [href]" #> Dispatchers.caseReviewPath(item.id.is) &
              "#download-diagnosis [href]" #> Dispatchers.diagnosisPath(item.id.is) &
              "#probes-list [href]" #> CaseSnippet.caseViewPath(item)

          case _ =>
            "#case-result" #> NodeSeq.Empty &
              "#conduct" #> NodeSeq.Empty
        }) &
          "#childForm" #> childForm.view("Dziecko", caseItem.childField.obj, item => {
            (item match {
              case Full(t) =>
                caseItem.childField(t).saveMe()
                context.setHtml()
              case _ =>
                JsCmds.Noop
            })
          })
    }
  }


  def caseHtml(caseItem: Case, in: NodeSeq = HTMLUtil.template("case" :: "view" :: Nil)): NodeSeq = renderCase(caseItem)(in)

  def conductUrl(caseItem: Case) = Urls.Conduct.params(caseParamName -> caseItem.id.is.toString)


  def conduct(in: NodeSeq): NodeSeq = {

    (S.param(caseParamName).flatMap(id => Case.find(By(Case.id, id.toLong))) match {
      case Full(caseItem) =>
        var sessionState = CaseSessionState.createForCase(caseItem)
        "#conduct-main" #> HTMLUtil.uniqueIdMemoize {
          context =>
            "#current-probe" #> sessionState.currentProbeType.toList.map {
              el =>
                val (pt, res) = el
                "#probeAnswers" #> pt.possibleAnswers.map {
                  answer =>
                    "#probeAnswer *" #> answer.name.is &
                      "#probeAnswer [onclick]" #> HTMLUtil.ajaxCall {
                        sessionState = sessionState.answer(answer, pt)
                        context.setHtml()
                      } &
                      "#probeAnswer [class+]" #> (if (answer == res) answer.className.is else answer.className + " unselected")
                } &
                  "#probeType *" #> pt.name &
                  "#skip-item [onclick]" #> HTMLUtil.ajaxCall {
                    sessionState = sessionState.skip(pt)
                    context.setHtml()
                  }
            } &
              "#new-probeType [onclick]" #> HTMLUtil.ajaxCall(HTMLUtil.showDialog("Nowa próba", SearchModal.modal[ProbeType](ProbeType,
                box => {
                  (box match {
                    case Full(probeType) =>
                      sessionState = sessionState.addType(probeType)
                      context.setHtml()
                    case _ =>
                      JsCmds.Noop
                  }) & HTMLUtil.closeDialog()
                }))) &
              "#next-step [onclick]" #> HTMLUtil.ajaxCall {
                sessionState = sessionState.nextStep()
                context.setHtml()
              } &
              "#result [onclick]" #> HTMLUtil.ajaxCall {
                JsCmds.RedirectTo(DiagnosisSnippet.diagnosisUrl(sessionState.result.saveMe()))
              } &
              (sessionState.currentResource match {
                case Full(pr) =>
                  "#refresh-resource [onclick]" #> HTMLUtil.ajaxCall {
                    sessionState = sessionState.refreshProbe
                    context.setHtml()
                  } &
                    (if (sessionState.hasNext)
                      "#next-resource [onclick]" #> HTMLUtil.ajaxCall {
                        sessionState = sessionState.nextProbeResource
                        context.setHtml()
                      } else {
                      "#next-resource" #> NodeSeq.Empty
                    }) &
                    "#resource [src]" #> pr.url
                case _ =>
                  "#resource-control-box" #> NodeSeq.Empty
              })
        }
      case _ =>
        "#conduct-main *" #> "Brak przypadku lub brak uprawnień!"
    })(in)
  }

  def caseViewPath(c: Case) = Urls.CaseView.params(caseParamName -> c.id.is.toString)

  def caseView(in: NodeSeq): NodeSeq = {
    (S.param(caseParamName).flatMap(id => Case.find(By(Case.id, id.toLong))) match {
      case Full(caseItem) =>
        "#probe-table" #> caseItem.probes
          .map(c => c.myType.openTheBox -> c.result.obj).filterNot(_._2.isEmpty).map {
          el =>
            "#probeType *" #> el._1.name.is &
              el._2.map {
                res =>
                  "#probeAnswers *" #> res.name.is &
                    "#probeAnswers [class+]" #> (if (!res.positive) res.className.is else "")
              }.openOr("#probeAnswers *" #> "brak")
        }
      case _ =>
        "#conduct-main *" #> "Brak przypadku lub brak uprawnień!"
    })(in)
  }


}
