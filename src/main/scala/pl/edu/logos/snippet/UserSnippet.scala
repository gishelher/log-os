package pl.edu.logos.snippet

import xml.NodeSeq
import net.liftweb.util.Helpers._
import net.liftweb.http.{S, SHtml}
import pl.edu.logos.util.{AjaxForm, HTMLUtil}
import pl.edu.logos.model.User
import net.liftweb.common.{Empty, Box, Full}
import net.liftweb.http.js.JsCmds
import collection.immutable.Nil
import net.liftweb.util.{SecurityHelpers, FieldError}
import net.liftweb.mapper.MappedPassword

/**
 * Created with IntelliJ IDEA.
 * User: jar
 * Date: 16.08.12
 * Time: 21:19
 * To change this template use File | Settings | File Templates.
 */

class UserSnippet {


  def login(in: NodeSeq): NodeSeq = {

    var email: String = ""
    var pass: String = ""
    (
      "#email" #> SHtml.ajaxText(email, email = _) &
        "#password" #> SHtml.ajaxText(pass, pass = _, "type" -> "password") &
        "#log-in [onclick]" #> HTMLUtil.ajaxCall {
          User.findByEmail(email) match {
            case Full(user)
              if  user.password.match_?(pass) =>
              User.logUserIn(user)

              JsCmds.RedirectTo(User.loginRedirect.is match {
                case Full(url) =>
                  url
                case _ => User.homePage
              })
            case _ =>
              HTMLUtil.showStringMessage(S.?("user.login.bad-data"))
          }
        } &
        "#register [href]" #> User.signUpUrl
      )(in)
  }

  def singUp(in: NodeSeq): NodeSeq = {

    val user = User.create
    val rePass = new MappedPassword(user) {

      def matchPass(value: String): List[FieldError] = {
        if (!user.password.match_?(value)) {
          List(FieldError(this, S.?("user.singup.password.mismatch").toString))
        } else {
          Nil
        }
      }

      override def validations = matchPass _ :: Nil


      override def displayName = "rePass"
    }

    val form = new AjaxForm[User](user, user.email, rePass, user.password) {
      edit = true;

    }

    (
      "#singUpForm *" #> form.view {

        val usr = form.obj.validated(true)
        usr.saveMe()

        User.logUserIn(usr)
        JsCmds.RedirectTo(User.homePage)
      } &
        "#login [href]" #> User.loginPath.mkString("/", "/", "") &
        "#myPassword-input [type]" #> "password"
      )(in)
  }


  def edit(in: NodeSeq): NodeSeq = {
    val user = User.loggedUser
    val form = new AjaxForm(user, user.myEditFields: _*)


    ("#editForm" #> form.view {
      user.saveMe
      JsCmds.Noop
    })(in)
  }

}
