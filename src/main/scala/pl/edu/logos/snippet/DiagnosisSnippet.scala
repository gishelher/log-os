package pl.edu.logos.snippet

import pl.edu.logos.model.{Suggestion, Case, Disorder, Diagnosis}
import xml.{Utility => XMLUtil, Text, NodeSeq}
import pl.edu.logos.util.{FindForm, SearchModal, HTMLUtil}
import net.liftweb.util.Helpers._
import net.liftweb.http.js.JsCmds
import net.liftweb.common._
import net.liftweb.http.{S, SHtml}
import net.liftweb.http.js.jquery.JqJsCmds
import net.liftweb.mapper.By
import pl.edu.logos.Urls
import java.net.URLDecoder


/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 10/21/12
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */
object DiagnosisSnippet {

  val redirectPram = "redirectTo"

  val diagnosisParamName = "diagnosisId"

  def diagnosisUrl(diagnosis: Diagnosis, redirect: String = "-1") = Urls.Diagnosis.params(diagnosisParamName -> diagnosis.id.is.toString, redirectPram -> redirect)

  def docsUrl(myCase: Case, redirect: String = "-1") = Urls.CaseDocs.params(caseParamName -> myCase.id.is.toString, redirectPram -> redirect)

  def suggestionsUrl(diagnosis: Diagnosis, redirect: String = "-1") = Urls.Suggestions.params(diagnosisParamName -> diagnosis.id.is.toString, redirectPram -> redirect)

  val caseParamName = "caseId"

  def renderDiagnosis(in: NodeSeq): NodeSeq = {

    val redirect = S.param(redirectPram).map(p => URLDecoder.decode(p, "UTF-8")).filter(_ != "-1")

    (S.param(diagnosisParamName).flatMap(id => Diagnosis.find(By(Diagnosis.id, id.toLong))) match {
      case Full(_diagnosis) =>
        "#main-diagnosis *" #> HTMLUtil.uniqueIdMemoize {
          context =>
            val diagnosis = _diagnosis.reload
            "#add-disorder [onclick]" #> HTMLUtil.ajaxCall {
              HTMLUtil.showDialog("Dodaj zaburzenie", SearchModal.modal[Disorder](Disorder,
                disorderBox => {
                  disorderBox match {
                    case Full(disorder) =>
                      diagnosis.disorders += disorder
                      diagnosis.saveMe()
                    case _ =>
                  }
                  HTMLUtil.closeDialog() &
                    context.setHtml()
                }
              ))
            } &
              "#disorder" #> diagnosis.disorders.map {
                disorder =>
                  "#disorder-name *" #> disorder.name.is &
                    "#remove-disoreder [onclick]" #> HTMLUtil.ajaxCall {
                      diagnosis.disorders -= disorder
                      diagnosis.saveMe()

                      context.setHtml()
                    }
              } &
              "#suggestions [onclick]" #> HTMLUtil.ajaxCall {
                JsCmds.RedirectTo(redirect.getOrElse(
                  suggestionsUrl(diagnosis)))
              }
        }
      case _ =>
        "#main-diagnosis *" #> "Brak diagnozy albo nie posiadasz do niej uprawnień!"
    })(in)
  }

  def renderSuggestions(in: NodeSeq): NodeSeq = {

    val redirect = S.param(redirectPram).map(p => URLDecoder.decode(p, "UTF-8")).filter(_ != "-1")
    (S.param(diagnosisParamName).flatMap(id => Diagnosis.find(By(Diagnosis.id, id.toLong))) match {
      case Full(_diagnosis) =>
        var newShown = false
        var newSuggestion: Box[Suggestion] = Empty
        var newDisordersSet = Set[Disorder]()
        "#suggestions *" #> HTMLUtil.uniqueIdMemoize {
          context =>
            val diagnosis = _diagnosis.reload
            "#added-suggestion" #> diagnosis.suggestions.map {
              suggestion =>
                "#name *" #> suggestion.name.is &
                  "#description *" #> suggestion.description.is &
                  "#delete-suggestion [onclick]" #> HTMLUtil.ajaxCall {
                    diagnosis.suggestions -= suggestion
                    diagnosis.saveMe()
                    context.setHtml()
                  }

            } &
              (if (newShown) {
                "#new-suggestion" #> NodeSeq.Empty &
                  "#selectedSuggestion *" #> FindForm(Suggestion).view("Wybierz zalecenie", newSuggestion, box => {
                    newSuggestion = box
                    box.foreach(_.saveMe())
                    context.setHtml()
                  }) &
                  "#disorder-match" #> diagnosis.disorders.map {
                    disorder =>
                      "#disorder-checkbox" #> SHtml.ajaxCheckbox(newDisordersSet.contains(disorder),
                        _ match {
                          case true => newDisordersSet = newDisordersSet + disorder
                          case _ => newDisordersSet = newDisordersSet - disorder
                        }) &
                        "#disorderName" #> disorder.name
                  } &
                  "#add-suggestion [onclick]" #> HTMLUtil.ajaxCall {
                    newSuggestion match {
                      case Full(suggestion) if !newDisordersSet.isEmpty =>
                        suggestion.disorders ++= newDisordersSet
                        suggestion.saveMe()
                        diagnosis.suggestions += suggestion
                        diagnosis.saveMe()

                        newSuggestion = Empty
                        newShown = false

                        context.setHtml()
                      case Full(_) => JqJsCmds.DisplayMessage("suggestion-error", Text("Musisz powiązać przynajmniej jedno zaburzenie"), 5 second, 2 second)
                      case _ => JqJsCmds.DisplayMessage("suggestion-error", Text("Dodaj zalecenie!"), 5 second, 2 second)
                    }
                  }
              } else {
                "#new-suggestion [onclick]" #> HTMLUtil.ajaxCall {
                  newShown = true;
                  context.setHtml()
                } &
                  "#newSuggestion" #> NodeSeq.Empty
              }) &
              "#finish [onclick]" #> HTMLUtil.ajaxCall {
                diagnosis.saveMe()
                JsCmds.RedirectTo(redirect.orElse(
                  diagnosis.myCase.map(el => docsUrl(el)))
                  .getOrElse("/"))
              }
        }
      case _ =>
        "#suggestions *" #> "Brak diagnozy albo nie posiadasz do niej uprawnień!"
    })(in)
  }

  def renderDocs(in: NodeSeq): NodeSeq = {
    val funcName = "sendtextFieldData"
    val caseReviewParam = "caseReview"
    val caseDiagnosisParam = "diagnosis"

    val redirect = S.param(redirectPram).map(p => URLDecoder.decode(p, "UTF-8")).filter(_ != "-1")
    (S.param(caseParamName).flatMap(id => Case.find(By(Case.id, id.toLong))) match {
      case Full(myCase) =>
        "#case-review *" #> DocumentGenerator.caseReview(myCase) &
          "#case-diagnosis *" #> DocumentGenerator.diagnosis(myCase) &
          "#finish-case [onclick]" #> (JsCmds.Run("%s(CKEDITOR.instances['case-review'].getData(), CKEDITOR.instances['case-diagnosis'].getData());".format(funcName)) & JsCmds.Noop) &
          "#textfield-script" #> HTMLUtil.ajaxFunction(funcName, caseReviewParam, caseDiagnosisParam)(map => {
            (map.get(caseReviewParam), map.get(caseDiagnosisParam)) match {
              case (Some(caseReviewText), Some(caseDiagnosisText)) =>

                myCase.diagnosisText(caseDiagnosisText).caseReviewText(caseReviewText).saveMe()

                JsCmds.RedirectTo(redirect
                  .orElse(myCase.session.map(el => SessionSnippet.sessionViewUrl(el))).getOrElse("/"))
              case _ =>
                HTMLUtil.showStringMessage("wystąpił bład systemu, skopiuj dane i odświerz storne", "ok-errors")
            }
          })
      case _ =>
        "#docs" #> "Brak przypadku albo nie posiadasz do niego uprawnień!"
    })(in)
  }
}
