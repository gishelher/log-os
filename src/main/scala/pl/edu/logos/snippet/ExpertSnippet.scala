package pl.edu.logos.snippet

import xml.NodeSeq
import net.liftweb.http.SHtml
import pl.edu.logos.util.HTMLUtil
import pl.edu.logos.util.HTMLUtil._
import net.liftweb.util.Helpers._
import pl.edu.logos.expert.MalletClassifierSelector._
import cc.mallet.classify.Classifier
import pl.edu.logos.expert.MalletManager
import pl.edu.logos.model.{Disorder, Case}
import pl.edu.logos.expert.MalletManager.ClassifierResult

/**
 * Created with IntelliJ IDEA.
 * User: mikolaj
 * Date: 1/2/13
 * Time: 10:20 PM
 * To change this template use File | Settings | File Templates.
 */
object ExpertSnippet {

  def view(in: NodeSeq): NodeSeq = {

    var data = List[(Disorder, (List[ClassifierResult], ClassifierResult))]()
    (
      "#content" #> SHtml.idMemoize {
        context =>
          "#train [onclick]" #> HTMLUtil.ajaxCall {
            TrainerActor ! TrainALL
            context.setHtml()
          } &
            (if (TrainerActor.busy) {
              ".disorder-item *" #> NodeSeq.Empty
            } else {
              ".working" #> NodeSeq.Empty &
                TrainerActor.currentRes.map(
                  data =>
                    ".disorder-item" #> data.map(
                      disorderEntry =>
                        ".disorder-name *" #> disorderEntry._1.name.is &
                          ".bestName *" #> disorderEntry._2._2.name &
                          ".resolution *" #> toPercent(disorderEntry._2._2.positive) &
                          ".value-entry *" #> disorderEntry._2._1.map {
                            entry =>
                              ".name *" #> entry.name &
                                ".positive *" #> toPercent(entry.positive) &
                                ".yy *" #> toPercent(entry.yy) &
                                ".yn *" #> toPercent(entry.yn) &
                                ".ny *" #> toPercent(entry.ny) &
                                ".nn *" #> toPercent(entry.nn) &
                                ".train *" #> toPercent(entry.train)

                          }
                    )
                ).getOrElse(".disorder-item *" #> "no current data - search for best classifiers")

            })
      }
      )(in)
  }

}