package pl.edu.logos.snippet

import xml.NodeSeq
import net.liftweb.util.Helpers._
import pl.edu.logos.util.{AjaxForm, HTMLUtil}
import net.liftweb.http.{S, SHtml}
import pl.edu.logos.model._
import net.liftweb.http.js.JsCmds
import pl.edu.logos.Urls
import net.liftweb.common.Full
import net.liftweb.util.Helpers
import net.liftweb.mapper.By

/**
 * Created with IntelliJ IDEA.
 * User: krzysiek
 * Date: 9/22/12
 * Time: 7:47 PM
 * To change this template use File | Settings | File Templates.
 */
object SessionSnippet {

  def sessionList(in: NodeSeq): NodeSeq = {

    var showDeleted = false;
    ("#sessionCont" #> SHtml.idMemoize {
      context =>
        "#newSession [onclick]" #> HTMLUtil.ajaxCall {
          Session.create.owner(User.loggedUser).saveMe()
          context.setHtml()
        } &
          "#sessionItem" #> User.loggedUser.openSession.map {
            session =>
              "#session-clickable [onclick]" #> (JsCmds.RedirectTo(Urls.SessionView.params(sessionIdParam -> session.id.toString)) & JsCmds.Noop) &
                "#session-name *" #> session.id &
                "#school-name *" #> (session.school.obj match {
                  case Full(school) => school.name.toString
                  case s =>
                    S.?("school.name.empty")
                }) &
                "#delete-session [onclick]" #> HTMLUtil.ajaxCall {
                  session.deleted(true).saveMe()
                  context.setHtml()
                }
          } &
          "#done-sessionItem" #> User.loggedUser.closedSession.map {
            session =>
              "#done-session-clickable [onclick]" #> (JsCmds.RedirectTo(Urls.SessionView.params(sessionIdParam -> session.id.toString)) & JsCmds.Noop) &
                "#done-session-name *" #> session.id &
                "#done-school-name *" #> (School.find(By(School.id, session.school.is)) match {
                  case Full(school) => school.name.toString
                  case _ => S.?("school.name.empty")
                }) &
                "#done-finish-date *" #> session.examineDate
          } &
          (if (showDeleted)
            "#restore-sessionItem" #> User.loggedUser.deletedSession.map {
              session =>
                "#restore-session-clickable [onclick]" #> (JsCmds.RedirectTo(Urls.SessionView.params(sessionIdParam -> session.id.toString)) & JsCmds.Noop) &
                  "#restore-session-name *" #> session.id &
                  "#restore-school-name *" #> (School.find(By(School.id, session.school.is)) match {
                    case Full(school) => school.name.toString
                    case _ => S.?("school.name.empty")
                  }) &
                  "#finish-date *" #> session.examineDate
            } &
              "#toggle-deleted [onclick]" #> HTMLUtil.ajaxCall {
                showDeleted = false
                context.setHtml()
              } &
              "toggle-deleted-img [src]" #> "/images/slideUp.png"
          else
            "#restore-sessionItem" #> NodeSeq.Empty &
              "#toggle-deleted [onclick]" #> HTMLUtil.ajaxCall {
                showDeleted = true
                context.setHtml()
              } &
              "#toggle-deleted-img [src]" #> "/images/slideDown.png"
            )
    })(in)
  }


  val sessionIdParam = "sessionId"
  val caseId = "caseId"

  def sessionViewUrl(session: Session, caseItemId: Long = -1L) =
    Urls.SessionView.params(sessionIdParam -> session.id.is.toString, caseId -> caseItemId.toString)

  def sessionView(in: NodeSeq): NodeSeq = {
    (S.param(sessionIdParam).flatMap(id => Helpers.tryo(id.toInt)).flatMap(id => User.loggedUser.getSession(id.toLong)) match {
      case Full(session) =>

        val sessionForm = AjaxForm(session, session.isPublicEditable, session.examineDate, session.school)

        var selectedCaseId: Long = S.param(caseId).map(_.toLong).getOrElse(-1L)

        "#bad-data" #> NodeSeq.Empty &
          "#sessionView" #> SHtml.idMemoize {
            context =>

              val allCases = session.cases
              var showDeleted = false
              val toggle = HTMLUtil.ToggleItem("toggle-deleted",
                "#del-case-item" #> allCases.filter(_.deleted.is).map {
                  caseItem =>
                    "#del-childName *" #> caseItem.childName &
                      "#del-status [src]" #> caseItem.diagnosis.map(_ => "/images/ok.png").getOrElse("/images/question.png") &
                      "#del-resore [onclick]" #> HTMLUtil.ajaxCall {
                        caseItem.restore
                        context.setHtml()
                      }
                }
              )

              "#session-form *" #> sessionForm.view({
                session.saveMe()
                context.setHtml()
              }) &
                "#addCase [onclick]" #> HTMLUtil.ajaxCall {
                  val newCase = Case.create.saveMe()
                  session.cases += newCase
                  session.saveMe()
                  selectedCaseId = newCase.id.is
                  context.setHtml()
                } &
                "#case-item" #> allCases.filter(!_.deleted.is).map {
                  caseItem =>
                    "#childName *" #> caseItem.childName &
                      "#childName [onclick]" #> HTMLUtil.ajaxCall {
                        selectedCaseId = caseItem.id.is
                        context.setHtml()
                      } &
                      "#status [src]" #> caseItem.diagnosis.map(_ => "/images/ok.png")
                        .getOrElse("/images/question.png") &
                      "#status [onclick]" #> HTMLUtil.ajaxCall {
                        caseItem.setEmptyOkDiagnosis.saveMe()
                        context.setHtml()
                      } &
                      "#delete [onclick]" #> HTMLUtil.ajaxCall {
                        caseItem.delete
                        context.setHtml()
                      }
                } &
                toggle.bind &
                (Case.find(By(Case.id, selectedCaseId)) match {
                  case Full(caseItem) =>
                    "#case-view *" #> CaseSnippet.caseHtml(caseItem)
                  case _ => "#case-view" #> NodeSeq.Empty
                }
                  )
          }
      case _ => "#sessionView" #> NodeSeq.Empty
    })(in)

  }

  def sessionCreate(in: NodeSeq): NodeSeq = {
    (S.param("hash").flatMap(hash => Helpers.tryo(hash)).flatMap(hash => Session.find(By(Session.hash, hash))) match {
      case Full(session) =>

        var name = ""
        var pesel = ""

        "#sessionCreate" #> SHtml.idMemoize {
          context =>
            var child = Child.create

            "#child" #> Session.find(By(Session.id, session.id)).get.cases.filter(!_.deleted).map(_.child).map(child =>
              "#name *" #> child.name &
                "#pesel *" #> child.PESEL &
                "name=delete [onclick]" #> HTMLUtil.ajaxCall {
                  val cs = Case.find(By(Case.childField, child)).get
                  session.cases -= cs
                  session.saveMe()
                  cs.delete
                  child.delete
                  context.setHtml()
                }
            ) &
              "name=name *" #> SHtml.ajaxText(name, name = _) &
              "name=schoolName *" #> School.find(By(School.id, session.school.is)).get.name &
              "name=pesel *" #> SHtml.ajaxText(pesel, pesel = _) &
              "name=add [onclick]" #> HTMLUtil.ajaxCall {
                child.PESEL(pesel).name(name).saveMe()
                session.cases += Case.create.childField(child).saveMe()
                session.saveMe()
                name = ""
                pesel = ""
                context.setHtml()
              }
        }
      case _ => "#sessionCreate" #> NodeSeq.Empty
    })(in)
  }

}
