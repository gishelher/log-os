package pl.edu.logos.snippet

import pl.edu.logos.model._
import pl.edu.logos.util.HTMLUtil
import net.liftweb.http.js.JsCmds
import xml.NodeSeq
import net.liftweb.http.SHtml
import pl.edu.logos.Urls
import net.liftweb.util.Helpers._

/**
 * Created with IntelliJ IDEA.
 * User: mikolaj
 * Date: 11/16/12
 * Time: 11:03 AM
 * To change this template use File | Settings | File Templates.
 */
object SchoolSnippet {
  def view(in: NodeSeq): NodeSeq = {
    var schoolShowId: Long = -1

    ("#schoolCont" #> SHtml.idMemoize {
      context =>
        var name = ""

          "name=name" #> SHtml.ajaxText(name, name = _) &
          "name=submit [onclick]" #> HTMLUtil.ajaxCall {
            School.create.name(name).saveMe()
            context.setHtml()
          } &
          "#schoolList" #> School.findAll().filter(!_.deleted).map {
            school =>
              "name=name *" #> school.name &
                (if (school.id.is == schoolShowId)
                  "name=session *" #> school.sessions.map(ss =>
                    SHtml.a(<span>{ss.id.is.toString}</span><br/>, HTMLUtil.ajaxCall {
                      JsCmds.RedirectTo(Urls.SessionCreateView.params("hash" -> ss.hash.is))
                    })
                  )
                else
                  "name=sessions *" #> school.sessions.size.toString
                  ) &
                "name=sessionCreate [onclick]" #> HTMLUtil.ajaxCall {
                  val s = Session.create.owner(User.loggedUser).saveMe
                  school.sessions += s
                  school.saveMe()
                  JsCmds.RedirectTo(Urls.SessionCreateView.params("hash" -> s.hash.is))
                } &
                "name=delete [onclick]" #> HTMLUtil.ajaxCall {
                  school.delete
                  context.setHtml()
                } &
                "name=sessionShow [onclick]" #> HTMLUtil.ajaxCall {
                  if (school.id.is == schoolShowId)
                    schoolShowId = -1
                  else
                    schoolShowId = school.id.is
                  context.setHtml()
                }
          }
    }
      )(in)
  }
}
