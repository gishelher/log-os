package bootstrap.liftweb

import _root_.net.liftweb.common._
import _root_.net.liftweb.http._
import _root_.net.liftweb.http.provider._
import net.liftweb.sitemap._
import _root_.pl.edu.logos.model._

import net.liftweb.db.{DefaultConnectionIdentifier, DB}
import net.liftweb.mapper.Schemifier
import pl.edu.logos.util.PostgreSQLDBVendor
import pl.edu.logos.{Dispatchers, Urls}


/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 */
class Boot extends Loggable {
  def boot {

    if (!DB.jndiJdbcConnAvailable_?) {
      DB.defineConnectionManager(DefaultConnectionIdentifier, PostgreSQLDBVendor)
  }

    Schemifier.schemify(true, Schemifier.infoF _, User, Session, Case,
      Child, Diagnosis, Probe, ProbeType, ProbeResource, DiagnosisDisorder,
      Diagnosis, Disorder, School, DiagnosisSuggestion, Suggestion,
      DisorderSuggestion, ProbeResource, Tag, ProbeTypeTag, ProbeResult, ProbeResultProbeType)

    logger.info("DB initialized")

    LiftRules.htmlProperties.default.set((r: Req) => new Html5Properties(r.userAgent).setContentType(() => Full("text/html")))

    // where to search snippet
    LiftRules.addToPackages("pl.edu.logos")

    (Dispatchers.dispatch ::
      Nil).foreach(LiftRules.dispatch.append _)

    val entries: List[Menu] =
      Menu(Loc("index", "index" :: Nil, S.?("index"), User.loginFirst)) ::
        Urls.entries :::
        User.menus


    LiftRules.setSiteMap(SiteMap(entries: _*))

    /*
     * Show the spinny image when an Ajax call starts
     */
    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)

    /*
     * Make the spinny image go away when it ends
     */
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    LiftRules.early.append(makeUtf8)

    LiftRules.loggedInTest = Full(() => User.loggedIn_?)

  }

  /**
   * Force the request to be UTF-8
   */
  private def makeUtf8(req: HTTPRequest) {
    req.setCharacterEncoding("UTF-8")
  }
}
