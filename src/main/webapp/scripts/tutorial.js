try {
    logos;
} catch (e) {
    logos = {}
}

logos.tutorials = {

    sample: [
        {id: 'home', text: 'ala ma kota'},
        {id: 'logout', text: 'ula tez ma kota!'}
    ],

    info: []
}
;

logos.showTutorial = function (tips) {

    self = this;

    self.tips = tips.reverse();

    self.tipBaseSelector = "#base-tip"

    self.tipOffestW = 35;
    self.tipOffestH = 15;

    self.nextTip = function () {
        var tip = self.tips.pop();

        if (tip) {
            var selector = tip.selector ? tip.selector : '#' + tip.id;
            logos.showTipOnButton(selector, tip.text);
        }

    }
    self.nextTip();

}


logos.showTipOnButton = function (buttonQuery, text, onlyInfo) {

    var bodyWidth = $('body').width();
    var bodyHeight = $('body').height();


    var button = $(buttonQuery)[0];
    var buttons = $(button);

    if (button) {
        var tipBase = $(self.tipBaseSelector).clone().html(text)


        var left = bodyWidth / 2 - 100;
        var top = bodyHeight / 2 - 100;


        var oldClick = button.onclick;
        var oldHref = button.href;

        var fixButton = function () {
            if (oldClick) {
                button.onclick = oldClick;
            }
            buttons.toggleClass('tip-hightlight');


            setTimeout(function () {
                if (oldHref) {
                    button.href = oldHref;
                }
            }, 1000);

        }


        var hideTip = function () {
            $('.tip').hide();
            fixButton();
            $.unblockUI();
            if (!onlyInfo)
                self.nextTip();
        }


        if (oldHref)
            button.href = 'javascript://';

        button.onclick = function () {
            hideTip();
        }

        buttons.toggleClass('tip-hightlight');


        var tt = tipBase.click(hideTip)
            .addClass('tip')
        $.blockUI({message: tt});
        return tipBase;
    }
}
{
    var old = window.onload;
    window.onload = function () {
        try {

            var tutorial = window.location.search.replace('?', '').split('&')
                .map(function (item) {
                    return item.split('=')
                })
                .filter(function (item) {
                    return item[0] == 'tutorial'
                })[0]
            if (tutorial && tutorial[1]) {
                logos.showTutorial(logos.tutorials[tutorial[1]])
            }
        } catch (e) {
            console.error(e)
        }
        if (old) {
            old();
        }

    }
}
