﻿SELECT
p.result, pr.positive_c, p.mytype, p.mycase, dd.disorder
FROM probe as p 
left join proberesult as pr 
on p.result = pr.id
left join diagnosis as d
on d.mycase = p.mycase
left outer join diagnosisdisorder as dd
on d.id = dd.diagnosis;
