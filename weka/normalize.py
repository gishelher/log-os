#! /usr/bin/env python
import sys

data = []
f = open(sys.argv[1], 'r')
f.readline()
for line in f:
    data += [line.strip().split(';')]
print 'Przypdaki ktore nie maja przypisanego badania:'
ilness = {}
classes = []
for res in data:
    try:
	ilnessId = int(res[4])
    except ValueError:
	ilnessId = -1
    try:
	cases = ilness[ilnessId]
    except KeyError:
	ilness[ilnessId] = {}
	cases = ilness[ilnessId]
	classes.append(ilnessId)
    try:
        try:
            cases[int(res[3][:2])][int(res[2])] = res[1]
        except KeyError:
            cases[int(res[3][:2])] = {}
            cases[int(res[3][:2])][int(res[2])] = res[1]
    except ValueError:
	print res

output = ''
children = []

class Ilness:
    sick = []
    ok = []
    pass

ilnessesDict = {}
for iln in ilness:
    ilnessesDict[iln] = Ilness()
    
for iln in ilness:
    for case in ilness[iln]:
	childStr=''
	for i in range(1,70):
	    try:
		childStr += cases[case][i]+','
	    except KeyError:
		childStr += '?,'
	childStr = childStr.replace('f','0').replace('t','1')
	if not childStr in children:
	    children.append(childStr)
	if iln != -1 and not childStr in ilnessesDict[iln].sick:
	    ilnessesDict[iln].sick.append(childStr)
	    
for iln in ilness:
    for case in ilness[iln]:
	childStr=''
	for i in range(1,70):
	    try:
		childStr += cases[case][i]+','
	    except KeyError:
		childStr += '?,'
	childStr = childStr.replace('f','0').replace('t','1')
	if not childStr in children:
	    children.append(childStr)
	if iln == -1:
	    childStr = childStr.replace('?','1')
	    ilnessesDict[iln].ok.append(childStr)
	else:
	    for iln in ilness:
		if not childStr in ilnessesDict[iln].sick:
		    ilnessesDict[iln].ok.append(childStr)
		  
output = {}
for iln in ilness:		    
    tmp = ''
    for k in ilnessesDict[iln].ok:
	tmp += k + 'healthy\n'
    for k in ilnessesDict[iln].sick:
	tmp += k + 'class' + str(iln) + '\n'
    output[iln] = tmp
    
classes.remove(-1)
for cl in classes:
    fo = open('class' + str(cl) + '.arff','w+')
    classesStr = 'healthy, class' + str(cl) 
    fo.write('@relation class'+str(cl)+'\n\n')
    for n in range(1,70):
	fo.write('@attribute f' + str(n) + ' numeric\n')
    fo.write('@attribute class {'+classesStr+'}\n\n')
    fo.write('@data\n')
    fo.write(output[cl])
